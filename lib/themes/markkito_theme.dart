import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:markkito_customer/constants/colors.dart';

TextStyle get originalBodyText1 => GoogleFonts.nunito(
      color: AppColors.black1,
    );

TextTheme get originalTextTheme {
  // if (mode == ThemeMode.dark) {
  //   return ThemeData.dark().textTheme;
  // }

  return ThemeData.light().textTheme;
}

IconThemeData get originalIconTheme {
  // if (mode == ThemeMode.dark) {
  //   return ThemeData.dark().iconTheme;
  // }

  return ThemeData.light().iconTheme;
}
