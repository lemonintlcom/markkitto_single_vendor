import 'package:country_codes/country_codes.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:markkito_customer/firebase_options.dart';
import 'app/routes/app_pages.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await CountryCodes.init();
  await Hive.initFlutter();
  await Hive.openBox(Constants.configName);

  Stripe.publishableKey =
      'pk_test_51Kh9KsDKBBvEBaZxK6nj25HaVH7puFseM1ZMti2xWt5Irteol9fvRxXXohConP6QHWpKOnNrKLKJkk6XXJyjo3AO00pEIsMJLL';
  var secretKey =
      'sk_test_51Kh9KsDKBBvEBaZxYG29fpE9CiVSmJIv1LJiLpaHtrk0pva2MwD8SXdwWHiwMs2X0XbCjAEWmdb8bOwJUNI5FPlP00m4WkPFvq';
  Stripe.merchantIdentifier = 'Mohdyoonus@hotmail.com';
  Stripe.urlScheme = 'flutterstripe';
  await Stripe.instance.applySettings();
  // Firebase.initializeApp();
  // FirebaseAnalytics analytics = FirebaseAnalytics.instance;

  runApp(GetMaterialApp(
    builder: EasyLoading.init(),
    title: "Application",
    debugShowCheckedModeBanner: false,
    initialRoute: AppPages.INITIAL,
    getPages: AppPages.routes,
    theme: ThemeData(
        pageTransitionsTheme: const PageTransitionsTheme(builders: {
      TargetPlatform.iOS: ZoomPageTransitionsBuilder(),
      TargetPlatform.android: ZoomPageTransitionsBuilder(),
    })),
  ));
}
