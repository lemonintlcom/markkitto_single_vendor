class Dimens {
  Dimens._();
}

const paddingExtraSmall = 6.0;
const paddingSmall = 8.0;
const paddingMedium = 12.0;
const paddingLarge = 16.0;
const paddingExtraLarge = 24.0;
