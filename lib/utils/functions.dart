import 'dart:collection';
import 'dart:developer';

import 'package:intl/intl.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../constants/constants.dart';

extension DoubleExt on double {
  String toCurrency() {
    return NumberFormat.simpleCurrency(
      name: '${Storage.instance.getValue(Constants.currency)}',
    ).format(this / 100);
  }

  String formatNumber() {
    NumberFormat numberFormat = NumberFormat("#,##0.00", "en_US");
    // log((numberFormat.format(this)));
    return numberFormat.format(this);
  }
}

extension StringExtension on String {
  String useCorrectEllipsis() {
    return replaceAll('', '\u200B');
  }

  String formatNumber() {
    NumberFormat numberFormat = NumberFormat("#,##0.00", "en_US");
    log((numberFormat.format(this)));
    return numberFormat.format(this);
  }
}

bool isNullOrEmpty(dynamic x) {
  assert(x == null || x is String || x is List || x is Map || x is HashMap || x is Set);

  if (x == null) {
    return true;
  }

  if (x is String) {
    return x.isEmpty;
  }

  if (x is List) {
    return x.isEmpty;
  }

  if (x is Map) {
    return x.isEmpty;
  }

  if (x is HashMap) {
    return x.isEmpty;
  }

  if (x is Set) {
    return x.isEmpty;
  }

  return true;
}

bool isNotNullOrEmpty(dynamic x) {
  assert(x == null || x is String || x is List || x is Map || x is HashMap || x is Set);

  if (x == null) {
    return false;
  }

  if (x is String) {
    return x.isNotEmpty;
  }

  if (x is List) {
    return x.isNotEmpty;
  }

  if (x is Map) {
    return x.isNotEmpty;
  }

  if (x is HashMap) {
    return x.isNotEmpty;
  }

  if (x is Set) {
    return x.isNotEmpty;
  }

  return false;
}

bool isNull(dynamic x) {
  return x == null;
}

bool isNotNull(dynamic x) {
  return x != null;
}

/// Print Long String
void printLongString(String text) {
  final RegExp pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
  pattern.allMatches(text).forEach((RegExpMatch match) => print(match.group(0)));
}
