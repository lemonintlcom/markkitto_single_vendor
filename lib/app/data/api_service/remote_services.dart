import 'package:dio/dio.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import 'HeaderInterceptor.dart';

class RemoteService {
  static Dio client = Dio(getBaseOption())
    ..interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      compact: false,
    ))
    ..interceptors.add(HeaderInterceptor());

  static Future<Response?> getHTTP(String url) async {
    try {
      Response response = await client.get(url);
      return response;
    } on DioError catch (e) {
      // Handle error
      e.stackTrace;
    }
    return null;
  }

  static Future<Response?> postHTTP(String url, dynamic data) async {
    try {
      Response response = await client.post(url, data: data);

      return response;
    } on DioError catch (e) {
      print('tt${e.message}');
      e.stackTrace;
      // Handle error
    }
    return null;
  }

  static Future<Response?> putHTTP(String url, dynamic data) async {
    try {
      Response response = await client.put(url, data: data);
      return response;
    } on DioError catch (e) {
      // Handle error
    }
    return null;
  }

  static Future<Response?> deleteHTTP(String url) async {
    try {
      Response response = await client.delete(url);
      return response;
    } on DioError catch (e) {
      // Handle error
    }
  }

  static BaseOptions? getBaseOption() {
    var box = Hive.box(Constants.configName);
    return BaseOptions(
      baseUrl: box.get(Constants.baseUrl + "api/", defaultValue: Endpoints.baseUrl + "api/"),
      responseType: ResponseType.plain,
      // headers: {
      //   'Authorization': token,
      // }
    );
  }
}
