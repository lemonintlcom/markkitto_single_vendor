/// statusCode : 200
/// data : {"customerRegisterStatus":"0"}

class LoginOtp {
  LoginOtp({
      this.statusCode, 
      this.data,});

  LoginOtp.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? statusCode;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }

}

/// customerRegisterStatus : "0"

class Data {
  Data({
      this.customerRegisterStatus,});

  Data.fromJson(dynamic json) {
    customerRegisterStatus = json['customerRegisterStatus'];
  }
  int? customerRegisterStatus;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['customerRegisterStatus'] = customerRegisterStatus;
    return map;
  }

}