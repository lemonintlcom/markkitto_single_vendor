/// statusCode : 200
/// data : {"status":"success","UserId":"24"}

class CustomerRegistrationResponse {
  CustomerRegistrationResponse({
    this.statusCode,
    this.data,
  });

  CustomerRegistrationResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? statusCode;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// status : "success"
/// UserId : "24"

class Data {
  Data({
    this.status,
    this.userId,
  });

  Data.fromJson(dynamic json) {
    status = json['status'];
    userId = json['UserId'];
  }
  String? status;
  int? userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    map['UserId'] = userId;
    return map;
  }
}
