/// statusCode : 200
/// data : [{"Order_id":"OrD1047","Order_time":"2022-03-07 at 4:46 pm","total_price":408,"Delivered_time":"","delivery_status":"Ordered","Product_id":"","Product_thumb":"http://india.markkito.com/assets/upload/image/60d410c0c5331.png","Product_name":"1 X Amul Mithai Mate (1 Kg)","Items":[{"Product_id":3953,"Item_name":"1XAmul Mithai Mate (1 Kg)","Product_thumb":"http://india.markkito.com/assets/upload/image/60d410c0c5331.png","Product_price":208},{"Product_id":3843,"Item_name":"1XSweet Potato Purple Yam(1 Kg)","Product_thumb":"http://india.markkito.com/assets/upload/image/60e3e35f00d90.jpg","Product_price":200}]},{"Order_id":"OrD815","Order_time":"2022-01-29 at 1:53 pm","total_price":121,"Delivered_time":"","delivery_status":"Order Confirmed","Product_id":"","Product_thumb":"http://india.markkito.com/assets/upload/image/60dc4e22bd13f.jpg","Product_name":"1 X Camlin White Board Marker Blue(1+Pack)","Items":[{"Product_id":11257,"Item_name":"1XCamlin White Board Marker Blue(1+Pack)","Product_thumb":"http://india.markkito.com/assets/upload/image/60dc4e22bd13f.jpg","Product_price":95},{"Product_id":6025,"Item_name":"1XBisleri Water (2+Ltr)","Product_thumb":"http://india.markkito.com/assets/upload/image/60d417f8c21ff.png","Product_price":26}]},{"Order_id":"OrD779","Order_time":"2021-11-17 at 2:39 pm","total_price":185,"Delivered_time":"31-Dec-2121 09:59 am","delivery_status":"Order Delivered","Product_id":"","Product_thumb":"http://india.markkito.com/assets/upload/image/60d58ff0ea253.jpg","Product_name":"1 X Mirinda Soft Drink  Orange1(1 Ltr)","Items":[{"Product_id":4487,"Item_name":"1XMirinda Soft Drink  Orange1(1 Ltr)","Product_thumb":"http://india.markkito.com/assets/upload/image/60d58ff0ea253.jpg","Product_price":65},{"Product_id":4464,"Item_name":"1XLehar Soda (1 No)","Product_thumb":"http://india.markkito.com/assets/upload/image/60d57780e89b4.jpg","Product_price":65},{"Product_id":4463,"Item_name":"1XAppy Fizz (1 Grm)","Product_thumb":"http://india.markkito.com/assets/upload/image/60d5774d1556e.jpg","Product_price":55}]},{"Order_id":"OrD778","Order_time":"2021-11-17 at 2:37 pm","total_price":191,"Delivered_time":"17-Nov-2121 03:39 pm","delivery_status":"Order Delivered","Product_id":"","Product_thumb":"http://india.markkito.com/assets/upload/image/60dc4e22bd13f.jpg","Product_name":"1 X Camlin White Board Marker Blue(1 Pack)","Items":[{"Product_id":11257,"Item_name":"1XCamlin White Board Marker Blue(1 Pack)","Product_thumb":"http://india.markkito.com/assets/upload/image/60dc4e22bd13f.jpg","Product_price":95},{"Product_id":6025,"Item_name":"1XBisleri Water (2 Ltr)","Product_thumb":"http://india.markkito.com/assets/upload/image/60d417f8c21ff.png","Product_price":26},{"Product_id":4472,"Item_name":"1X7up  Party(1 Ml)","Product_thumb":"http://india.markkito.com/assets/upload/image/60d41a17b1b7a.jpg","Product_price":70}]},{"Order_id":"OrD777","Order_time":"2021-11-17 at 1:49 pm","total_price":149,"Delivered_time":"","delivery_status":"Order Cancelled","Product_id":"","Product_thumb":"http://india.markkito.com/assets/upload/image/60d417f8c21ff.png","Product_name":"1 X Bisleri Water (2 Ltr)","Items":[{"Product_id":6025,"Item_name":"1XBisleri Water (2 Ltr)","Product_thumb":"http://india.markkito.com/assets/upload/image/60d417f8c21ff.png","Product_price":26},{"Product_id":4464,"Item_name":"1XLehar Soda (1 No)","Product_thumb":"http://india.markkito.com/assets/upload/image/60d57780e89b4.jpg","Product_price":65},{"Product_id":4486,"Item_name":"1X Water Large(1 Ltr)","Product_thumb":"http://india.markkito.com/assets/upload/image/60d57fbbf2563.jpg","Product_price":13},{"Product_id":3659,"Item_name":"1XJaya Rice (1 Kg)","Product_thumb":"http://india.markkito.com/assets/upload/image/5fa7c5f47b38a.jpeg","Product_price":45}]}]

class OrderListResponse {
  OrderListResponse({
    this.statusCode,
    this.ordersList,
  });

  OrderListResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    if (json['data'] != null) {
      ordersList = [];
      json['data'].forEach((v) {
        ordersList?.add(Order.fromJson(v));
      });
    }
  }
  int? statusCode;
  List<Order>? ordersList;
  OrderListResponse copyWith({
    int? statusCode,
    List<Order>? ordersList,
  }) =>
      OrderListResponse(
        statusCode: statusCode ?? this.statusCode,
        ordersList: ordersList ?? this.ordersList,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (ordersList != null) {
      map['data'] = ordersList?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// Order_id : "OrD1047"
/// Order_time : "2022-03-07 at 4:46 pm"
/// total_price : 408
/// Delivered_time : ""
/// delivery_status : "Ordered"
/// Product_id : ""
/// Product_thumb : "http://india.markkito.com/assets/upload/image/60d410c0c5331.png"
/// Product_name : "1 X Amul Mithai Mate (1 Kg)"
/// Items : [{"Product_id":3953,"Item_name":"1XAmul Mithai Mate (1 Kg)","Product_thumb":"http://india.markkito.com/assets/upload/image/60d410c0c5331.png","Product_price":208},{"Product_id":3843,"Item_name":"1XSweet Potato Purple Yam(1 Kg)","Product_thumb":"http://india.markkito.com/assets/upload/image/60e3e35f00d90.jpg","Product_price":200}]

class Order {
  Order({
    this.orderId,
    this.orderTime,
    this.totalPrice,
    this.deliveredTime,
    this.deliveryStatus,
    this.productId,
    this.productThumb,
    this.productName,
    this.items,
  });

  Order.fromJson(dynamic json) {
    orderId = json['Order_id'];
    orderTime = json['Order_time'];
    totalPrice = json['total_price'];
    deliveredTime = json['Delivered_time'];
    deliveryStatus = json['delivery_status'];
    productId = json['Product_id'];
    productThumb = json['Product_thumb'];
    productName = json['Product_name'];
    if (json['Items'] != null) {
      items = [];
      json['Items'].forEach((v) {
        items?.add(Items.fromJson(v));
      });
    }
  }
  String? orderId;
  String? orderTime;
  dynamic totalPrice;
  String? deliveredTime;
  String? deliveryStatus;
  int? productId;
  String? productThumb;
  String? productName;
  List<Items>? items;
  Order copyWith({
    String? orderId,
    String? orderTime,
    dynamic totalPrice,
    String? deliveredTime,
    String? deliveryStatus,
    int? productId,
    String? productThumb,
    String? productName,
    List<Items>? items,
  }) =>
      Order(
        orderId: orderId ?? this.orderId,
        orderTime: orderTime ?? this.orderTime,
        totalPrice: totalPrice ?? this.totalPrice,
        deliveredTime: deliveredTime ?? this.deliveredTime,
        deliveryStatus: deliveryStatus ?? this.deliveryStatus,
        productId: productId ?? this.productId,
        productThumb: productThumb ?? this.productThumb,
        productName: productName ?? this.productName,
        items: items ?? this.items,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Order_id'] = orderId;
    map['Order_time'] = orderTime;
    map['total_price'] = totalPrice;
    map['Delivered_time'] = deliveredTime;
    map['delivery_status'] = deliveryStatus;
    map['Product_id'] = productId;
    map['Product_thumb'] = productThumb;
    map['Product_name'] = productName;
    if (items != null) {
      map['Items'] = items?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// Product_id : 3953
/// Item_name : "1XAmul Mithai Mate (1 Kg)"
/// Product_thumb : "http://india.markkito.com/assets/upload/image/60d410c0c5331.png"
/// Product_price : 208

class Items {
  Items({
    this.productId,
    this.itemName,
    this.productThumb,
    this.productPrice,
  });

  Items.fromJson(dynamic json) {
    productId = json['Product_id'];
    itemName = json['Item_name'];
    productThumb = json['Product_thumb'];
    productPrice = json['Product_price'];
  }
  int? productId;
  String? itemName;
  String? productThumb;
  dynamic productPrice;
  Items copyWith({
    int? productId,
    String? itemName,
    String? productThumb,
    dynamic productPrice,
  }) =>
      Items(
        productId: productId ?? this.productId,
        itemName: itemName ?? this.itemName,
        productThumb: productThumb ?? this.productThumb,
        productPrice: productPrice ?? this.productPrice,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Product_id'] = productId;
    map['Item_name'] = itemName;
    map['Product_thumb'] = productThumb;
    map['Product_price'] = productPrice;
    return map;
  }
}
