/// statusCode : 200
/// data : "Cart items removed"

class ClearCartResponse {
  ClearCartResponse({
    this.statusCode,
    this.data,
  });

  ClearCartResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'];
  }
  int? statusCode;
  String? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    map['data'] = data;
    return map;
  }
}
