/// statusCode : 200
/// data : {"status":"success","msg":"successfully delete Your File"}

class DeleteAddressResponse {
  DeleteAddressResponse({
    this.statusCode,
    this.data,
  });

  DeleteAddressResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? statusCode;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// status : "success"
/// msg : "successfully delete Your File"

class Data {
  Data({
    this.status,
    this.msg,
  });

  Data.fromJson(dynamic json) {
    status = json['status'];
    msg = json['msg'];
  }
  String? status;
  String? msg;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    map['msg'] = msg;
    return map;
  }
}
