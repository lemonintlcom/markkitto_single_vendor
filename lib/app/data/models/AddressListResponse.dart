/// statusCode : 200
/// data : {"default_id":"","Address":[{"AddressId":"FLaddress24","latitude":"31.6318889","Longitude":"76.4020704","address1":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India\n (Kashmir) ","address2":"hh","contact1":"9744904818","contact2":""}]}

class AddressListResponse {
  AddressListResponse({
    this.statusCode,
    this.data,
  });

  AddressListResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  int? statusCode;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// default_id : ""
/// Address : [{"AddressId":"FLaddress24","latitude":"31.6318889","Longitude":"76.4020704","address1":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India\n (Kashmir) ","address2":"hh","contact1":"9744904818","contact2":""}]

class Data {
  Data({
    this.defaultId,
    this.address,
  });

  Data.fromJson(dynamic json) {
    defaultId = json['default_id'];
    if (json['Address'] != null) {
      address = [];
      json['Address'].forEach((v) {
        address?.add(Address.fromJson(v));
      });
    }
  }

  String? defaultId;
  List<Address>? address;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['default_id'] = defaultId;
    if (address != null) {
      map['Address'] = address?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// AddressId : "FLaddress24"
/// latitude : "31.6318889"
/// Longitude : "76.4020704"
/// address1 : "Kashmir Rd, Kashmir, Himachal Pradesh 177006, India\n (Kashmir) "
/// address2 : "hh"
/// contact1 : "9744904818"
/// contact2 : ""

class Address {
  Address({
    this.addressId,
    this.latitude,
    this.longitude,
    this.address1,
    this.address2,
    this.contact1,
    this.contact2,
  });

  Address.fromJson(dynamic json) {
    addressId = json['AddressId'];
    latitude = json['latitude'];
    longitude = json['Longitude'];
    address1 = json['address1'];
    address2 = json['address2'];
    contact1 = json['contact1'];
    contact2 = json['contact2'];
  }

  dynamic addressId;
  dynamic latitude;
  dynamic longitude;
  dynamic address1;
  dynamic address2;
  dynamic contact1;
  dynamic contact2;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['AddressId'] = addressId;
    map['latitude'] = latitude;
    map['Longitude'] = longitude;
    map['address1'] = address1;
    map['address2'] = address2;
    map['contact1'] = contact1;
    map['contact2'] = contact2;
    return map;
  }
}
