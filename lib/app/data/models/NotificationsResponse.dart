/// statusCode : 200
/// data : {"currentPage":1,"pageSize":10,"notifications":[{"Id":"6","user_id":"12","usertype":"vendor","created_at":"2022-02-03 11:37:40","title":"New Order Received","message":"","image":"","type":"order","is_seen":"False","device_type":""},{"Id":"7","user_id":"12","usertype":"vendor","created_at":"2022-02-03 11:38:01","title":"New Order Received","message":"","image":"","type":"order","is_seen":"False","device_type":""}]}

class NotificationsResponse {
  NotificationsResponse({
    this.statusCode,
    this.data,
  });

  NotificationsResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? statusCode;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// currentPage : 1
/// pageSize : 10
/// notifications : [{"Id":"6","user_id":"12","usertype":"vendor","created_at":"2022-02-03 11:37:40","title":"New Order Received","message":"","image":"","type":"order","is_seen":"False","device_type":""},{"Id":"7","user_id":"12","usertype":"vendor","created_at":"2022-02-03 11:38:01","title":"New Order Received","message":"","image":"","type":"order","is_seen":"False","device_type":""}]

class Data {
  Data({
    this.currentPage,
    this.pageSize,
    this.notifications,
  });

  Data.fromJson(dynamic json) {
    currentPage = json['currentPage'];
    pageSize = json['pageSize'];
    if (json['notifications'] != null) {
      notifications = [];
      json['notifications'].forEach((v) {
        notifications?.add(Notifications.fromJson(v));
      });
    }
  }
  int? currentPage;
  int? pageSize;
  List<Notifications>? notifications;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['currentPage'] = currentPage;
    map['pageSize'] = pageSize;
    if (notifications != null) {
      map['notifications'] = notifications?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// Id : "6"
/// user_id : "12"
/// usertype : "vendor"
/// created_at : "2022-02-03 11:37:40"
/// title : "New Order Received"
/// message : ""
/// image : ""
/// type : "order"
/// is_seen : "False"
/// device_type : ""

class Notifications {
  Notifications({
    this.id,
    this.userId,
    this.usertype,
    this.createdAt,
    this.title,
    this.message,
    this.image,
    this.type,
    this.isSeen,
    this.deviceType,
  });

  Notifications.fromJson(dynamic json) {
    id = json['Id'];
    userId = json['user_id'];
    usertype = json['usertype'];
    createdAt = json['created_at'];
    title = json['title'];
    message = json['message'];
    image = json['image'];
    type = json['type'];
    isSeen = json['is_seen'];
    deviceType = json['device_type'];
  }
  String? id;
  String? userId;
  String? usertype;
  String? createdAt;
  String? title;
  String? message;
  String? image;
  String? type;
  String? isSeen;
  String? deviceType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Id'] = id;
    map['user_id'] = userId;
    map['usertype'] = usertype;
    map['created_at'] = createdAt;
    map['title'] = title;
    map['message'] = message;
    map['image'] = image;
    map['type'] = type;
    map['is_seen'] = isSeen;
    map['device_type'] = deviceType;
    return map;
  }
}
