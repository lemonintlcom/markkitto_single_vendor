import 'package:markkito_customer/app/data/models/ShopDetailsResponse.dart';

/// statusCode : 200
/// data : {"productList":[{"id":81,"GenerateProductListId":198,"offer":12,"image":"https://uae.markkito.com/assets/upload/image/6220a4851c7f2.jpg","ImageList":"","product_name":"Bentley Absolute For Men Eau De Parfum 100ML","catagory_name":"Men's Perfume","price":246.4,"unit":"Ml","favouirte":0,"CurrencySymbol":"د.إ","CountryId":2,"isIncart":0,"priceList":[{"generatelistdetailsId":81,"price":246.4,"priceFor":"Ml","cartQty":0}]}],"page":1}

class BrandProductListResponse {
  BrandProductListResponse({
    this.statusCode,
    this.data,
  });

  BrandProductListResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? statusCode;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// productList : [{"id":81,"GenerateProductListId":198,"offer":12,"image":"https://uae.markkito.com/assets/upload/image/6220a4851c7f2.jpg","ImageList":"","product_name":"Bentley Absolute For Men Eau De Parfum 100ML","catagory_name":"Men's Perfume","price":246.4,"unit":"Ml","favouirte":0,"CurrencySymbol":"د.إ","CountryId":2,"isIncart":0,"priceList":[{"generatelistdetailsId":81,"price":246.4,"priceFor":"Ml","cartQty":0}]}]
/// page : 1

class Data {
  Data({
    this.productList,
    this.page,
  });

  Data.fromJson(dynamic json) {
    if (json['productList'] != null) {
      productList = [];
      json['productList'].forEach((v) {
        productList?.add(Products.fromJson(v));
      });
    }
    page = json['page'];
  }
  List<Products>? productList;
  int? page;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (productList != null) {
      map['productList'] = productList?.map((v) => v.toJson()).toList();
    }
    map['page'] = page;
    return map;
  }
}
