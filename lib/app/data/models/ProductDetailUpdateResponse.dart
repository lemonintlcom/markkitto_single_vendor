/// statusCode : 200
/// data : {"product_id":244,"GenerateProductListId":206,"product_name":"Chanel Allure For Men Eau De Toilette 150ML","product_price":250,"discount":37.5,"offer_text":"","favorite":0,"product_type":"Men's Perfume","product_description":"DESCRIPTION\r\nFragrance Description:\r\n\r\nScent Allure Cologne For Men By Chanel Was Delivered In 1999. Who Is It Best For? Well It Is A Smell Featured By A Bundle Of Fragrant Citrus, Sweet-smelling And Fruity Scented Tones That Will Bring An Erotic, Light And New Perfumed Sensation. Analyzing It Closer Once Applied You Will See A Waiting Nature Of Milder Fragranced Botanical, Fine And Warm Fiery Insights That Shroud A Quintessence Of Quieting, Warm And Rich Emotions.\r\n\r\nFragrance Family: Citrus\r\n\r\nSize: 150 ML\r\n\r\nFragrance Notes:\r\n\r\nTop Note: citrus Bargamia, lemon, mandarine, peach\r\n\r\nHeart Note: freesia, geranium, jasmine, rose\r\n\r\nBase Note: styrax Balsam, Tonkin Beans, vanilla, sandalwood\r\n\r\nFragrance Gender: Male\r\n\r\nAge Group: Any Age Group\r\n\r\nOccasion: Day\r\n\r\nSeason: Summer\r\n\r\nScent Sillage: Soft\r\n\r\nLaunch Year: 1999\r\n\r\nPerfumer: Jacques Polge \r\n\r\nOdour: Citrus And Aromatic\r\n\r\nSensation: Sensual And Light\r\n\r\nScent Longevity: Jacques Helleu\r\n\r\nDesign House: Chanel","isIncart":1,"priceList":[{"generatelistdetailsId":244,"price":212.5,"priceFor":"Ml","varientId":0,"toppingId":0,"crustId":0,"cartQty":3}],"thumb_list":["https://uae.markkito.com/assets/upload/image/6220c2b6a9248.jpg"],"shop_details":{"shopid":17,"shopname":"Aroma Gallery","shop_thumb":"623c70e3ee0a2.jpg","shop_category":"Perfumes"},"choice_of_crust":[{"id":1,"name":"Basic"},{"id":2,"name":"Mexican"},{"id":3,"name":"Combo"}],"topping":[{"id":1,"name":"Extra Cheese"},{"id":2,"name":"Veg Topping"},{"id":3,"name":"Mushroom Topping"},{"id":4,"name":"Non Veg Topping"}],"varient":[{"Title":"Color","Data":[{"varient_id":1,"varient":"Color","varient_name":"#ff80ed"},{"varient_id":2,"varient":"Color","varient_name":"#008080"},{"varient_id":3,"varient":"Color","varient_name":"#b0e0e6"},{"varient_id":4,"varient":"Color","varient_name":"#800080"}]},{"Title":"Size","Data":[{"varient_id":5,"varient":"Size","varient_name":"S"},{"varient_id":6,"varient":"Size","varient_name":"M"},{"varient_id":7,"varient":"Size","varient_name":"L"},{"varient_id":8,"varient":"Size","varient_name":"XL"}]}]}

class ProductDetailUpdateResponse {
  ProductDetailUpdateResponse({
    this.statusCode,
    this.productDetail,
  });

  ProductDetailUpdateResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    productDetail = json['data'] != null ? ProductDetail.fromJson(json['data']) : null;
  }
  int? statusCode;
  ProductDetail? productDetail;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (productDetail != null) {
      map['data'] = productDetail?.toJson();
    }
    return map;
  }
}

/// product_id : 244
/// GenerateProductListId : 206
/// product_name : "Chanel Allure For Men Eau De Toilette 150ML"
/// product_price : 250
/// discount : 37.5
/// offer_text : ""
/// favorite : 0
/// product_type : "Men's Perfume"
/// product_description : "DESCRIPTION\r\nFragrance Description:\r\n\r\nScent Allure Cologne For Men By Chanel Was Delivered In 1999. Who Is It Best For? Well It Is A Smell Featured By A Bundle Of Fragrant Citrus, Sweet-smelling And Fruity Scented Tones That Will Bring An Erotic, Light And New Perfumed Sensation. Analyzing It Closer Once Applied You Will See A Waiting Nature Of Milder Fragranced Botanical, Fine And Warm Fiery Insights That Shroud A Quintessence Of Quieting, Warm And Rich Emotions.\r\n\r\nFragrance Family: Citrus\r\n\r\nSize: 150 ML\r\n\r\nFragrance Notes:\r\n\r\nTop Note: citrus Bargamia, lemon, mandarine, peach\r\n\r\nHeart Note: freesia, geranium, jasmine, rose\r\n\r\nBase Note: styrax Balsam, Tonkin Beans, vanilla, sandalwood\r\n\r\nFragrance Gender: Male\r\n\r\nAge Group: Any Age Group\r\n\r\nOccasion: Day\r\n\r\nSeason: Summer\r\n\r\nScent Sillage: Soft\r\n\r\nLaunch Year: 1999\r\n\r\nPerfumer: Jacques Polge \r\n\r\nOdour: Citrus And Aromatic\r\n\r\nSensation: Sensual And Light\r\n\r\nScent Longevity: Jacques Helleu\r\n\r\nDesign House: Chanel"
/// isIncart : 1
/// priceList : [{"generatelistdetailsId":244,"price":212.5,"priceFor":"Ml","varientId":0,"toppingId":0,"crustId":0,"cartQty":3}]
/// thumb_list : ["https://uae.markkito.com/assets/upload/image/6220c2b6a9248.jpg"]
/// shop_details : {"shopid":17,"shopname":"Aroma Gallery","shop_thumb":"623c70e3ee0a2.jpg","shop_category":"Perfumes"}
/// choice_of_crust : [{"id":1,"name":"Basic"},{"id":2,"name":"Mexican"},{"id":3,"name":"Combo"}]
/// topping : [{"id":1,"name":"Extra Cheese"},{"id":2,"name":"Veg Topping"},{"id":3,"name":"Mushroom Topping"},{"id":4,"name":"Non Veg Topping"}]
/// varient : [{"Title":"Color","Data":[{"varient_id":1,"varient":"Color","varient_name":"#ff80ed"},{"varient_id":2,"varient":"Color","varient_name":"#008080"},{"varient_id":3,"varient":"Color","varient_name":"#b0e0e6"},{"varient_id":4,"varient":"Color","varient_name":"#800080"}]},{"Title":"Size","Data":[{"varient_id":5,"varient":"Size","varient_name":"S"},{"varient_id":6,"varient":"Size","varient_name":"M"},{"varient_id":7,"varient":"Size","varient_name":"L"},{"varient_id":8,"varient":"Size","varient_name":"XL"}]}]

class ProductDetail {
  ProductDetail({
    this.productId,
    this.generateProductListId,
    this.productName,
    this.productPrice,
    this.discount,
    this.offerText,
    this.favorite,
    this.productType,
    this.productDescription,
    this.isIncart,
    this.cartTempCount,
    this.priceList,
    this.thumbList,
    this.shopDetails,
    this.choiceOfCrust,
    this.topping,
    this.varient,
  });

  ProductDetail.fromJson(dynamic json) {
    productId = json['product_id'];
    generateProductListId = json['GenerateProductListId'];
    productName = json['product_name'];
    productPrice = json['product_price'];
    discount = json['discount'];
    offerText = json['offer_text'];
    favorite = json['favorite'];
    cartTempCount = json['cartTempCount'];
    productType = json['product_type'];
    productDescription = json['product_description'];
    isIncart = json['isIncart'];
    if (json['priceList'] != null) {
      priceList = [];
      json['priceList'].forEach((v) {
        priceList?.add(PriceList.fromJson(v));
      });
    }
    thumbList = json['thumb_list'] != null ? json['thumb_list'].cast<String>() : [];
    shopDetails = json['shop_details'] != null ? ShopDetails.fromJson(json['shop_details']) : null;
    if (json['choice_of_crust'] != null) {
      choiceOfCrust = [];
      json['choice_of_crust'].forEach((v) {
        choiceOfCrust?.add(ChoiceOfCrust.fromJson(v));
      });
    }
    if (json['topping'] != null) {
      topping = [];
      json['topping'].forEach((v) {
        topping?.add(Topping.fromJson(v));
      });
    }
    if (json['varient'] != null) {
      varient = [];
      json['varient'].forEach((v) {
        varient?.add(Varient.fromJson(v));
      });
    }
  }
  int? productId;
  int? generateProductListId;
  String? productName;
  int? productPrice;
  dynamic discount;
  String? offerText;
  int? favorite;
  int? cartTempCount;
  String? productType;
  String? productDescription;
  int? isIncart;
  List<PriceList>? priceList;
  List<String>? thumbList;
  ShopDetails? shopDetails;
  List<ChoiceOfCrust>? choiceOfCrust;
  List<Topping>? topping;
  List<Varient>? varient;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['product_id'] = productId;
    map['GenerateProductListId'] = generateProductListId;
    map['product_name'] = productName;
    map['product_price'] = productPrice;
    map['discount'] = discount;
    map['cartTempCount'] = cartTempCount;
    map['offer_text'] = offerText;
    map['favorite'] = favorite;
    map['product_type'] = productType;
    map['product_description'] = productDescription;
    map['isIncart'] = isIncart;
    if (priceList != null) {
      map['priceList'] = priceList?.map((v) => v.toJson()).toList();
    }
    map['thumb_list'] = thumbList;
    if (shopDetails != null) {
      map['shop_details'] = shopDetails?.toJson();
    }
    if (choiceOfCrust != null) {
      map['choice_of_crust'] = choiceOfCrust?.map((v) => v.toJson()).toList();
    }
    if (topping != null) {
      map['topping'] = topping?.map((v) => v.toJson()).toList();
    }
    if (varient != null) {
      map['varient'] = varient?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// Title : "Color"
/// Data : [{"varient_id":1,"varient":"Color","varient_name":"#ff80ed"},{"varient_id":2,"varient":"Color","varient_name":"#008080"},{"varient_id":3,"varient":"Color","varient_name":"#b0e0e6"},{"varient_id":4,"varient":"Color","varient_name":"#800080"}]

class Varient {
  Varient({
    this.title,
    this.varientData,
  });

  Varient.fromJson(dynamic json) {
    title = json['Title'];
    if (json['Data'] != null) {
      varientData = [];
      json['Data'].forEach((v) {
        varientData?.add(VarientData.fromJson(v));
      });
    }
  }
  String? title;
  List<VarientData>? varientData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Title'] = title;
    if (varientData != null) {
      map['Data'] = varientData?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// varient_id : 1
/// varient : "Color"
/// varient_name : "#ff80ed"

class VarientData {
  VarientData({
    this.varientId,
    this.varient,
    this.varientName,
    this.checked = false,
  });

  @override
  String toString() {
    return '$varientName';
  }

  VarientData.fromJson(dynamic json) {
    varientId = json['varient_id'];
    varient = json['varient'];
    varientName = json['varient_name'];
  }
  int? varientId;
  String? varient;
  String? varientName;
  bool checked = false;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['varient_id'] = varientId;
    map['varient'] = varient;
    map['varient_name'] = varientName;
    return map;
  }
}

/// id : 1
/// name : "Extra Cheese"

class Topping {
  Topping({
    this.id,
    this.name,
    this.checked = false,
  });

  Topping.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
  }
  int? id;
  String? name;
  bool checked = false;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    return map;
  }
}

/// id : 1
/// name : "Basic"

class ChoiceOfCrust {
  ChoiceOfCrust({
    this.id,
    this.name,
    this.checked = false,
  });

  ChoiceOfCrust.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
  }
  int? id;
  String? name;
  bool checked = false;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    return map;
  }
}

/// shopid : 17
/// shopname : "Aroma Gallery"
/// shop_thumb : "623c70e3ee0a2.jpg"
/// shop_category : "Perfumes"

class ShopDetails {
  ShopDetails({
    this.shopid,
    this.shopname,
    this.shopThumb,
    this.shopCategory,
  });

  ShopDetails.fromJson(dynamic json) {
    shopid = json['shopid'];
    shopname = json['shopname'];
    shopThumb = json['shop_thumb'];
    shopCategory = json['shop_category'];
  }
  int? shopid;
  String? shopname;
  String? shopThumb;
  String? shopCategory;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['shopid'] = shopid;
    map['shopname'] = shopname;
    map['shop_thumb'] = shopThumb;
    map['shop_category'] = shopCategory;
    return map;
  }
}

/// generatelistdetailsId : 244
/// price : 212.5
/// priceFor : "Ml"
/// varientId : 0
/// toppingId : 0
/// crustId : 0
/// cartQty : 3

class PriceList {
  PriceList({
    this.generatelistdetailsId,
    this.price,
    this.priceFor,
    this.cartQty,
    this.type,
    this.value,
    this.color,
  });

  PriceList.fromJson(dynamic json) {
    generatelistdetailsId = json['generatelistdetailsId'];
    price = json['price'];
    priceFor = json['priceFor'];
    cartQty = json['cartQty'];
    type = json['type'];
    value = json['value'];
    color = json['Color'];
  }
  int? generatelistdetailsId;
  dynamic price;
  String? priceFor;
  int? cartQty;
  String? type;
  String? value;
  String? color;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['generatelistdetailsId'] = generatelistdetailsId;
    map['price'] = price;
    map['priceFor'] = priceFor;
    map['cartQty'] = cartQty;
    map['type'] = type;
    map['value'] = value;
    map['color'] = color;
    return map;
  }
}
