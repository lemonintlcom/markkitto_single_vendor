/// statusCode : 200
/// data : {"cart_datas":{"total_amount":500,"total_qty":2,"total_savings":75},"message":"Successfully added to cart"}

class CartStatusResponse {
  CartStatusResponse({
    this.statusCode,
    this.data,
  });

  CartStatusResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? statusCode;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// cart_datas : {"total_amount":500,"total_qty":2,"total_savings":75}
/// message : "Successfully added to cart"

class Data {
  Data({
    this.cartDatas,
    this.message,
  });

  Data.fromJson(dynamic json) {
    cartDatas = json['cart_datas'] != null ? CartDatas.fromJson(json['cart_datas']) : null;
    message = json['message'];
  }
  CartDatas? cartDatas;
  String? message;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (cartDatas != null) {
      map['cart_datas'] = cartDatas?.toJson();
    }
    map['message'] = message;
    return map;
  }
}

/// total_amount : 500
/// total_qty : 2
/// total_savings : 75

class CartDatas {
  CartDatas({
    this.totalAmount,
    this.totalQty,
    this.totalSavings,
  });

  CartDatas.fromJson(dynamic json) {
    totalAmount = json['total_amount'];
    totalQty = json['total_qty'];
    totalSavings = json['total_savings'];
  }
  dynamic totalAmount;
  dynamic totalQty;
  dynamic totalSavings;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['total_amount'] = totalAmount;
    map['total_qty'] = totalQty;
    map['total_savings'] = totalSavings;
    return map;
  }
}
