/// statusCode : 200
/// data : {"colorCode":"#FFFFFF","appName":"Star supermarket","storeName":"Aswathi Vendor","tagLine":"All in one store","IsActive":1,"CurrencySymbol":"₹ ","currency":"INR","country":"India","countryCode":91,"location":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India (Kashmir) ","storeId":25,"latitude":31.6318889,"longitude":76.4020704,"contact1":9744904818,"contact2":"","email":"abc@gmail.com","Logo":"http://india.markkito.com/assets/upload/image/5f917ffd66c0c.jpg","storeType":"Book and stationery ","base_url":"http://india.markkito.com/"}

class AppConfigResponse {
  AppConfigResponse({
    this.statusCode,
    this.data,
  });

  AppConfigResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? statusCode;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// colorCode : "#FFFFFF"
/// appName : "Star supermarket"
/// storeName : "Aswathi Vendor"
/// tagLine : "All in one store"
/// IsActive : 1
/// CurrencySymbol : "₹ "
/// currency : "INR"
/// country : "India"
/// countryCode : 91
/// location : "Kashmir Rd, Kashmir, Himachal Pradesh 177006, India (Kashmir) "
/// storeId : 25
/// latitude : 31.6318889
/// longitude : 76.4020704
/// contact1 : 9744904818
/// contact2 : ""
/// email : "abc@gmail.com"
/// Logo : "http://india.markkito.com/assets/upload/image/5f917ffd66c0c.jpg"
/// storeType : "Book and stationery "
/// base_url : "http://india.markkito.com/"

class Data {
  Data({
    this.colorCode,
    this.appName,
    this.storeName,
    this.displayName,
    this.tagLine,
    this.isActive,
    this.currencySymbol,
    this.currency,
    this.country,
    this.countryCode,
    this.location,
    this.storeId,
    this.latitude,
    this.GlobalCountryCode,
    this.CountryId,
    this.whatsappNo,
    this.terms_condition,
    this.longitude,
    this.privacy_policy,
    this.contact1,
    this.contact2,
    this.email,
    this.logo,
    this.storeType,
    this.baseUrl,
    this.gauth_enabled,
    this.vendorCount,
    this.customerCount,
  });

  Data.fromJson(dynamic json) {
    colorCode = json['colorCode'];
    appName = json['appName'];
    storeName = json['storeName'];
    displayName = json['displayName'];
    tagLine = json['tagLine'];
    isActive = json['IsActive'];
    currencySymbol = json['CurrencySymbol'];
    terms_condition = json['terms_condition'];
    currency = json['currency'];
    country = json['country'];
    countryCode = json['countryCode'];
    location = json['location'];
    storeId = json['storeId'];
    whatsappNo = json['whatsappNo'];
    GlobalCountryCode = json['GlobalCountryCode'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    privacy_policy = json['privacy_policy'];
    contact1 = json['contact1'];
    contact2 = json['contact2'];
    CountryId = json['CountryId'];
    email = json['email'];
    logo = json['Logo'];
    storeType = json['storeType'];
    baseUrl = json['base_url'];
    gauth_enabled = json['gauth_enabled'];
    vendorCount = json['vendorCount'];
    customerCount = json['customerCount'];
  }
  String? colorCode;
  String? appName;
  String? storeName;
  String? displayName;
  String? tagLine;
  int? isActive;
  String? currencySymbol;
  String? currency;
  String? country;
  String? terms_condition;
  dynamic countryCode;
  dynamic CountryId;
  dynamic whatsappNo;
  String? location;
  String? GlobalCountryCode;
  String? privacy_policy;
  dynamic storeId;
  double? latitude;
  double? longitude;
  dynamic contact1;
  dynamic contact2;
  String? email;
  String? logo;
  String? storeType;
  String? baseUrl;
  int? gauth_enabled;
  int? vendorCount;
  int? customerCount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['colorCode'] = colorCode;
    map['appName'] = appName;
    map['storeName'] = storeName;
    map['storeName'] = displayName;
    map['GlobalCountryCode'] = GlobalCountryCode;
    map['tagLine'] = tagLine;
    map['terms_condition'] = terms_condition;
    map['privacy_policy'] = privacy_policy;
    map['IsActive'] = isActive;
    map['CurrencySymbol'] = currencySymbol;
    map['currency'] = currency;
    map['country'] = country;
    map['countryCode'] = countryCode;
    map['CountryId'] = CountryId;
    map['location'] = location;
    map['storeId'] = storeId;
    map['latitude'] = latitude;
    map['longitude'] = longitude;
    map['contact1'] = contact1;
    map['contact2'] = contact2;
    map['whatsappNo'] = whatsappNo;
    map['email'] = email;
    map['Logo'] = logo;
    map['storeType'] = storeType;
    map['base_url'] = baseUrl;
    map['gauth_enabled'] = gauth_enabled;
    map['vendorCount'] = vendorCount;
    map['customerCount'] = customerCount;
    return map;
  }
}
