/// statusCode : 200
/// data : "success"

class ProfileUpdateResponse {
  ProfileUpdateResponse({
    this.statusCode,
    this.data,
  });

  ProfileUpdateResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'];
  }
  int? statusCode;
  String? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    map['data'] = data;
    return map;
  }
}
