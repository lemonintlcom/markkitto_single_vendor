/// statusCode : 200
/// data : {"message":"success","Image":"https://uae.markkito.com/assets/upload/image/629773c1e3e36.jpg"}

class ProfileUpdatedResponse {
  ProfileUpdatedResponse({
    this.statusCode,
    this.data,
  });

  ProfileUpdatedResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? statusCode;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// message : "success"
/// Image : "https://uae.markkito.com/assets/upload/image/629773c1e3e36.jpg"

class Data {
  Data({
    this.message,
    this.image,
  });

  Data.fromJson(dynamic json) {
    message = json['message'];
    image = json['Image'];
  }
  String? message;
  String? image;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = message;
    map['Image'] = image;
    return map;
  }
}
