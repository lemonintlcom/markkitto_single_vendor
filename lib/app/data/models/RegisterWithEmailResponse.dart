import 'package:markkito_customer/app/data/UserData.dart';

/// statusCode : 200
/// data : [{"auth":"7933c1ae5cb0d91e01dca300bad6ec3821a068314be3e1e2bac48af16639fe4b788d6c060adcfb71756440fa95a309c53466a150a5c9a79fc00f412aaf8f3295nhVYkYk0eOu059QrbAMgCDfnwf7quIiH2Qpkdo5rSju/v/oAoCgozm0h+RbtbgSHKiweoZJthuSrdqhk/Wmwp9xoq+/VIwZ4epL4Jzm1Ahh38RvhLq0rgQqzoyYHcniZ+8vh4D7uRSWq7nRe2vgABwCa11hFc8fl7VqcedqDGmmSVE96Fg44JyKchgO0ihdZe/CtSomJFMVSspF6kqMnJ+INJyc4AkKojIrZ0iEPaZkMNe++e+zC69jgE63avAMRWtk1GWUBIzTjOXrcUlPLBPEjBq7cRmMCR+KAhs3rBdng0+63f/Txw2nxCbI+x66JrapbvEg=","UserTypeId":33,"StatusId":1,"CountryId":107,"UserMasterId":24,"UserId":24,"Name":"Aswathi Sujith","ProfilePic":null,"Contact1":9744904818,"Contact2":"","AreaCode":"","Gender":"F","DOB":"17-12-1994","Email":"aswathimohandasv@gmail.com","MobileNo":9744904818,"OtpVerification":1,"UserTypeName":"CUSTOMER"}]

class RegisterWithEmailResponse {
  RegisterWithEmailResponse({
    this.statusCode,
    this.user,
  });

  RegisterWithEmailResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    if (json['data'] != null) {
      user = [];
      json['data'].forEach((v) {
        user?.add(User.fromJson(v));
      });
    }
  }
  int? statusCode;
  List<User>? user;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (user != null) {
      map['data'] = user?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// auth : "7933c1ae5cb0d91e01dca300bad6ec3821a068314be3e1e2bac48af16639fe4b788d6c060adcfb71756440fa95a309c53466a150a5c9a79fc00f412aaf8f3295nhVYkYk0eOu059QrbAMgCDfnwf7quIiH2Qpkdo5rSju/v/oAoCgozm0h+RbtbgSHKiweoZJthuSrdqhk/Wmwp9xoq+/VIwZ4epL4Jzm1Ahh38RvhLq0rgQqzoyYHcniZ+8vh4D7uRSWq7nRe2vgABwCa11hFc8fl7VqcedqDGmmSVE96Fg44JyKchgO0ihdZe/CtSomJFMVSspF6kqMnJ+INJyc4AkKojIrZ0iEPaZkMNe++e+zC69jgE63avAMRWtk1GWUBIzTjOXrcUlPLBPEjBq7cRmMCR+KAhs3rBdng0+63f/Txw2nxCbI+x66JrapbvEg="
/// UserTypeId : 33
/// StatusId : 1
/// CountryId : 107
/// UserMasterId : 24
/// UserId : 24
/// Name : "Aswathi Sujith"
/// ProfilePic : null
/// Contact1 : 9744904818
/// Contact2 : ""
/// AreaCode : ""
/// Gender : "F"
/// DOB : "17-12-1994"
/// Email : "aswathimohandasv@gmail.com"
/// MobileNo : 9744904818
/// OtpVerification : 1
/// UserTypeName : "CUSTOMER"
