/// statusCode : 200
/// data : {"status":"Order Successfully Placed","OrderDetails":{"OrderID":"OrD1079","TotalAmount":329.32}}

class UpdatePaymentRefResponse {
  UpdatePaymentRefResponse({
    this.statusCode,
    this.data,
  });

  UpdatePaymentRefResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? statusCode;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// status : "Order Successfully Placed"
/// OrderDetails : {"OrderID":"OrD1079","TotalAmount":329.32}

class Data {
  Data({
    this.status,
    this.orderDetails,
  });

  Data.fromJson(dynamic json) {
    status = json['status'];
    orderDetails = json['OrderDetails'] != null ? OrderDetails.fromJson(json['OrderDetails']) : null;
  }
  String? status;
  OrderDetails? orderDetails;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    if (orderDetails != null) {
      map['OrderDetails'] = orderDetails?.toJson();
    }
    return map;
  }
}

/// OrderID : "OrD1079"
/// TotalAmount : 329.32

class OrderDetails {
  OrderDetails({
    this.orderID,
    this.totalAmount,
  });

  OrderDetails.fromJson(dynamic json) {
    orderID = json['OrderID'];
    totalAmount = json['TotalAmount'];
  }
  String? orderID;
  double? totalAmount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['OrderID'] = orderID;
    map['TotalAmount'] = totalAmount;
    return map;
  }
}
