import 'package:markkito_customer/app/data/models/HomePageResponse.dart';
import 'package:markkito_customer/app/data/models/ShopDetailsResponse.dart';

/// statusCode : 200
/// data : {"shop_id":25,"shop_name":"Aswathi Vendor","place":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India (Kashmir) ","latitude":31.6318889,"longitude":76.4020704,"profilePic":"60d2c7c5d1344.jpg","type":"Book and stationery ","openingStatus":1,"rating":3,"delivery_time":20,"offer_text":"","offer_code":"","favorite":0,"contact":9744904818,"categories":[{"CategoryName":"Stationery","CategoryId":18,"ImagePath":"60d3522ad429e.jpg"},{"CategoryName":"Rice & Noodles","CategoryId":256,"ImagePath":"60eb585dcbf38.png"},{"CategoryName":"Gift","CategoryId":79,"ImagePath":"60d35e07d5302.jpg"},{"CategoryName":"Beverages","CategoryId":3,"ImagePath":"60d3401c643d8.jpg"},{"CategoryName":"Rice Flour Pasta & More","CategoryId":5,"ImagePath":"60d3407c2d329.jpg"},{"CategoryName":"Barbecue","CategoryId":252,"ImagePath":"60eb567467c4d.jpg"},{"CategoryName":"MOTORCYCLE ACCESSORIES & PARTS","CategoryId":95,"ImagePath":"60d35d0089afa.jpg"},{"CategoryName":"Fresh Juices","CategoryId":253,"ImagePath":"60eb569b36488.jpg"},{"CategoryName":"Cooking Ingredients","CategoryId":4,"ImagePath":"60d3404b89090.jpg"},{"CategoryName":"Household Care","CategoryId":16,"ImagePath":"60d350d10107b.jpg"},{"CategoryName":"Dairy & Eggs","CategoryId":2,"ImagePath":"60d33fc8122fb.jpg"},{"CategoryName":"Bakery","CategoryId":11,"ImagePath":"60d3439d1e7c4.jpg"}],"products":[{"id":7829,"GenerateProductListId":5822,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5ff3cdcb1762b.jpg","ImageList":"6152e705dfb2a.jpg","product_name":"Everest Chhole Masala ","catagory_name":"Cooking Ingredients","price":170,"unit":"Grm","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7829,"price":170,"priceFor":"Grm","cartQty":0},{"generatelistdetailsId":2379,"price":500,"priceFor":"Bag","cartQty":0}]},{"id":7828,"GenerateProductListId":14144,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d54d88dfdc0.jpg","ImageList":"","product_name":"Britannia Gobbles Butter Blast Cake","catagory_name":"Bakery","price":23.75,"unit":"Grm","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7828,"price":23.75,"priceFor":"Grm","cartQty":0}]},{"id":7827,"GenerateProductListId":897,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60dc497895b7d.jpg","ImageList":"","product_name":"Brown Tape 1 Inches","catagory_name":"Stationery","price":200,"unit":"Pack","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7827,"price":200,"priceFor":"Pack","cartQty":0},{"generatelistdetailsId":49,"price":75,"priceFor":"Pack","cartQty":0}]},{"id":7825,"GenerateProductListId":4488,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d5aa123ccb7.jpg","ImageList":"","product_name":"Carabao Sugar Free Energy Drink","catagory_name":"Beverages","price":63.75,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7825,"price":63.75,"priceFor":"Ltr","cartQty":0},{"generatelistdetailsId":1178,"price":40,"priceFor":"Ltr","cartQty":0}]},{"id":7824,"GenerateProductListId":14140,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d4141859ead.jpg","ImageList":"","product_name":"Amul Butter ","catagory_name":"Dairy & Eggs","price":25,"unit":"Grm","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7824,"price":25,"priceFor":"Grm","cartQty":0}]},{"id":7699,"GenerateProductListId":14122,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5ffec826246d5.jpg","ImageList":"","product_name":" FRUITOMANS ORANGE SQUASH","catagory_name":"Fresh Juices","price":21.44,"unit":"Ml","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7700,"price":21.439999999999998,"priceFor":"Ml","cartQty":0},{"generatelistdetailsId":7699,"price":21.439999999999998,"priceFor":"Ml","cartQty":0}]},{"id":7697,"GenerateProductListId":14115,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5ffec826246d5.jpg","ImageList":"","product_name":" FRUITOMANS ORANGE SQUASH","catagory_name":"Fresh Juices","price":21.44,"unit":"Ml","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7698,"price":21.439999999999998,"priceFor":"Ml","cartQty":0},{"generatelistdetailsId":7697,"price":21.439999999999998,"priceFor":"Ml","cartQty":0}]},{"id":7695,"GenerateProductListId":14114,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5ffec826246d5.jpg","ImageList":"","product_name":" FRUITOMANS ORANGE SQUASH","catagory_name":"Fresh Juices","price":21.44,"unit":"Ml","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7696,"price":21.439999999999998,"priceFor":"Ml","cartQty":0},{"generatelistdetailsId":7695,"price":21.439999999999998,"priceFor":"Ml","cartQty":0}]},{"id":7693,"GenerateProductListId":14113,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5ffec826246d5.jpg","ImageList":"","product_name":" FRUITOMANS ORANGE SQUASH","catagory_name":"Fresh Juices","price":21.44,"unit":"Ml","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7694,"price":21.439999999999998,"priceFor":"Ml","cartQty":0},{"generatelistdetailsId":7693,"price":21.439999999999998,"priceFor":"Ml","cartQty":0}]},{"id":7691,"GenerateProductListId":14107,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5ffec826246d5.jpg","ImageList":"","product_name":" FRUITOMANS ORANGE SQUASH","catagory_name":"Fresh Juices","price":21.44,"unit":"Ml","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7692,"price":21.439999999999998,"priceFor":"Ml","cartQty":0},{"generatelistdetailsId":7691,"price":21.439999999999998,"priceFor":"Ml","cartQty":0}]},{"id":7683,"GenerateProductListId":14102,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5ffec826246d5.jpg","ImageList":"","product_name":"FRUITOMANS ORANGE SQUASH ","catagory_name":"Fresh Juices","price":21.44,"unit":"Ml","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7684,"price":21.439999999999998,"priceFor":"Ml","cartQty":0},{"generatelistdetailsId":7683,"price":21.439999999999998,"priceFor":"Ml","cartQty":0}]},{"id":4996,"GenerateProductListId":11257,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60dc4e22bd13f.jpg","ImageList":"","product_name":"Camlin White Board Marker Blue","catagory_name":"Stationery","price":95,"unit":"Pack","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":4996,"price":95,"priceFor":"Pack","cartQty":0}]},{"id":3348,"GenerateProductListId":5472,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5fa45f51da6be.jpg","ImageList":"","product_name":"Tang Orange ","catagory_name":"Fresh Juices","price":105,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":3348,"price":105,"priceFor":"Ltr","cartQty":0},{"generatelistdetailsId":3347,"price":55,"priceFor":"Ml","cartQty":0}]},{"id":2611,"GenerateProductListId":6025,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d417f8c21ff.png","ImageList":"","product_name":"Bisleri Water ","catagory_name":"Beverages","price":26,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":2611,"price":26,"priceFor":"Ltr","cartQty":0},{"generatelistdetailsId":2610,"price":13,"priceFor":"Ltr","cartQty":0}]},{"id":1177,"GenerateProductListId":4487,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d58ff0ea253.jpg","ImageList":"6131bf9be37e0.jpg,6131bf9c04bcc.jpg","product_name":"Mirinda Soft Drink  Orange1","catagory_name":"Beverages","price":65,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1177,"price":65,"priceFor":"Ltr","cartQty":0}]},{"id":1175,"GenerateProductListId":4485,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60dd870bf3aba.jpg","ImageList":"","product_name":"Stick File A4 Size","catagory_name":"Stationery","price":25,"unit":"No","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1175,"price":25,"priceFor":"No","cartQty":0}]},{"id":1170,"GenerateProductListId":4474,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d41c656f07d.jpg","ImageList":"61305bb28192a.jpg,61305bb2b3d3b.jpg,61305bb2c551e.jpg","product_name":"Pepsi 1","catagory_name":"Beverages","price":65,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1170,"price":65,"priceFor":"Ltr","cartQty":0}]},{"id":1169,"GenerateProductListId":1920,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d41930e2147.jpg","ImageList":"612dce2d37ed2.jpg,612dce36d0313.jpg,612dce37089ee.jpg,61305b15a8bec.jpg,61305b15da384.jpg,61305b15da0b6.jpg","product_name":"7 Up","catagory_name":"Beverages","price":70,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1169,"price":70,"priceFor":"Ltr","cartQty":0}]},{"id":1167,"GenerateProductListId":4472,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d41a17b1b7a.jpg","ImageList":"61304eb14678b.jpg,61304eb16029c.jpg","product_name":"7up  Party","catagory_name":"Beverages","price":70,"unit":"Ml","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1167,"price":70,"priceFor":"Ml","cartQty":0}]},{"id":1158,"GenerateProductListId":4464,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d57780e89b4.jpg","ImageList":"","product_name":"Lehar Soda ","catagory_name":"Beverages","price":65,"unit":"No","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1158,"price":65,"priceFor":"No","cartQty":0}]},{"id":1157,"GenerateProductListId":4463,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d5774d1556e.jpg","ImageList":"","product_name":"Appy Fizz ","catagory_name":"Beverages","price":55,"unit":"Grm","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1157,"price":55,"priceFor":"Grm","cartQty":0}]},{"id":1155,"GenerateProductListId":4460,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60e2b51cc0e8d.jpg","ImageList":"","product_name":"DEMI JET HELMET FGTR CLASSIC MILITARY GREEN","catagory_name":"MOTORCYCLE ACCESSORIES & PARTS","price":1000,"unit":"No","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1155,"price":1000,"priceFor":"No","cartQty":0}]},{"id":1154,"GenerateProductListId":4459,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d5a9dc5f1b9.jpg","ImageList":"","product_name":"Fruits and Veggies with Mangosteen Health Drink -","catagory_name":"Beverages","price":100,"unit":"No","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1154,"price":100,"priceFor":"No","cartQty":0}]},{"id":745,"GenerateProductListId":3693,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60e4344f4daa7.jpg","ImageList":"","product_name":"Special Mix Grill","catagory_name":"Barbecue","price":500,"unit":"Pack","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":745,"price":500,"priceFor":"Pack","cartQty":0}]},{"id":708,"GenerateProductListId":3659,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5fa7c5f47b38a.jpeg","ImageList":"","product_name":"Jaya Rice ","catagory_name":"Rice Flour Pasta & More","price":45,"unit":"Kg","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":708,"price":45,"priceFor":"Kg","cartQty":0}]}],"cart_data":{"total_amount":0,"total_qty":0,"total_savings":0},"offers":[{"bannerId":14,"bannerName":"test111","imagePath":"http://india.markkito.com/assets/upload/image/623c18524d1cc.png","description":"description"}]}

class SingleVendorHomeResponse {
  SingleVendorHomeResponse({
    this.statusCode,
    this.shopData,
  });

  SingleVendorHomeResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    shopData = json['data'] != null ? VendorData.fromJson(json['data']) : null;
  }
  int? statusCode;
  VendorData? shopData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (shopData != null) {
      map['data'] = shopData?.toJson();
    }
    return map;
  }
}

/// shop_id : 25
/// shop_name : "Aswathi Vendor"
/// place : "Kashmir Rd, Kashmir, Himachal Pradesh 177006, India (Kashmir) "
/// latitude : 31.6318889
/// longitude : 76.4020704
/// profilePic : "60d2c7c5d1344.jpg"
/// type : "Book and stationery "
/// openingStatus : 1
/// rating : 3
/// delivery_time : 20
/// offer_text : ""
/// offer_code : ""
/// favorite : 0
/// contact : 9744904818
/// categories : [{"CategoryName":"Stationery","CategoryId":18,"ImagePath":"60d3522ad429e.jpg"},{"CategoryName":"Rice & Noodles","CategoryId":256,"ImagePath":"60eb585dcbf38.png"},{"CategoryName":"Gift","CategoryId":79,"ImagePath":"60d35e07d5302.jpg"},{"CategoryName":"Beverages","CategoryId":3,"ImagePath":"60d3401c643d8.jpg"},{"CategoryName":"Rice Flour Pasta & More","CategoryId":5,"ImagePath":"60d3407c2d329.jpg"},{"CategoryName":"Barbecue","CategoryId":252,"ImagePath":"60eb567467c4d.jpg"},{"CategoryName":"MOTORCYCLE ACCESSORIES & PARTS","CategoryId":95,"ImagePath":"60d35d0089afa.jpg"},{"CategoryName":"Fresh Juices","CategoryId":253,"ImagePath":"60eb569b36488.jpg"},{"CategoryName":"Cooking Ingredients","CategoryId":4,"ImagePath":"60d3404b89090.jpg"},{"CategoryName":"Household Care","CategoryId":16,"ImagePath":"60d350d10107b.jpg"},{"CategoryName":"Dairy & Eggs","CategoryId":2,"ImagePath":"60d33fc8122fb.jpg"},{"CategoryName":"Bakery","CategoryId":11,"ImagePath":"60d3439d1e7c4.jpg"}]
/// products : [{"id":7829,"GenerateProductListId":5822,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5ff3cdcb1762b.jpg","ImageList":"6152e705dfb2a.jpg","product_name":"Everest Chhole Masala ","catagory_name":"Cooking Ingredients","price":170,"unit":"Grm","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7829,"price":170,"priceFor":"Grm","cartQty":0},{"generatelistdetailsId":2379,"price":500,"priceFor":"Bag","cartQty":0}]},{"id":7828,"GenerateProductListId":14144,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d54d88dfdc0.jpg","ImageList":"","product_name":"Britannia Gobbles Butter Blast Cake","catagory_name":"Bakery","price":23.75,"unit":"Grm","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7828,"price":23.75,"priceFor":"Grm","cartQty":0}]},{"id":7827,"GenerateProductListId":897,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60dc497895b7d.jpg","ImageList":"","product_name":"Brown Tape 1 Inches","catagory_name":"Stationery","price":200,"unit":"Pack","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7827,"price":200,"priceFor":"Pack","cartQty":0},{"generatelistdetailsId":49,"price":75,"priceFor":"Pack","cartQty":0}]},{"id":7825,"GenerateProductListId":4488,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d5aa123ccb7.jpg","ImageList":"","product_name":"Carabao Sugar Free Energy Drink","catagory_name":"Beverages","price":63.75,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7825,"price":63.75,"priceFor":"Ltr","cartQty":0},{"generatelistdetailsId":1178,"price":40,"priceFor":"Ltr","cartQty":0}]},{"id":7824,"GenerateProductListId":14140,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d4141859ead.jpg","ImageList":"","product_name":"Amul Butter ","catagory_name":"Dairy & Eggs","price":25,"unit":"Grm","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7824,"price":25,"priceFor":"Grm","cartQty":0}]},{"id":7699,"GenerateProductListId":14122,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5ffec826246d5.jpg","ImageList":"","product_name":" FRUITOMANS ORANGE SQUASH","catagory_name":"Fresh Juices","price":21.44,"unit":"Ml","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7700,"price":21.439999999999998,"priceFor":"Ml","cartQty":0},{"generatelistdetailsId":7699,"price":21.439999999999998,"priceFor":"Ml","cartQty":0}]},{"id":7697,"GenerateProductListId":14115,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5ffec826246d5.jpg","ImageList":"","product_name":" FRUITOMANS ORANGE SQUASH","catagory_name":"Fresh Juices","price":21.44,"unit":"Ml","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7698,"price":21.439999999999998,"priceFor":"Ml","cartQty":0},{"generatelistdetailsId":7697,"price":21.439999999999998,"priceFor":"Ml","cartQty":0}]},{"id":7695,"GenerateProductListId":14114,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5ffec826246d5.jpg","ImageList":"","product_name":" FRUITOMANS ORANGE SQUASH","catagory_name":"Fresh Juices","price":21.44,"unit":"Ml","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7696,"price":21.439999999999998,"priceFor":"Ml","cartQty":0},{"generatelistdetailsId":7695,"price":21.439999999999998,"priceFor":"Ml","cartQty":0}]},{"id":7693,"GenerateProductListId":14113,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5ffec826246d5.jpg","ImageList":"","product_name":" FRUITOMANS ORANGE SQUASH","catagory_name":"Fresh Juices","price":21.44,"unit":"Ml","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7694,"price":21.439999999999998,"priceFor":"Ml","cartQty":0},{"generatelistdetailsId":7693,"price":21.439999999999998,"priceFor":"Ml","cartQty":0}]},{"id":7691,"GenerateProductListId":14107,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5ffec826246d5.jpg","ImageList":"","product_name":" FRUITOMANS ORANGE SQUASH","catagory_name":"Fresh Juices","price":21.44,"unit":"Ml","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7692,"price":21.439999999999998,"priceFor":"Ml","cartQty":0},{"generatelistdetailsId":7691,"price":21.439999999999998,"priceFor":"Ml","cartQty":0}]},{"id":7683,"GenerateProductListId":14102,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5ffec826246d5.jpg","ImageList":"","product_name":"FRUITOMANS ORANGE SQUASH ","catagory_name":"Fresh Juices","price":21.44,"unit":"Ml","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":7684,"price":21.439999999999998,"priceFor":"Ml","cartQty":0},{"generatelistdetailsId":7683,"price":21.439999999999998,"priceFor":"Ml","cartQty":0}]},{"id":4996,"GenerateProductListId":11257,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60dc4e22bd13f.jpg","ImageList":"","product_name":"Camlin White Board Marker Blue","catagory_name":"Stationery","price":95,"unit":"Pack","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":4996,"price":95,"priceFor":"Pack","cartQty":0}]},{"id":3348,"GenerateProductListId":5472,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5fa45f51da6be.jpg","ImageList":"","product_name":"Tang Orange ","catagory_name":"Fresh Juices","price":105,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":3348,"price":105,"priceFor":"Ltr","cartQty":0},{"generatelistdetailsId":3347,"price":55,"priceFor":"Ml","cartQty":0}]},{"id":2611,"GenerateProductListId":6025,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d417f8c21ff.png","ImageList":"","product_name":"Bisleri Water ","catagory_name":"Beverages","price":26,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":2611,"price":26,"priceFor":"Ltr","cartQty":0},{"generatelistdetailsId":2610,"price":13,"priceFor":"Ltr","cartQty":0}]},{"id":1177,"GenerateProductListId":4487,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d58ff0ea253.jpg","ImageList":"6131bf9be37e0.jpg,6131bf9c04bcc.jpg","product_name":"Mirinda Soft Drink  Orange1","catagory_name":"Beverages","price":65,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1177,"price":65,"priceFor":"Ltr","cartQty":0}]},{"id":1175,"GenerateProductListId":4485,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60dd870bf3aba.jpg","ImageList":"","product_name":"Stick File A4 Size","catagory_name":"Stationery","price":25,"unit":"No","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1175,"price":25,"priceFor":"No","cartQty":0}]},{"id":1170,"GenerateProductListId":4474,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d41c656f07d.jpg","ImageList":"61305bb28192a.jpg,61305bb2b3d3b.jpg,61305bb2c551e.jpg","product_name":"Pepsi 1","catagory_name":"Beverages","price":65,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1170,"price":65,"priceFor":"Ltr","cartQty":0}]},{"id":1169,"GenerateProductListId":1920,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d41930e2147.jpg","ImageList":"612dce2d37ed2.jpg,612dce36d0313.jpg,612dce37089ee.jpg,61305b15a8bec.jpg,61305b15da384.jpg,61305b15da0b6.jpg","product_name":"7 Up","catagory_name":"Beverages","price":70,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1169,"price":70,"priceFor":"Ltr","cartQty":0}]},{"id":1167,"GenerateProductListId":4472,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d41a17b1b7a.jpg","ImageList":"61304eb14678b.jpg,61304eb16029c.jpg","product_name":"7up  Party","catagory_name":"Beverages","price":70,"unit":"Ml","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1167,"price":70,"priceFor":"Ml","cartQty":0}]},{"id":1158,"GenerateProductListId":4464,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d57780e89b4.jpg","ImageList":"","product_name":"Lehar Soda ","catagory_name":"Beverages","price":65,"unit":"No","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1158,"price":65,"priceFor":"No","cartQty":0}]},{"id":1157,"GenerateProductListId":4463,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d5774d1556e.jpg","ImageList":"","product_name":"Appy Fizz ","catagory_name":"Beverages","price":55,"unit":"Grm","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1157,"price":55,"priceFor":"Grm","cartQty":0}]},{"id":1155,"GenerateProductListId":4460,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60e2b51cc0e8d.jpg","ImageList":"","product_name":"DEMI JET HELMET FGTR CLASSIC MILITARY GREEN","catagory_name":"MOTORCYCLE ACCESSORIES & PARTS","price":1000,"unit":"No","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1155,"price":1000,"priceFor":"No","cartQty":0}]},{"id":1154,"GenerateProductListId":4459,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60d5a9dc5f1b9.jpg","ImageList":"","product_name":"Fruits and Veggies with Mangosteen Health Drink -","catagory_name":"Beverages","price":100,"unit":"No","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":1154,"price":100,"priceFor":"No","cartQty":0}]},{"id":745,"GenerateProductListId":3693,"offer":0,"image":"http://india.markkito.com/assets/upload/image/60e4344f4daa7.jpg","ImageList":"","product_name":"Special Mix Grill","catagory_name":"Barbecue","price":500,"unit":"Pack","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":745,"price":500,"priceFor":"Pack","cartQty":0}]},{"id":708,"GenerateProductListId":3659,"offer":0,"image":"http://india.markkito.com/assets/upload/image/5fa7c5f47b38a.jpeg","ImageList":"","product_name":"Jaya Rice ","catagory_name":"Rice Flour Pasta & More","price":45,"unit":"Kg","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"isIncart":0,"priceList":[{"generatelistdetailsId":708,"price":45,"priceFor":"Kg","cartQty":0}]}]
/// cart_data : {"total_amount":0,"total_qty":0,"total_savings":0}
/// offers : [{"bannerId":14,"bannerName":"test111","imagePath":"http://india.markkito.com/assets/upload/image/623c18524d1cc.png","description":"description"}]

class VendorData {
  VendorData({
    this.shopId,
    this.shopName,
    this.place,
    this.latitude,
    this.longitude,
    this.profilePic,
    this.type,
    this.openingStatus,
    this.rating,
    this.deliveryTime,
    this.offerText,
    this.offerCode,
    this.favorite,
    this.contact,
    this.categories,
    this.products,
    this.cartData,
    this.offers,
    this.banners,
    this.brands,
    this.recentlyViewed,
  });

  VendorData.fromJson(dynamic json) {
    shopId = json['shop_id'];
    shopName = json['shop_name'];
    place = json['place'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    profilePic = json['profilePic'];
    type = json['type'];
    openingStatus = json['openingStatus'];
    rating = json['rating'];
    deliveryTime = json['delivery_time'];
    offerText = json['offer_text'];
    offerCode = json['offer_code'];
    favorite = json['favorite'];
    contact = json['contact'];
    if (json['categories'] != null) {
      categories = [];
      json['categories'].forEach((v) {
        categories?.add(Categories.fromJson(v));
      });
    }
    if (json['products'] != null) {
      products = [];
      json['products'].forEach((v) {
        products?.add(Products.fromJson(v));
      });
    }
    cartData = json['cart_data'] != null ? CartData.fromJson(json['cart_data']) : null;
    if (json['offers'] != null) {
      offers = [];
      json['offers'].forEach((v) {
        offers?.add(Offers.fromJson(v));
      });
    }
    if (json['banners'] != null) {
      banners = [];
      json['banners'].forEach((v) {
        banners?.add(BannerData.fromJson(v));
      });
    }
    if (json['brands'] != null) {
      brands = [];
      json['brands'].forEach((v) {
        brands?.add(Brands.fromJson(v));
      });
    }
    if (json['recently_viewed'] != null) {
      recentlyViewed = [];
      json['recently_viewed'].forEach((v) {
        recentlyViewed?.add(Products.fromJson(v));
      });
    }
  }
  int? shopId;
  String? shopName;
  String? place;
  double? latitude;
  double? longitude;
  String? profilePic;
  String? type;
  int? openingStatus;
  int? rating;
  int? deliveryTime;
  String? offerText;
  String? offerCode;
  int? favorite;
  int? contact;
  List<Categories>? categories;
  List<Products>? products;
  CartData? cartData;
  List<Offers>? offers;
  List<BannerData>? banners;
  List<Brands>? brands;
  List<Products>? recentlyViewed;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['shop_id'] = shopId;
    map['shop_name'] = shopName;
    map['place'] = place;
    map['latitude'] = latitude;
    map['longitude'] = longitude;
    map['profilePic'] = profilePic;
    map['type'] = type;
    map['openingStatus'] = openingStatus;
    map['rating'] = rating;
    map['delivery_time'] = deliveryTime;
    map['offer_text'] = offerText;
    map['offer_code'] = offerCode;
    map['favorite'] = favorite;
    map['contact'] = contact;
    if (categories != null) {
      map['categories'] = categories?.map((v) => v.toJson()).toList();
    }
    if (products != null) {
      map['products'] = products?.map((v) => v.toJson()).toList();
    }
    if (cartData != null) {
      map['cart_data'] = cartData?.toJson();
    }
    if (offers != null) {
      map['offers'] = offers?.map((v) => v.toJson()).toList();
    }
    if (banners != null) {
      map['banners'] = banners?.map((v) => v.toJson()).toList();
    }
    if (brands != null) {
      map['brands'] = brands?.map((v) => v.toJson()).toList();
    }
    if (recentlyViewed != null) {
      map['recently_viewed'] = recentlyViewed?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Brands {
  Brands({
    this.brandName,
    this.brandId,
    this.imagePath,
  });

  Brands.fromJson(dynamic json) {
    brandName = json['brandName'];
    brandId = json['brandId'];
    imagePath = json['ImagePath'];
  }
  String? brandName;
  int? brandId;
  String? imagePath;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['brandName'] = brandName;
    map['brandId'] = brandId;
    map['ImagePath'] = imagePath;
    return map;
  }
}

/// bannerId : 14
/// bannerName : "test111"
/// imagePath : "http://india.markkito.com/assets/upload/image/623c18524d1cc.png"
/// description : "description"

class Offers {
  Offers({
    this.bannerId,
    this.bannerName,
    this.imagePath,
    this.description,
  });

  Offers.fromJson(dynamic json) {
    bannerId = json['bannerId'];
    bannerName = json['bannerName'];
    imagePath = json['imagePath'];
    description = json['description'];
  }
  int? bannerId;
  String? bannerName;
  String? imagePath;
  String? description;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['bannerId'] = bannerId;
    map['bannerName'] = bannerName;
    map['imagePath'] = imagePath;
    map['description'] = description;
    return map;
  }
}

/// total_amount : 0
/// total_qty : 0
/// total_savings : 0

class CartData {
  CartData({
    this.totalAmount,
    this.totalQty,
    this.totalSavings,
  });

  CartData.fromJson(dynamic json) {
    totalAmount = json['total_amount'];
    totalQty = json['total_qty'];
    totalSavings = json['total_savings'];
  }
  dynamic totalAmount;
  dynamic totalQty;
  dynamic totalSavings;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['total_amount'] = totalAmount;
    map['total_qty'] = totalQty;
    map['total_savings'] = totalSavings;
    return map;
  }
}
