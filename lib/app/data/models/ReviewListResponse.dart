/// statusCode : 200
/// data : [{"score":3,"review":"Test ","Name":"Aswathi Vendor","CustomerName":"Nowfal"},{"score":3,"review":"","Name":"Aswathi Vendor","CustomerName":"Nowfal"}]

class ReviewListResponse {
  ReviewListResponse({
    this.statusCode,
    this.review,
  });

  ReviewListResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    if (json['data'] != null) {
      review = [];
      json['data'].forEach((v) {
        review?.add(Review.fromJson(v));
      });
    }
  }
  int? statusCode;
  List<Review>? review;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (review != null) {
      map['data'] = review?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// score : 3
/// review : "Test "
/// Name : "Aswathi Vendor"
/// CustomerName : "Nowfal"

class Review {
  Review({
    this.score,
    this.review,
    this.name,
    this.customerName,
  });

  Review.fromJson(dynamic json) {
    score = json['score'];
    review = json['review'];
    name = json['Name'];
    customerName = json['CustomerName'];
  }
  dynamic score;
  String? review;
  String? name;
  String? customerName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['score'] = score;
    map['review'] = review;
    map['Name'] = name;
    map['CustomerName'] = customerName;
    return map;
  }
}
