/// statusCode : 200
/// data : {"paymentDetails":{"CustomerName":"Nowfal S","amount":113050,"currency":"Aed","CustomerMob":949688585,"payment_gateways":[{"pg_name":"Razorpay","paymentGateOrderId":"Order_tmp37","key":"rzp_test_Fj6IJVnJmyEXp5"},{"pg_name":"Stripe","paymentGateOrderId":"Order_tmp37","key":"pk_test_51L5RxzKDC4jNikJ1xOzmvMaFLd7YsqaFEaKCwNLrSPkd0LwLQDZsP8h4oyzQyrePUcQKwKjNsQn7FYk4YuoOSIwB00G6p4vFBi"}]},"status":"success"}

class PaymentGetwayGenerateResponse {
  PaymentGetwayGenerateResponse({
    this.statusCode,
    this.data,
  });

  PaymentGetwayGenerateResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? statusCode;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// paymentDetails : {"CustomerName":"Nowfal S","amount":113050,"currency":"Aed","CustomerMob":949688585,"payment_gateways":[{"pg_name":"Razorpay","paymentGateOrderId":"Order_tmp37","key":"rzp_test_Fj6IJVnJmyEXp5"},{"pg_name":"Stripe","paymentGateOrderId":"Order_tmp37","key":"pk_test_51L5RxzKDC4jNikJ1xOzmvMaFLd7YsqaFEaKCwNLrSPkd0LwLQDZsP8h4oyzQyrePUcQKwKjNsQn7FYk4YuoOSIwB00G6p4vFBi"}]}
/// status : "success"

class Data {
  Data({
    this.paymentDetails,
    this.status,
  });

  Data.fromJson(dynamic json) {
    paymentDetails = json['paymentDetails'] != null ? PaymentDetails.fromJson(json['paymentDetails']) : null;
    status = json['status'];
  }
  PaymentDetails? paymentDetails;
  String? status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (paymentDetails != null) {
      map['paymentDetails'] = paymentDetails?.toJson();
    }
    map['status'] = status;
    return map;
  }
}

/// CustomerName : "Nowfal S"
/// amount : 113050
/// currency : "Aed"
/// CustomerMob : 949688585
/// payment_gateways : [{"pg_name":"Razorpay","paymentGateOrderId":"Order_tmp37","key":"rzp_test_Fj6IJVnJmyEXp5"},{"pg_name":"Stripe","paymentGateOrderId":"Order_tmp37","key":"pk_test_51L5RxzKDC4jNikJ1xOzmvMaFLd7YsqaFEaKCwNLrSPkd0LwLQDZsP8h4oyzQyrePUcQKwKjNsQn7FYk4YuoOSIwB00G6p4vFBi"}]

class PaymentDetails {
  PaymentDetails({
    this.customerName,
    this.amount,
    this.currency,
    this.customerMob,
    this.paymentGateways,
  });

  PaymentDetails.fromJson(dynamic json) {
    customerName = json['CustomerName'];
    amount = json['amount'];
    currency = json['currency'];
    customerMob = json['CustomerMob'];
    if (json['payment_gateways'] != null) {
      paymentGateways = [];
      json['payment_gateways'].forEach((v) {
        paymentGateways?.add(PaymentGateways.fromJson(v));
      });
    }
  }
  String? customerName;
  dynamic amount;
  String? currency;
  int? customerMob;
  List<PaymentGateways>? paymentGateways;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['CustomerName'] = customerName;
    map['amount'] = amount;
    map['currency'] = currency;
    map['CustomerMob'] = customerMob;
    if (paymentGateways != null) {
      map['payment_gateways'] = paymentGateways?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// pg_name : "Razorpay"
/// paymentGateOrderId : "Order_tmp37"
/// key : "rzp_test_Fj6IJVnJmyEXp5"

class PaymentGateways {
  PaymentGateways({
    this.pgName,
    this.paymentGateOrderId,
    this.key,
  });

  PaymentGateways.fromJson(dynamic json) {
    pgName = json['pg_name'];
    paymentGateOrderId = json['paymentGateOrderId'];
    key = json['key'];
  }
  String? pgName;
  String? paymentGateOrderId;
  String? key;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['pg_name'] = pgName;
    map['paymentGateOrderId'] = paymentGateOrderId;
    map['key'] = key;
    return map;
  }
}
