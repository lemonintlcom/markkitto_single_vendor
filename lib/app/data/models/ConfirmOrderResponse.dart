/// statusCode : 200
/// data : {"orderId":"order_JR7LiMI0Y0HKt9","PaymentMethods":[{"Id":2,"PaymentName":"Cash on Delivery"},{"Id":4,"PaymentName":"GooglePay on Delivery"},{"Id":5,"PaymentName":"Paytm on Delivery"},{"Id":3,"PaymentName":"Card on Delivery"}]}

class ConfirmOrderResponse {
  ConfirmOrderResponse({
    this.statusCode,
    this.data,
  });

  ConfirmOrderResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? statusCode;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// orderId : "order_JR7LiMI0Y0HKt9"
/// PaymentMethods : [{"Id":2,"PaymentName":"Cash on Delivery"},{"Id":4,"PaymentName":"GooglePay on Delivery"},{"Id":5,"PaymentName":"Paytm on Delivery"},{"Id":3,"PaymentName":"Card on Delivery"}]

class Data {
  Data({
    this.orderId,
    this.paymentMethods,
  });

  Data.fromJson(dynamic json) {
    orderId = json['orderId'];
    if (json['PaymentMethods'] != null) {
      paymentMethods = [];
      json['PaymentMethods'].forEach((v) {
        paymentMethods?.add(PaymentMethods.fromJson(v));
      });
    }
  }
  String? orderId;
  List<PaymentMethods>? paymentMethods;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['orderId'] = orderId;
    if (paymentMethods != null) {
      map['PaymentMethods'] = paymentMethods?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// Id : 2
/// PaymentName : "Cash on Delivery"

class PaymentMethods {
  PaymentMethods({
    this.id,
    this.paymentName,
  });

  PaymentMethods.fromJson(dynamic json) {
    id = json['Id'];
    paymentName = json['PaymentName'];
  }
  int? id;
  String? paymentName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Id'] = id;
    map['PaymentName'] = paymentName;
    return map;
  }
}
