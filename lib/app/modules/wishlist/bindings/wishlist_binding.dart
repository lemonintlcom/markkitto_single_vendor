import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/wishlist/controllers/wishlist_controller.dart';

class WishlistBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<WishListController>(
      () => WishListController(),
    );
  }
}
