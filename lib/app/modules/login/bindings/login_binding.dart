import 'package:get/get.dart';

import '../controllers/login_controller.dart';
import '../controllers/login_mobile_controller.dart';
import '../controllers/login_otp_controller.dart';
import '../controllers/login_register_controller.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LoginController>(
      () => LoginController(),
    );
    Get.lazyPut<LoginRegisterController>(
      () => LoginRegisterController(),
    );
    Get.lazyPut<LoginOtpController>(
      () => LoginOtpController(),
    );
    Get.lazyPut<LoginMobileController>(
      () => LoginMobileController(),
    );
  }
}
