import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  late PageController pageController;

  @override
  void onInit() {
    super.onInit();
    pageController = PageController(
      initialPage: 0,
    );
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
