import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/LoginOtp.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/modules/login/controllers/login_controller.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

class LoginMobileController extends GetxController {
  var box = Hive.box(Constants.configName);

  String? completePhoneNumber;

  String? phoneNumber;

  int? customerRegisterStatus;

  // FirebaseUser _user;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  var isGAuthEnabled = Storage.box.get(Constants.isGAuthEnabled, defaultValue: 0) == 1;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void sendOtp() {
    if (GetUtils.isPhoneNumber(phoneNumber!)) {
      var data = {"Mobile_Number": phoneNumber};
      XHttp.request(Endpoints.customer_login, method: XHttp.POST, data: data).then((response) {
        var data = LoginOtp.fromJson(jsonDecode(response.data));
        if (data.statusCode == 200) {
          customerRegisterStatus = data.data?.customerRegisterStatus;
          SnackBarSuccess(titleText: "Success", messageText: "OTP has been sent successfully.").show();
          Get.find<LoginController>().pageController.jumpToPage(1);
        } else {
          SnackBarFailure(titleText: "Oops..", messageText: "Unable to sent OTP.").show();
        }
      });

      return;
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Invalid phone number.").show();
      return;
    }
  }

  void signinWithGoogle() async {
    final GoogleSignInAccount? googleUser = await _googleSignIn.signIn();
    if (googleUser == null) return null;
    final GoogleSignInAuthentication googleAuth = await googleUser.authentication;
    final OAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    final UserCredential authResult = await _auth.signInWithCredential(credential);
    final User? user = authResult.user;
    if (user != null) {
      Logger().e("${user.email}");
      Logger().e("${user}");

      Get.toNamed(Routes.LOGIN_EMAIL_SOCIAL,
          arguments: {'name': user.displayName, 'email': user.email, 'avatar': user.photoURL});
    } else {
      SnackBarFailure(titleText: 'Login error', messageText: 'Unable to login with google');
    }
  }
}
