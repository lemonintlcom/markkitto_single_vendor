import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/generated/assets.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../../../../constants/colors.dart';
import '../../../../themes/custom_theme.dart';
import '../../custom_widgets/primary_button.dart';
import '../controllers/login_mobile_controller.dart';

class LoginMobileView extends GetView<LoginMobileController> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Center(
          child: Text(
            'Enter your phone number and we will send and "OTP" to continue',
            style: subtitleLite.copyWith(fontSize: 16, color: Colors.grey),
            textAlign: TextAlign.center,
          ),
        ),
        const SizedBox(
          height: 60,
        ),
        Text(
          'Mobile No.',
          style: captionLite.copyWith(fontWeight: FontWeight.normal),
        ),
        const SizedBox(
          height: 16,
        ),
        IntlPhoneField(
          obscureText: false,
          disableLengthCheck: false,
          dropdownIconPosition: IconPosition.trailing,
          showDropdownIcon: true,
          flagsButtonPadding: const EdgeInsets.all(10),
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(8),
            enabledBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              // width: 0.0 produces a thin "hairline" border
              borderSide: BorderSide(color: Colors.grey, width: 0.0),
            ),
            focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              borderSide: BorderSide(color: AppColors.primary_color, width: 1),
            ),
            hintText: 'Mobile Number',
            hintStyle: subtitle1,
          ),
          initialCountryCode: '${Storage.instance.getValue(Constants.globalCountryCode)}',
          onChanged: (phone) {
            controller.phoneNumber = phone.number;
            controller.completePhoneNumber = phone.completeNumber;
          },
        ),
        const SizedBox(
          height: 42,
        ),
        Row(
          children: [
            Expanded(
              child: PrimaryButton(
                text: 'Send OTP',
                onTap: () {
                  controller.sendOtp();
                },
              ),
            ),
          ],
        ),
        !controller.isGAuthEnabled
            ? Container()
            : const SizedBox(
                height: paddingLarge,
              ),
        !controller.isGAuthEnabled ? Container() : const Divider(),
        !controller.isGAuthEnabled
            ? Container()
            : Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Text('OR'),
                ],
              ),
        !controller.isGAuthEnabled ? Container() : const Divider(),
        !controller.isGAuthEnabled
            ? Container()
            : Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    child: Container(
                        width: Get.width / 1.8,
                        height: Get.height / 18,
                        margin: const EdgeInsets.only(top: 25),
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(20), color: Colors.black),
                        child: Center(
                            child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(
                              height: 30.0,
                              width: 30.0,
                              decoration: const BoxDecoration(
                                image: DecorationImage(image: AssetImage(Assets.imagesGoogle), fit: BoxFit.cover),
                                shape: BoxShape.circle,
                              ),
                            ),
                            const Text(
                              'Sign in with Google',
                              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Colors.white),
                            ),
                          ],
                        ))),
                    onTap: () async {
                      controller.signinWithGoogle();
                      // signInWithGoogle(model)
                      //     .then((FirebaseUser user){
                      //   model.clearAllModels();
                      //   Navigator.of(context).pushNamedAndRemoveUntil
                      //     (RouteName.Home, (Route<dynamic> route) => false
                      //   );}
                      // ).catchError((e) => print(e));
                    },
                  ),
                ],
              ),
        const Expanded(child: SizedBox()),
        Text(
          'By continue, you agree to our Terms of Service, Privacy Policy, Content Policy',
          textAlign: TextAlign.center,
          style: subtitle2.copyWith(color: Colors.grey),
        )
      ],
    );
  }
}
