import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/utils/color_utils.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../../../../constants/colors.dart';
import '../../../../themes/custom_theme.dart';
import '../../custom_widgets/primary_button.dart';
import '../controllers/login_register_controller.dart';

class LoginRegisterView extends GetView<LoginRegisterController> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Welcome to ${Storage.instance.getValue(Constants.displayName)}',
          style: headline5.copyWith(fontWeight: FontWeight.bold),
        ),
        const SizedBox(
          height: 8,
        ),
        Text(
          'Enter your details',
          style: subtitleLite.copyWith(fontSize: 18),
        ),
        const SizedBox(
          height: 16,
        ),
        TextField(
          controller: controller.nameController,
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(8),
            enabledBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              // width: 0.0 produces a thin "hairline" border
              borderSide: BorderSide(color: Colors.grey, width: 0.0),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              borderSide: BorderSide(
                  color: Storage.instance.getValue(Constants.colorCode) == null
                      ? AppColors.primary_color
                      : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                  width: 1),
            ),
            hintText: 'Enter your name',
            hintStyle: subtitle1,
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        TextField(
          controller: controller.emailController,
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(8),
            enabledBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              // width: 0.0 produces a thin "hairline" border
              borderSide: BorderSide(color: Colors.grey, width: 0.0),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(const Radius.circular(8)),
              borderSide: BorderSide(
                  color: Storage.instance.getValue(Constants.colorCode) == null
                      ? AppColors.primary_color
                      : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                  width: 1),
            ),
            hintText: 'Enter your E-mail',
            hintStyle: subtitle1,
          ),
        ),
        const SizedBox(
          height: 42,
        ),
        Row(
          children: [
            Expanded(
              child: PrimaryButton(
                text: 'Continue',
                onTap: () {
                  controller.validateAndRegisterUser();
                },
              ),
            ),
          ],
        )
      ],
    );
  }
}
