import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/models/HomePageResponse.dart' as Home;
import 'package:markkito_customer/app/data/models/HomePageResponse.dart';
import 'package:markkito_customer/app/data/models/SingleVendorHomeResponse.dart';
import 'package:markkito_customer/app/modules/custom_widgets/secondary_button.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

import '../../../constants/dimens.dart';

class UserNotLogginView extends StatelessWidget {
  const UserNotLogginView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height * .5,
      child: Column(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Icon(
                  Icons.person,
                  color: AppColors.primary_color,
                  size: 72,
                ),
                Text(
                  'Hello Guest ...',
                  style: headline5,
                ),
                Text(
                  'Please login to unlock this feature.',
                  style: captionLite.copyWith(fontSize: 14),
                ),
                const SizedBox(
                  height: 50,
                ),
                SecondaryButton(
                    text: 'Login',
                    textColor: Colors.black,
                    height: 38,
                    borderColor: AppColors.primary_color,
                    onTap: () {
                      Get.find<HomeController>().logout();
                    }),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
