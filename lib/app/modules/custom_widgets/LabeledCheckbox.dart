import 'package:flutter/material.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

class LabeledCheckbox extends StatelessWidget {
  const LabeledCheckbox({
    Key? key,
    required this.label,
    required this.trailinglabel,
    required this.value,
    required this.selectedColor,
    required this.onChanged,
  }) : super(key: key);

  final String label;
  final String trailinglabel;
  final Color selectedColor;
  final bool value;
  final Function onChanged;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          label,
          style: captionLite.copyWith(color: selectedColor, fontSize: 15),
        ),
        Row(
          children: [
            Text(
              trailinglabel,
              style: body2.copyWith(color: selectedColor, fontWeight: FontWeight.bold),
            ),
            Checkbox(
              value: value,
              onChanged: (bool? newValue) {
                onChanged(newValue);
              },
            ),
          ],
        ),
      ],
    );
  }
}
