import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/models/HomePageResponse.dart';
import 'package:markkito_customer/app/data/models/ShopDetailsResponse.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:shape_of_view_null_safe/shape_of_view_null_safe.dart';

class CategoryItem extends StatelessWidget {
  final Categories? categoryItem;
  const CategoryItem({
    this.categoryItem,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.toNamed(Routes.CATEGORY_PRODUCTS, arguments: {
          'categoryName': categoryItem?.categoryName,
          'categoryId': categoryItem?.categoryId,
          'type': 'category'
        });
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 80,
            width: 80,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(width: 1.5, color: Colors.lime),
            ),
            child: Padding(
              padding: const EdgeInsets.all(3.0),
              child: ShapeOfView(
                shape: CircleShape(),
                elevation: 0,
                child: Image.network(
                  // 'https://picsum.photos/200/300',
                  "${categoryItem?.imagePath}",
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 0.0),
            child: Text(
              '${categoryItem?.categoryName}',
              softWrap: true,
              maxLines: 2,
              textAlign: TextAlign.center,
              overflow: TextOverflow.fade,
              style: caption.copyWith(
                fontWeight: FontWeight.normal,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
