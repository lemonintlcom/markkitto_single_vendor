import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/models/HomePageResponse.dart';
import 'package:markkito_customer/app/data/models/ShopDetailsResponse.dart';
import 'package:markkito_customer/app/data/models/SingleVendorHomeResponse.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/color_utils.dart';
import 'package:markkito_customer/utils/storage.dart';
import 'package:shape_of_view_null_safe/shape_of_view_null_safe.dart';

class BrandItem extends StatelessWidget {
  final Brands? brandItem;
  const BrandItem({
    this.brandItem,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.toNamed(Routes.CATEGORY_PRODUCTS,
            arguments: {'categoryName': brandItem?.brandName, 'categoryId': brandItem?.brandId, 'type': 'brand'});
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 80,
            width: 80,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                width: 1.5,
                color: Storage.instance.getValue(Constants.colorCode) == null
                    ? AppColors.primary_color
                    : hexToColor(
                        Storage.instance.getValue(Constants.colorCode),
                      ),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(3.0),
              child: ShapeOfView(
                shape: CircleShape(),
                elevation: 0,
                child: Image.network(
                  // 'https://picsum.photos/200/300',
                  "${brandItem?.imagePath}",
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 0.0),
            child: Text(
              '${brandItem?.brandName}',
              softWrap: true,
              maxLines: 2,
              textAlign: TextAlign.center,
              overflow: TextOverflow.fade,
              style: caption.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
