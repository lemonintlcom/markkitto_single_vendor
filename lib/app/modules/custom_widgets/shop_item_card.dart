import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/models/ShopListResponse.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/generated/assets.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/functions.dart';

class ShopItemCard extends StatelessWidget {
  final bool isFullWidth;
  final Shop? shop;

  const ShopItemCard({
    Key? key,
    this.isFullWidth = false,
    this.shop,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: isFullWidth ? 4 : paddingLarge),
      child: InkWell(
        onTap: () {
          Get.toNamed(Routes.STORE_DETAILS, arguments: {'shop_id': shop?.shopId});
        },
        child: SizedBox(
          width: isFullWidth ? Get.width : Get.width * .85,
          height: 120,
          child: LimitedBox(
            child: Row(
              children: [
                Stack(
                  children: [
                    Container(), //arraneged to top
                    ClipRRect(
                      borderRadius: BorderRadius.circular(16),
                      child: Image.network(
                        shop!.profilePic!.isNotEmpty ? "${shop?.profilePic}" : "https://picsum.photos/200/300",
                        height: 120,
                        width: Get.width * .3,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Positioned(
                      top: paddingMedium,
                      left: paddingMedium,
                      child: Container(
                        decoration: const BoxDecoration(
                            color: AppColors.primary_color, borderRadius: BorderRadius.all(Radius.circular(6))),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            '${shop?.offerText}% OFF',
                            style: body2.copyWith(color: AppColors.white, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.only(left: paddingSmall, top: 0, bottom: 0, right: paddingMedium),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Flexible(
                          child: Text(
                            '${shop?.shopName}'.useCorrectEllipsis(),
                            overflow: TextOverflow.fade,
                            softWrap: false,
                            style: subtitle1.copyWith(fontWeight: FontWeight.bold),
                          ),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        Flexible(
                          child: Text(
                            '${shop?.shopLocation?.place}'.useCorrectEllipsis(),
                            overflow: TextOverflow.fade,
                            softWrap: false,
                            style: subtitle2.copyWith(color: Colors.grey),
                          ),
                        ),
                        Flexible(
                          child: Text(
                            '${(shop?.km)?.toStringAsFixed(1)} kms'.useCorrectEllipsis(),
                            overflow: TextOverflow.fade,
                            softWrap: false,
                            style: subtitle2.copyWith(color: Colors.grey),
                          ),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const Icon(
                              Icons.star,
                              size: 18,
                            ),
                            const SizedBox(
                              width: 2,
                            ),
                            Flexible(
                              child: Text(
                                '${shop?.rating ?? 0} \u2022 ${shop?.deliveryTime} mins \u2022 ${shop?.offerText}'
                                    .useCorrectEllipsis(),
                                overflow: TextOverflow.fade,
                                softWrap: false,
                                style: subtitle1.copyWith(fontWeight: FontWeight.normal),
                              ),
                            ),
                          ],
                        ),
                        const Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              Assets.svgOfferPercentage,
                              height: 18,
                            ),
                            const SizedBox(
                              width: paddingMedium,
                            ),
                            Text(
                              'Use ${shop?.offerCode}',
                              style: subtitle2.copyWith(color: Colors.grey.shade700),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
