import 'package:flutter/material.dart';
import 'package:markkito_customer/app/modules/custom_widgets/app_text.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/utils/color_utils.dart';
import 'package:markkito_customer/utils/storage.dart';

class SecondaryButton extends StatelessWidget {
  final Color borderColor;
  final Color textColor;
  final double height;
  final double elevation;
  final double radius;
  final VoidCallback onTap;
  final String text;
  const SecondaryButton({
    required this.text,
    required this.onTap,
    Key? key,
    this.borderColor = Colors.black,
    this.textColor = AppColors.primary_color,
    this.height = 45,
    this.elevation = 0,
    this.radius = 8,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      textColor: Storage.instance.getValue(Constants.colorCode) == null
          ? textColor
          : hexToColor(Storage.instance.getValue(Constants.colorCode)),
      height: height,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(radius),
        ),
        side: BorderSide(color: borderColor),
      ),
      onPressed: onTap,
      child: AppText(
        text: text,
        textColor: Storage.instance.getValue(Constants.colorCode) == null
            ? textColor
            : hexToColor(Storage.instance.getValue(Constants.colorCode)),
        fontSize: 16,
      ),
    );
  }
}
