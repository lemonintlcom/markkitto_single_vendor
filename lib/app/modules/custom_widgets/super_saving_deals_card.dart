import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

import '../../../constants/colors.dart';
import '../../../generated/assets.dart';

class SuperSavingDealCard extends StatelessWidget {
  const SuperSavingDealCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(12)),
        gradient: LinearGradient(
            colors: [
              AppColors.primary_color,
              AppColors.primary_color.withOpacity(.5),
            ],
            begin: const FractionalOffset(0.0, 0.0),
            end: const FractionalOffset(1.0, 0.0),
            stops: const [0.0, 1.0],
            tileMode: TileMode.clamp),
      ),
      child: Stack(
        children: [
          Positioned(
            right: 30,
            top: 0,
            child: Image.asset(Assets.imagesPercentage),
            height: 100,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: paddingLarge),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flexible(
                        child: Text(
                          'SUPER SAVING DEALS',
                          style: body1.copyWith(color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Flexible(
                        child: Text(
                          'Exclusive offers only for you. don\'t miss out,',
                          style: caption.copyWith(color: Colors.white),
                          maxLines: 2,
                        ),
                      ),
                      Flexible(
                        child: Text(
                          'Explore Now',
                          style: subtitle1.copyWith(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: Get.width * .2,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    '40+ Offers',
                    softWrap: true,
                    style: headline6.copyWith(color: Colors.white),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
