import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/ConfirmOrderResponse.dart';
import 'package:markkito_customer/app/data/models/PaymentGetwayGenerateResponse.dart';

import 'package:markkito_customer/app/data/models/PlaceOrderResponse.dart';
import 'package:markkito_customer/app/data/models/UpdatePaymentRefResponse.dart';

import 'package:markkito_customer/app/modules/home/controllers/cart_update_controller.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:upi_pay/upi_pay.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:http/http.dart' as http;

import '../../home/controllers/home_controller.dart';

class ConfirmOrderController extends GetxController {
  final count = 0.obs;

  var upiAppList = <ApplicationMeta>[].obs;
  var paymentGatewaysList = <PaymentGateways>[].obs;

  var isLoading = true.obs;
  var isProcessingPayment = false.obs;

  var confirmOrderResponse = Get.find<CartUpdateController>().confirmOrderResponse;

  PaymentDetails? generatePaymentDetails;

  int? currentPaymentId;

  late Razorpay _razorpay;

  @override
  void onInit() {
    super.onInit();
    // log(jsonDecode(Get.arguments['data']));
    getUpiAppList();
    log("${confirmOrderResponse?.statusCode}");

    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    _razorpay.clear();
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    Fluttertoast.showToast(msg: "SUCCESS: " + response.paymentId!, timeInSecForIosWeb: 2);
    updatePaymentReference(response.paymentId!);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    isProcessingPayment.value = false;
    // Fluttertoast.showToast(
    //     msg: "ERROR: " + response.code.toString() + " - " + response.message!, timeInSecForIosWeb: 2);
    Logger().e(response.message!);
    SnackBarFailure(titleText: "Oops..", messageText: "Payment Failed. Try again - ${response.message!}").show();
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(msg: "EXTERNAL_WALLET: " + response.walletName!, timeInSecForIosWeb: 2);
  }

  void getUpiAppList() async {
    final List<ApplicationMeta> appMetaList = await UpiPay.getInstalledUpiApplications(
        statusType: UpiApplicationDiscoveryAppStatusType.all,
        paymentType: UpiApplicationDiscoveryAppPaymentType.nonMerchant);
    Logger().e(appMetaList);
    upiAppList.addAll(appMetaList);
    for (var element in appMetaList) {
      log(element.packageName);
    }
  }

  void doUpiTransation(ApplicationMeta upiApp) async {
    final UpiTransactionResponse response = await UpiPay.initiateTransaction(
      amount: '10',
      app: upiApp.upiApplication,
      receiverName: 'Nowfal Salahudeen',
      receiverUpiAddress: 'nowfalsalahudeen@okicici',
      transactionRef: 'UPITXREF0001',
      transactionNote: 'A UPI Transaction',
    );
    print(response.status);
  }

  void placeOrder(PaymentMethods payMethod) async {
    log(payMethod.id!.toString());
    currentPaymentId = payMethod.id!;
    var data = {
      'orderID': '${confirmOrderResponse?.data?.orderId}',
      'PaymentmethodID': payMethod.id,
    };
    var result = await XHttp.request(Endpoints.c_place_order, method: XHttp.POST, data: data);

    var placeOrderResponse = PlaceOrderResponse.fromJson(jsonDecode(result.data));
    if (placeOrderResponse.statusCode == 200) {
      SnackBarSuccess(titleText: "Success", messageText: "${placeOrderResponse.data?.status}").show();
      Get.offAndToNamed(Routes.ORDER_DETAILS,
          arguments: {'orderId': placeOrderResponse.data?.orderDetails?.orderID, 'fromCart': true});
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void generatePaymentGatewayData(PaymentMethods payMethod) async {
    currentPaymentId = payMethod.id!;
    log(payMethod.id!.toString());
    paymentGatewaysList.clear();
    var data = {
      'orderID': '${confirmOrderResponse?.data?.orderId}',
    };
    var result = await XHttp.request(Endpoints.c_generate_payment_details, method: XHttp.POST, data: data);

    var placeOrderResponse = PaymentGetwayGenerateResponse.fromJson(jsonDecode(result.data));
    if (placeOrderResponse.statusCode == 200) {
      // SnackBarSuccess(titleText: "Success", messageText: "${placeOrderResponse.data?.status}").show();
      if (placeOrderResponse.data!.paymentDetails != null) {
        isProcessingPayment.value = false;
        generatePaymentDetails = placeOrderResponse.data!.paymentDetails!;
        // openCheckout(placeOrderResponse.data!.paymentDetails!);
        if (placeOrderResponse.data!.paymentDetails!.paymentGateways!.isNotEmpty) {
          paymentGatewaysList.addAll(placeOrderResponse.data!.paymentDetails!.paymentGateways!);
        }
      } else {
        SnackBarFailure(titleText: "Oops..", messageText: "${placeOrderResponse.data?.status}").show();
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void updatePaymentReference(String paymentRef) async {
    var data = {
      'PaymentmethodID': currentPaymentId,
      'orderID': confirmOrderResponse!.data!.orderId,
      'paymentAmount': generatePaymentDetails?.amount,
      'Paymentref': paymentRef,
    };
    var result = await XHttp.request(Endpoints.update_order_payment_ref, method: XHttp.POST, data: data);

    var placeOrderResponse = UpdatePaymentRefResponse.fromJson(jsonDecode(result.data));
    if (placeOrderResponse.statusCode == 200) {
      if (placeOrderResponse.data!.orderDetails != null) {
        SnackBarSuccess(titleText: "Success", messageText: "${placeOrderResponse.data?.status}").show();
        Get.offAndToNamed(Routes.ORDER_DETAILS,
            arguments: {'orderId': placeOrderResponse.data!.orderDetails?.orderID, 'fromCart': true});
      } else {
        SnackBarFailure(titleText: "Oops..", messageText: "${placeOrderResponse.data?.status}").show();
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void listUpiAppsDialog(BuildContext context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        barrierColor: Colors.grey.withOpacity(.4),
        builder: (context) {
          return Padding(
            padding: const EdgeInsets.all(paddingExtraLarge),
            child: Container(
              height: Get.height * .5,
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: const BorderRadius.all(Radius.circular(20)),
                border: Border.all(
                  color: AppColors.white,
                  width: 0.8,
                ),
              ),
              child: Column(
                children: [
                  Center(
                    child: FractionallySizedBox(
                      widthFactor: 0.25,
                      child: Container(
                        margin: const EdgeInsets.symmetric(
                          vertical: 8,
                        ),
                        height: 4,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(2),
                          border: Border.all(
                            color: Colors.black12,
                            width: 0.5,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Text(
                      'UPI Apps',
                      style: subtitle1,
                    ),
                  ),
                  const SizedBox(height: 24),
                  Column(
                    children: List.generate(upiAppList.length, (index) {
                      var item = upiAppList[index];
                      return InkWell(
                        customBorder: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        onTap: () {
                          // selectedDeliveryAddress.value = item;
                          doUpiTransation(item);
                          Get.back();
                        },
                        child: ListTile(
                          leading: item.iconImage(24),
                          title: Text('${item.upiApplication.appName}'),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ),
          );
        });
  }

  void checkoutUsingSelectedPg(PaymentGateways pgData) {
    if (pgData.pgName == "Razorpay") {
      doCheckoutUsingRazorPay(pgData);
    } else if (pgData.pgName == "Stripe") {
      doCheckoutUsingStripe(pgData);
    }
  }

  void doCheckoutUsingRazorPay(PaymentGateways pgData) async {
    Logger().e("${double.parse("${generatePaymentDetails?.amount}")}");
    var options = {
      'key': pgData.key,
      'amount': "${double.parse("${generatePaymentDetails?.amount}")}",
      'name': generatePaymentDetails?.customerName,
      'description': pgData.paymentGateOrderId,
      'prefill': {'contact': generatePaymentDetails?.customerMob, 'email': Get.find<HomeController>().user?.email},
      'external': {
        'wallets': ['paytm']
      }
    };

    try {
      _razorpay.open(options);
    } catch (e) {
      e.printError();
    }
  }

  void doCheckoutUsingStripe(PaymentGateways pgData) async {
    // final paymentMethod =
    //     await Stripe.instance.createPaymentMethod(PaymentMethodParams.));
    initPaymentSheet();
  }

  //stripe

  Future<void> initPaymentSheet() async {
    try {
      // 1. create payment intent on the server
      final data = await _createTestPaymentSheet();
      Logger().e(data['data']['client_secret']);

      // create some billingdetails
      // final billingDetails = BillingDetails(
      //   name: 'Flutter Stripe',
      //   email: 'email@stripe.com',
      //   phone: '+919496885852',
      //   address: Address(
      //     city: 'Nowfal',
      //     country: 'IN',
      //     line1: '1459 Kochi',
      //     line2: '',
      //     state: 'Kochi',
      //     postalCode: '690502',
      //   ),
      // ); // mocked data for tests

      // 2. initialize the payment sheet
      await Stripe.instance.initPaymentSheet(
        paymentSheetParameters: SetupPaymentSheetParameters(
          // Main params
          paymentIntentClientSecret: data['data']['client_secret'],
          // paymentIntentClientSecret: data['paymentIntent'],
          merchantDisplayName: 'Flutter Stripe Store Demo',
          // Customer params
          // customerId: data['customer'],
          // customerEphemeralKeySecret: data['ephemeralKey'],
          // Extra params
          applePay: true,
          googlePay: true,
          style: ThemeMode.dark,
          appearance: const PaymentSheetAppearance(
            colors: PaymentSheetAppearanceColors(
              background: Colors.lightBlue,
              primary: Colors.blue,
              componentBorder: Colors.red,
            ),
            shapes: PaymentSheetShape(
              borderWidth: 4,
              shadow: PaymentSheetShadowParams(color: Colors.red),
            ),
            primaryButton: PaymentSheetPrimaryButtonAppearance(
              shapes: PaymentSheetPrimaryButtonShape(blurRadius: 8),
              colors: PaymentSheetPrimaryButtonTheme(
                light: PaymentSheetPrimaryButtonThemeColors(
                  background: Color.fromARGB(255, 231, 235, 30),
                  text: Color.fromARGB(255, 235, 92, 30),
                  border: Color.fromARGB(255, 235, 92, 30),
                ),
              ),
            ),
          ),
          // billingDetails: billingDetails,
          testEnv: true,
          merchantCountryCode: 'uae',
        ),
      );

      await confirmPayment(data['data']['client_secret']);
    } catch (e) {
      Logger().e(e);
      rethrow;
    }
  }

  Future<Map<String, dynamic>> _createTestPaymentSheet() async {
    var data = {
      'amount': '${(generatePaymentDetails?.amount).round()}',
      'currency': '${generatePaymentDetails?.currency}',
    };
    var result = await XHttp.request(Endpoints.c_makeStripeCurl, method: XHttp.POST, data: data);
    //
    // final url = Uri.parse('https://api.stripe.com/v1/payment_intents');
    // // final url = Uri.parse('http://9fc7-223-184-37-172.ngrok.io/payment-sheet');
    // final response = await http.post(
    //   url,
    //   headers: {
    //     'Content-Type': 'application/json',
    //   },
    //   body: json.encode({
    //     'a': 'a',
    //   }),
    // );
    if (result.code != 200) {
      throw Exception('error');
    }
    final body = json.decode(result.data);
    Logger().i(body);
    if (body['error'] != null) {
      throw Exception(body['error']);
    }
    return body;
  }

  Future<void> confirmPayment(client_secret) async {
    try {
      // 3. display the payment sheet.
      await Stripe.instance.presentPaymentSheet();
      // 4. retrieve status of payment
      await Stripe.instance.retrievePaymentIntent(client_secret).then((value) {
        Logger().i('${value}');
        if (value.status == PaymentIntentsStatus.Succeeded) {
          updatePaymentReference(value.id);
        } else {
          SnackBarFailure(titleText: "Oops..", messageText: "Payment Failed. Try again ").show();
        }
      });
      // ScaffoldMessenger.of(context).showSnackBar(
      //   SnackBar(
      //     content: Text('Payment succesfully completed'),
      //   ),
      // );
      Logger().i('Payment succesfully completed');
    } on Exception catch (e) {
      if (e is StripeException) {
        Logger().i(e);
      } else {
        Logger().i(e);
      }
    }
  }
}
