import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:markkito_customer/app/data/models/ShopDetailsResponse.dart';
import 'package:markkito_customer/app/modules/custom_widgets/bottom_sheet_modal.dart';
import 'package:markkito_customer/app/modules/custom_widgets/brand_item.dart';
import 'package:markkito_customer/app/modules/custom_widgets/category_item.dart';
import 'package:markkito_customer/app/modules/custom_widgets/increment_decrement.dart';
import 'package:markkito_customer/app/modules/custom_widgets/slider_card_item.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_content_controller.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/utils/color_utils.dart';
import 'package:markkito_customer/utils/functions.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../../../../constants/colors.dart';
import '../../../../constants/dimens.dart';
import '../../../../generated/assets.dart';
import '../../../../themes/custom_theme.dart';
import '../../../routes/app_pages.dart';
import '../controllers/store_details_controller.dart';

class StoreDetailsView extends StatelessWidget {
  const StoreDetailsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(StoreDetailsController());
    return SafeArea(
      child: Scaffold(
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark
              .copyWith(statusBarColor: AppColors.transparent, systemNavigationBarColor: AppColors.white),
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Obx(() {
              return Container(
                child: controller.isLoading.value
                    ? Container()
                    : Column(
                        children: [
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      '${Storage.box.get(Constants.displayName)}',
                                      style: headline5.copyWith(
                                          color: Storage.instance.getValue(Constants.colorCode) == null
                                              ? AppColors.primary_color
                                              : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                InkWell(
                                  onTap: () {},
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      SvgPicture.asset(
                                        Assets.svgLocation,
                                        height: 24,
                                      ),
                                      const SizedBox(
                                        width: 6,
                                      ),
                                      InkWell(
                                        onTap: () {
                                          Get.toNamed(Routes.SELECT_LOCATION_MAP,
                                              arguments: {'fromHome': true, 'toLogin': false});
                                        },
                                        child: SizedBox(
                                          width: Get.width * .3,
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                ' Delivery to',
                                                style: caption,
                                              ),
                                              ValueListenableBuilder(
                                                  valueListenable: Hive.box(Constants.configName).listenable(),
                                                  builder: (context, Box box, _) {
                                                    return Text(
                                                      '${box.get(Constants.locationText)}',
                                                      style: body2,
                                                      maxLines: 1,
                                                      softWrap: false,
                                                      overflow: TextOverflow.fade,
                                                    );
                                                  }),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          Expanded(
                            child: CustomScrollView(
                              key: const PageStorageKey('store_details_view_key'),
                              physics: const BouncingScrollPhysics(),
                              slivers: [
                                // const SliverToBoxAdapter(
                                //   child: SizedBox(
                                //     height: paddingLarge,
                                //   ),
                                // ),
                                // SliverToBoxAdapter(
                                //   child: ,
                                // ), //Top bar
                                // const SliverToBoxAdapter(
                                //   child: SizedBox(
                                //     height: paddingLarge,
                                //   ),
                                // ),
                                SliverToBoxAdapter(
                                  child: Stack(
                                    children: [
                                      Container(
                                        height: Get.height * .2,
                                        decoration: BoxDecoration(
                                          color: AppColors.black1,
                                          image: DecorationImage(
                                            fit: BoxFit.cover,
                                            colorFilter:
                                                ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop),
                                            image: NetworkImage(
                                              controller.shopData?.profilePic != null
                                                  ? "${controller.shopData?.profilePic}"
                                                  : "https://picsum.photos/200/300",
                                            ),
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        bottom: 10,
                                        left: 10,
                                        right: 10,
                                        child: Container(
                                          height: Get.width * .2,
                                          width: double.infinity,
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              ClipRRect(
                                                borderRadius: BorderRadius.circular(10),
                                                child: Image.network(
                                                  controller.shopData?.profilePic != null
                                                      ? "${controller.shopData?.profilePic}"
                                                      : "https://picsum.photos/200/300",
                                                  width: 70,
                                                  fit: BoxFit.cover,
                                                  height: 70,
                                                ),
                                              ),
                                              const SizedBox(
                                                width: paddingMedium,
                                              ),
                                              Flexible(
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                    Text(
                                                      '${controller.shopData?.shopName}',
                                                      style: subtitleLite.copyWith(
                                                          fontWeight: FontWeight.bold,
                                                          color: AppColors.white,
                                                          fontSize: 20),
                                                    ),
                                                    Text(
                                                      '${controller.shopData?.type}',
                                                      style: captionLite.copyWith(
                                                        color: AppColors.white,
                                                      ),
                                                    ),
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Expanded(
                                                          child: Text(
                                                            '${controller.shopData?.place} '.useCorrectEllipsis(),
                                                            maxLines: 1,
                                                            softWrap: false,
                                                            overflow: TextOverflow.fade,
                                                            style: caption.copyWith(
                                                                fontWeight: FontWeight.bold, color: AppColors.white),
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          width: 20,
                                                        ),
                                                        Text(
                                                          controller.shopData?.openingStatus! == 1
                                                              ? 'Open Now'
                                                              : 'Closed Now',
                                                          style: captionLite.copyWith(
                                                              color: controller.shopData?.openingStatus! == 1
                                                                  ? Colors.green
                                                                  : Colors.red,
                                                              fontWeight: FontWeight.bold),
                                                        ),
                                                        const SizedBox(
                                                          width: 10,
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),

                                SliverToBoxAdapter(
                                  child: Obx(() {
                                    return controller.offersSlider.isEmpty
                                        ? Container()
                                        : const SizedBox(
                                            height: paddingSmall,
                                          );
                                  }),
                                ),
                                SliverToBoxAdapter(
                                  child: Obx(() {
                                    return controller.offersSlider.isEmpty
                                        ? Container()
                                        : Padding(
                                            padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                SvgPicture.asset(
                                                  Assets.svgStoreSmall,
                                                  height: 20,
                                                ),
                                                const SizedBox(
                                                  width: 12,
                                                ),
                                                Text(
                                                  'Latest Offers',
                                                  style: headline6.copyWith(fontWeight: FontWeight.bold),
                                                ),
                                              ],
                                            ),
                                          );
                                  }),
                                ),

                                SliverToBoxAdapter(
                                  child: Obx(() {
                                    return controller.offersSlider.isEmpty
                                        ? Container()
                                        : const SizedBox(
                                            height: paddingLarge,
                                          );
                                  }),
                                ),
                                SliverToBoxAdapter(
                                  child: SizedBox(
                                    child: Obx(() {
                                      return controller.offersSlider.isEmpty
                                          ? Container()
                                          : CarouselSlider.builder(
                                              itemCount: controller.offersSlider.length,
                                              itemBuilder: (BuildContext context, int itemIndex, int pageViewIndex) {
                                                var bannerItem = controller.offersSlider[itemIndex];
                                                return ClipRRect(
                                                  key: ValueKey(bannerItem?.bannerId),
                                                  borderRadius: BorderRadius.circular(16),
                                                  child: InkWell(
                                                    onTap: () {},
                                                    child: Container(
                                                      width: MediaQuery.of(context).size.width,
                                                      margin: const EdgeInsets.symmetric(horizontal: 5.0),
                                                      decoration: BoxDecoration(color: Colors.grey.shade200),
                                                      child: Image.network(
                                                        bannerItem!.imagePath!.isNotEmpty
                                                            ? "${bannerItem.imagePath}"
                                                            : "https://picsum.photos/200/300",
                                                        fit: BoxFit.cover,
                                                      ),
                                                    ),
                                                  ),
                                                );
                                              },
                                              options: CarouselOptions(
                                                height: 180.0,
                                                autoPlay: true,
                                                initialPage: 0,
                                                enableInfiniteScroll: true,
                                              ),
                                            );
                                    }),
                                  ),
                                ),
                                SliverToBoxAdapter(
                                  child: Obx(() {
                                    return controller.offersSlider.isEmpty
                                        ? Container()
                                        : const SizedBox(
                                            height: paddingLarge,
                                          );
                                  }),
                                ),
                                SliverToBoxAdapter(
                                  child: Container(
                                      width: double.infinity, height: paddingExtraSmall, color: Colors.grey.shade200),
                                ),
                                const SliverToBoxAdapter(
                                  child: SizedBox(
                                    height: paddingLarge,
                                  ),
                                ),
                                SliverToBoxAdapter(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        SvgPicture.asset(
                                          Assets.svgStoreSmall,
                                          height: 20,
                                        ),
                                        const SizedBox(
                                          width: 12,
                                        ),
                                        Text(
                                          'Categories',
                                          style: headline6.copyWith(fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                const SliverToBoxAdapter(
                                  child: SizedBox(
                                    height: paddingSmall,
                                  ),
                                ),
                                SliverToBoxAdapter(
                                  child: SizedBox(
                                    height: controller.catogories.length > 8 ? 118 + 118 : 118,
                                    child: Obx(() {
                                      return GridView.builder(
                                          padding: const EdgeInsets.only(left: paddingLarge),
                                          itemCount: controller.catogories.length,
                                          physics: const BouncingScrollPhysics(),
                                          scrollDirection: Axis.horizontal,
                                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisCount: controller.catogories.length > 8 ? 2 : 1,
                                            childAspectRatio: 1.5,
                                          ),
                                          itemBuilder: (context, index) {
                                            var categoryItem = controller.catogories[index];
                                            return Padding(
                                              key: ValueKey(categoryItem.categoryId),
                                              padding: const EdgeInsets.only(right: 20.0, bottom: 0),
                                              child: CategoryItem(
                                                categoryItem: categoryItem,
                                              ),
                                            );
                                          });
                                    }),
                                  ),
                                ),
                                // const SliverToBoxAdapter(
                                //   child: SizedBox(
                                //     height: paddingLarge,
                                //   ),
                                // ),
                                // SliverToBoxAdapter(
                                //   child: Container(
                                //       width: double.infinity, height: paddingExtraSmall, color: Colors.grey.shade200),
                                // ),
                                const SliverToBoxAdapter(
                                  child: SizedBox(
                                    height: paddingLarge,
                                  ),
                                ),
                                SliverToBoxAdapter(
                                  child: SizedBox(
                                    child: Obx(() {
                                      return controller.banners.isEmpty
                                          ? Container()
                                          : CarouselSlider.builder(
                                              itemCount: controller.banners.length,
                                              itemBuilder: (BuildContext context, int itemIndex, int pageViewIndex) {
                                                var bannerItem = controller.banners[itemIndex];
                                                return InkWell(
                                                  key: ValueKey(bannerItem?.id),
                                                  onTap: () {
                                                    Get.toNamed(Routes.CATEGORY_PRODUCTS, arguments: {
                                                      'categoryName': 'Offer products',
                                                      'categoryId': bannerItem?.id,
                                                      'type': 'banner'
                                                    });
                                                  },
                                                  child: ClipRRect(
                                                    borderRadius: BorderRadius.circular(16),
                                                    child: Container(
                                                      width: MediaQuery.of(context).size.width,
                                                      margin: const EdgeInsets.symmetric(horizontal: 5.0),
                                                      decoration: BoxDecoration(color: Colors.grey.shade200),
                                                      child: Image.network(
                                                        bannerItem!.url!.isNotEmpty
                                                            ? "${bannerItem.url}"
                                                            : "https://picsum.photos/200/300",
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                  ),
                                                );
                                              },
                                              options: CarouselOptions(
                                                height: 140.0,
                                                autoPlay: true,
                                                initialPage: 0,
                                                enableInfiniteScroll: true,
                                              ),
                                            );
                                    }),
                                  ),
                                ),
                                SliverToBoxAdapter(
                                  child: Obx(() {
                                    return controller.banners.isEmpty
                                        ? Container()
                                        : const SizedBox(
                                            height: paddingLarge,
                                          );
                                  }),
                                ),
                                SliverToBoxAdapter(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        SvgPicture.asset(
                                          Assets.svgStoreSmall,
                                          height: 20,
                                        ),
                                        const SizedBox(
                                          width: 12,
                                        ),
                                        Text(
                                          'Brands',
                                          style: headline6.copyWith(fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SliverToBoxAdapter(
                                  child: controller.brands.isEmpty
                                      ? Container()
                                      : const SizedBox(
                                          height: paddingSmall,
                                        ),
                                ),
                                SliverToBoxAdapter(
                                  child: controller.brands.isEmpty
                                      ? Container()
                                      : SizedBox(
                                          height: controller.brands.length > 8 ? 118 + 118 : 118,
                                          child: Obx(() {
                                            return GridView.builder(
                                                padding: const EdgeInsets.only(left: paddingLarge),
                                                itemCount: controller.brands.length,
                                                physics: const BouncingScrollPhysics(),
                                                scrollDirection: Axis.horizontal,
                                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                                  crossAxisCount: controller.brands.length > 8 ? 2 : 1,
                                                  childAspectRatio: 1.5,
                                                ),
                                                itemBuilder: (context, index) {
                                                  var brandsItem = controller.brands[index];
                                                  return Padding(
                                                    key: ValueKey(brandsItem?.brandId),
                                                    padding: const EdgeInsets.only(right: 20.0, bottom: 0),
                                                    child: BrandItem(
                                                      brandItem: brandsItem,
                                                    ),
                                                  );
                                                });
                                          }),
                                        ),
                                ),
                                SliverToBoxAdapter(
                                  child: controller.brands.isEmpty
                                      ? Container()
                                      : const SizedBox(
                                          height: paddingLarge,
                                        ),
                                ),
                                SliverToBoxAdapter(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        SvgPicture.asset(
                                          Assets.svgStoreSmall,
                                          height: 20,
                                        ),
                                        const SizedBox(
                                          width: 12,
                                        ),
                                        Text(
                                          'Featured Products',
                                          style: headline6.copyWith(fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                const SliverToBoxAdapter(
                                  child: SizedBox(
                                    height: paddingLarge,
                                  ),
                                ),
                                SliverPadding(
                                  padding: const EdgeInsets.only(
                                      left: paddingLarge, right: paddingLarge, bottom: paddingLarge),
                                  sliver: Obx(() {
                                    return SliverGrid(
                                      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 2, childAspectRatio: .7),
                                      delegate: SliverChildBuilderDelegate((ctx, index) {
                                        var product = controller.products[index];
                                        return InkWell(
                                          key: ValueKey(product?.selectedPriceListId),
                                          onTap: () {
                                            Get.toNamed(Routes.PRODUCT_DETAILS, arguments: {
                                              'productId': product?.generateProductListId,
                                              'VendorUserTypeId': controller.shopData?.shopId
                                            });
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                              border: Border.all(color: Colors.grey.shade200),
                                            ),
                                            child: Stack(
                                              children: [
                                                Padding(
                                                  padding: const EdgeInsets.only(left: 8, right: 8, top: 12),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      Image.network(
                                                        product?.image != null
                                                            ? "${product?.image}"
                                                            : "https://picsum.photos/200/300",
                                                        height: 90,
                                                        width: 100,
                                                        fit: BoxFit.contain,
                                                        errorBuilder: (x, y, z) {
                                                          return Image.asset(
                                                            Assets.imagesProductPlaceholder,
                                                            height: 90,
                                                            width: 100,
                                                            fit: BoxFit.contain,
                                                          );
                                                        },
                                                      ),
                                                      const SizedBox(
                                                        height: 10,
                                                      ),
                                                      SizedBox(
                                                        width: double.infinity,
                                                        child: Text(
                                                          '${product?.catagoryName}',
                                                          style: caption.copyWith(color: Colors.grey),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: double.infinity,
                                                        child: Text(
                                                          '${product?.productName} '.useCorrectEllipsis(),
                                                          maxLines: 1,
                                                          softWrap: false,
                                                          overflow: TextOverflow.fade,
                                                          style: caption.copyWith(fontWeight: FontWeight.bold),
                                                        ),
                                                      ),
                                                      const SizedBox(
                                                        height: 8,
                                                      ),
                                                      Row(
                                                        children: [
                                                          RichText(
                                                            text: TextSpan(
                                                              children: [
                                                                TextSpan(
                                                                  text:
                                                                      '${product?.currencySymbol}${product?.price.toStringAsFixed(2)}',
                                                                  style: body2.copyWith(
                                                                      color: Colors.black, fontWeight: FontWeight.bold),
                                                                ),
                                                                TextSpan(
                                                                  text: ' /${product?.unit}',
                                                                  style: captionSmall.copyWith(
                                                                      color: Colors.black, fontWeight: FontWeight.bold),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      const SizedBox(
                                                        height: 8,
                                                      ),
                                                      Row(
                                                        children: [
                                                          Expanded(
                                                            child: Container(
                                                              padding: const EdgeInsets.only(left: 8, right: 8),
                                                              decoration: BoxDecoration(
                                                                borderRadius:
                                                                    const BorderRadius.all(Radius.circular(4)),
                                                                border: Border.all(color: Colors.grey.shade400),
                                                              ),
                                                              child: PopupMenuButton(
                                                                enabled: product!.priceList!.length > 1,
                                                                itemBuilder: (BuildContext context) {
                                                                  return product.priceList!.map((PriceList item) {
                                                                    return PopupMenuItem<PriceList>(
                                                                      value: item,
                                                                      child: Text(
                                                                        '${item.price.toStringAsFixed(2)} ${item.priceFor}',
                                                                        style: caption.copyWith(color: Colors.black),
                                                                      ),
                                                                    );
                                                                  }).toList();
                                                                },
                                                                onSelected: (PriceList value) {
                                                                  controller.productTypeValue[index] =
                                                                      '${value.price.toStringAsFixed(2)} ${value.priceFor}';
                                                                  product.price = value.price;
                                                                  product.unit = value.priceFor;
                                                                  product.selectedPriceListId =
                                                                      value.generatelistdetailsId;
                                                                  product.cartTempCount = value.cartQty!;

                                                                  // controller.selectedProductTypeObject[index] = value;

                                                                  controller.updateProductPrice(product);
                                                                  // controller.productQtyValue.value = '${value.price} ${value.priceFor}';
                                                                },
                                                                child: Row(
                                                                  children: [
                                                                    Obx(() {
                                                                      return Expanded(
                                                                        child: Text(
                                                                          controller.productTypeValue[index] == null &&
                                                                                  product.priceList!.isNotEmpty
                                                                              ? '${product.priceList![0].price.toStringAsFixed(2)} ${product.priceList![0].priceFor}'
                                                                              : controller.productTypeValue[index] ??
                                                                                  '',
                                                                          style: caption.copyWith(
                                                                              color: Colors.grey.shade600),
                                                                        ),
                                                                      );
                                                                    }),
                                                                    product.priceList!.isNotEmpty &&
                                                                            product.priceList!.length != 1
                                                                        ? const Icon(Icons.arrow_drop_down)
                                                                        : Container(
                                                                            height: 24,
                                                                          ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      !Get.find<HomeController>().isLoggedIn
                                                          ? Container()
                                                          : Expanded(
                                                              child: Row(
                                                                children: [
                                                                  product.isIncart! == 0
                                                                      ? Expanded(
                                                                          child: MaterialButton(
                                                                            color: Storage.instance.getValue(
                                                                                        Constants.colorCode) ==
                                                                                    null
                                                                                ? AppColors.primary_color
                                                                                : hexToColor(Storage.instance
                                                                                    .getValue(Constants.colorCode)),
                                                                            textColor: AppColors.white,
                                                                            height: 28,
                                                                            elevation: 0,
                                                                            shape: const RoundedRectangleBorder(
                                                                              borderRadius: BorderRadius.all(
                                                                                Radius.circular(6),
                                                                              ),
                                                                            ),
                                                                            onPressed: () {
                                                                              Get.find<HomeController>().isLoggedIn
                                                                                  ? controller.addToCart(
                                                                                      status: 'add',
                                                                                      product_id:
                                                                                          product.selectedPriceListId,
                                                                                      quantity: 1,
                                                                                      productData: product)
                                                                                  : BottomSheetModal()
                                                                                      .showLoginRequired(context);
                                                                            },
                                                                            child: Text(
                                                                              'Add to cart',
                                                                              style: caption.copyWith(
                                                                                  fontWeight: FontWeight.bold),
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : Expanded(
                                                                          child: Column(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.center,
                                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                                            children: [
                                                                              IncrementDecrement(
                                                                                cartProductItems: null,
                                                                                count: product.cartTempCount,
                                                                                onDecrement: () {
                                                                                  if (product.cartTempCount > 1) {
                                                                                    // product.cartTempCount = product.cartTempCount - 1;
                                                                                    controller.updateCartCount(
                                                                                        product, 'decrement');
                                                                                  }
                                                                                  if (product.cartTempCount == 1) {
                                                                                    controller.updateCartCount(
                                                                                        product, 'remove');
                                                                                  }
                                                                                },
                                                                                onIncrement: () {
                                                                                  // product.cartTempCount = product.cartTempCount + 1;
                                                                                  controller.updateCartCount(
                                                                                      product, 'increment');
                                                                                },
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                  // MaterialButton(
                                                                  //   minWidth: 8,
                                                                  //   elevation: 0,
                                                                  //   shape: const RoundedRectangleBorder(
                                                                  //     borderRadius: BorderRadius.all(
                                                                  //       Radius.circular(6),
                                                                  //     ),
                                                                  //   ),
                                                                  //   onPressed: () {
                                                                  //     if (product.cartTempCount > 0) {
                                                                  //       controller.addToCart(
                                                                  //           status: 'add',
                                                                  //           product_id: product.selectedPriceListId,
                                                                  //           quantity: product.cartTempCount);
                                                                  //     } else {
                                                                  //       SnackBarFailure(
                                                                  //           titleText: "Oops..",
                                                                  //           messageText: "Please change quantity");
                                                                  //     }
                                                                  //   },
                                                                  //   child: const Icon(
                                                                  //     Icons.add_shopping_cart,
                                                                  //     size: 18,
                                                                  //   ),
                                                                  // ),
                                                                ],
                                                              ),
                                                            ),
                                                    ],
                                                  ),
                                                ),
                                                Positioned(
                                                  top: 10,
                                                  left: 0,
                                                  right: 0,
                                                  child: SizedBox(
                                                    width: double.infinity,
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Container(
                                                          color: Colors.red,
                                                          child: Text(
                                                            ' Flat ${product.offer}% ',
                                                            style: captionSmall.copyWith(color: AppColors.white),
                                                          ),
                                                        ),
                                                        InkWell(
                                                          onTap: () {
                                                            Get.find<HomeController>().isLoggedIn
                                                                ? controller.doProductFavorite(product)
                                                                : BottomSheetModal().showLoginRequired(context);
                                                          },
                                                          child: Padding(
                                                            padding: const EdgeInsets.only(right: 8.0),
                                                            child: Image.asset(
                                                              Assets.imagesBookmarkOutlineGrey,
                                                              height: 16,
                                                              color: product.favouirte == 1
                                                                  ? AppColors.primary_color
                                                                  : Colors.grey,
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                      }, childCount: controller.products.length),
                                    );
                                  }),
                                ),
                                /*Recenlty Viewed Products*/
                                SliverToBoxAdapter(
                                  child: controller.recentlyViewedProducts.isEmpty
                                      ? Container()
                                      : Padding(
                                          padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              SvgPicture.asset(
                                                Assets.svgStoreSmall,
                                                height: 20,
                                              ),
                                              const SizedBox(
                                                width: 12,
                                              ),
                                              Text(
                                                'Recently Viewed Products',
                                                style: headline6.copyWith(fontWeight: FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ),
                                ),
                                SliverToBoxAdapter(
                                  child: controller.recentlyViewedProducts.isEmpty
                                      ? Container()
                                      : const SizedBox(
                                          height: paddingLarge,
                                        ),
                                ),
                                SliverPadding(
                                  padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                                  sliver: controller.recentlyViewedProducts.isEmpty
                                      ? SliverToBoxAdapter(child: Container())
                                      : Obx(() {
                                          return SliverGrid(
                                            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                                                crossAxisCount: 2, childAspectRatio: .7),
                                            delegate: SliverChildBuilderDelegate((ctx, index) {
                                              var product = controller.recentlyViewedProducts[index];
                                              return InkWell(
                                                key: ValueKey(product?.selectedPriceListId),
                                                onTap: () {
                                                  Get.toNamed(Routes.PRODUCT_DETAILS, arguments: {
                                                    'productId': product?.generateProductListId,
                                                    'VendorUserTypeId': controller.shopData?.shopId
                                                  });
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    border: Border.all(color: Colors.grey.shade200),
                                                  ),
                                                  child: Stack(
                                                    children: [
                                                      Padding(
                                                        padding: const EdgeInsets.only(left: 8, right: 8, top: 12),
                                                        child: Column(
                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                          children: [
                                                            Image.network(
                                                              product?.image != null
                                                                  ? "${product?.image}"
                                                                  : "https://picsum.photos/200/300",
                                                              height: 90,
                                                              width: 100,
                                                              fit: BoxFit.contain,
                                                            ),
                                                            const SizedBox(
                                                              height: 10,
                                                            ),
                                                            SizedBox(
                                                              width: double.infinity,
                                                              child: Text(
                                                                '${product?.catagoryName}',
                                                                style: caption.copyWith(color: Colors.grey),
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              width: double.infinity,
                                                              child: Text(
                                                                '${product?.productName} '.useCorrectEllipsis(),
                                                                maxLines: 1,
                                                                softWrap: false,
                                                                overflow: TextOverflow.fade,
                                                                style: caption.copyWith(fontWeight: FontWeight.bold),
                                                              ),
                                                            ),
                                                            const SizedBox(
                                                              height: 8,
                                                            ),
                                                            Row(
                                                              children: [
                                                                RichText(
                                                                  text: TextSpan(
                                                                    children: [
                                                                      TextSpan(
                                                                        text:
                                                                            '${product?.currencySymbol}${product?.price.toStringAsFixed(2)}',
                                                                        style: body2.copyWith(
                                                                            color: Colors.black,
                                                                            fontWeight: FontWeight.bold),
                                                                      ),
                                                                      TextSpan(
                                                                        text: ' /${product?.unit}',
                                                                        style: captionSmall.copyWith(
                                                                            color: Colors.black,
                                                                            fontWeight: FontWeight.bold),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                            const SizedBox(
                                                              height: 8,
                                                            ),
                                                            Row(
                                                              children: [
                                                                Expanded(
                                                                  child: Container(
                                                                    padding: const EdgeInsets.only(left: 8, right: 8),
                                                                    decoration: BoxDecoration(
                                                                      borderRadius:
                                                                          const BorderRadius.all(Radius.circular(4)),
                                                                      border: Border.all(color: Colors.grey.shade400),
                                                                    ),
                                                                    child: PopupMenuButton(
                                                                      enabled: product!.priceList!.length > 1,
                                                                      itemBuilder: (BuildContext context) {
                                                                        return product.priceList!.map((PriceList item) {
                                                                          return PopupMenuItem<PriceList>(
                                                                            value: item,
                                                                            child: Text(
                                                                              '${item.price.toStringAsFixed(2)} ${item.priceFor}',
                                                                              style:
                                                                                  caption.copyWith(color: Colors.black),
                                                                            ),
                                                                          );
                                                                        }).toList();
                                                                      },
                                                                      onSelected: (PriceList value) {
                                                                        controller.productTypeValue[index] =
                                                                            '${value.price.toStringAsFixed(2)} ${value.priceFor}';
                                                                        product.price = value.price;
                                                                        product.unit = value.priceFor;
                                                                        product.selectedPriceListId =
                                                                            value.generatelistdetailsId;
                                                                        product.cartTempCount = value.cartQty!;

                                                                        // controller.selectedProductTypeObject[index] = value;

                                                                        controller.updateProductPrice(product);
                                                                        // controller.productQtyValue.value = '${value.price} ${value.priceFor}';
                                                                      },
                                                                      child: Row(
                                                                        children: [
                                                                          Obx(() {
                                                                            return Expanded(
                                                                              child: Text(
                                                                                controller.productTypeValue[index] ==
                                                                                            null &&
                                                                                        product.priceList!.isNotEmpty
                                                                                    ? '${product.priceList![0].price.toStringAsFixed(2)} ${product.priceList![0].priceFor}'
                                                                                    : controller
                                                                                            .productTypeValue[index] ??
                                                                                        '',
                                                                                style: caption.copyWith(
                                                                                    color: Colors.grey.shade600),
                                                                              ),
                                                                            );
                                                                          }),
                                                                          product.priceList!.isNotEmpty &&
                                                                                  product.priceList!.length != 1
                                                                              ? const Icon(Icons.arrow_drop_down)
                                                                              : Container(
                                                                                  height: 24,
                                                                                ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                            !Get.find<HomeController>().isLoggedIn
                                                                ? Container()
                                                                : Expanded(
                                                                    child: Row(
                                                                      children: [
                                                                        product.isIncart! == 0
                                                                            ? Expanded(
                                                                                child: MaterialButton(
                                                                                  color: Storage.instance.getValue(
                                                                                              Constants.colorCode) ==
                                                                                          null
                                                                                      ? AppColors.primary_color
                                                                                      : hexToColor(Storage.instance
                                                                                          .getValue(
                                                                                              Constants.colorCode)),
                                                                                  textColor: AppColors.white,
                                                                                  height: 28,
                                                                                  elevation: 0,
                                                                                  shape: const RoundedRectangleBorder(
                                                                                    borderRadius: BorderRadius.all(
                                                                                      Radius.circular(6),
                                                                                    ),
                                                                                  ),
                                                                                  onPressed: () {
                                                                                    Get.find<HomeController>()
                                                                                            .isLoggedIn
                                                                                        ? controller.addToCart(
                                                                                            status: 'add',
                                                                                            product_id: product
                                                                                                .selectedPriceListId,
                                                                                            quantity: 1,
                                                                                            productData: product)
                                                                                        : BottomSheetModal()
                                                                                            .showLoginRequired(context);
                                                                                  },
                                                                                  child: Text(
                                                                                    'Add to cart',
                                                                                    style: caption.copyWith(
                                                                                        fontWeight: FontWeight.bold),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            : Expanded(
                                                                                child: Column(
                                                                                  crossAxisAlignment:
                                                                                      CrossAxisAlignment.center,
                                                                                  mainAxisAlignment:
                                                                                      MainAxisAlignment.center,
                                                                                  children: [
                                                                                    IncrementDecrement(
                                                                                      cartProductItems: null,
                                                                                      count: product.cartTempCount,
                                                                                      onDecrement: () {
                                                                                        if (product.cartTempCount > 1) {
                                                                                          // product.cartTempCount = product.cartTempCount - 1;
                                                                                          controller.updateCartCount(
                                                                                              product, 'decrement');
                                                                                        }
                                                                                        if (product.cartTempCount ==
                                                                                            1) {
                                                                                          controller.updateCartCount(
                                                                                              product, 'remove');
                                                                                        }
                                                                                      },
                                                                                      onIncrement: () {
                                                                                        // product.cartTempCount = product.cartTempCount + 1;
                                                                                        controller.updateCartCount(
                                                                                            product, 'increment');
                                                                                      },
                                                                                    ),
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                          ],
                                                        ),
                                                      ),
                                                      Positioned(
                                                        top: 10,
                                                        left: 0,
                                                        right: 0,
                                                        child: SizedBox(
                                                          width: double.infinity,
                                                          child: Row(
                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                            children: [
                                                              Container(
                                                                color: Colors.red,
                                                                child: Text(
                                                                  ' Flat ${product.offer}% ',
                                                                  style: captionSmall.copyWith(color: AppColors.white),
                                                                ),
                                                              ),
                                                              InkWell(
                                                                onTap: () {
                                                                  Get.find<HomeController>().isLoggedIn
                                                                      ? controller.doProductFavorite(product)
                                                                      : BottomSheetModal().showLoginRequired(context);
                                                                },
                                                                child: Padding(
                                                                  padding: const EdgeInsets.only(right: 8.0),
                                                                  child: Image.asset(
                                                                    Assets.imagesBookmarkOutlineGrey,
                                                                    height: 16,
                                                                    color: product.favouirte == 1
                                                                        ? AppColors.primary_color
                                                                        : Colors.grey,
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              );
                                            }, childCount: controller.recentlyViewedProducts.length),
                                          );
                                        }),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
              );
            }),
          ),
        ),
      ),
    );
  }

  Widget buildActionMenuItem({icon, title, onTap}) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            SvgPicture.asset(icon),
            SizedBox(
              height: paddingExtraSmall,
            ),
            Text(
              title,
              style: caption,
            )
          ],
        ),
      ),
    );
  }
}
