import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/CartChangeItemResponse.dart';
import 'package:markkito_customer/app/data/models/CartStatusResponse.dart';
import 'package:markkito_customer/app/data/models/ProductListResponse.dart';
import 'package:markkito_customer/app/data/models/ShopDetailsResponse.dart';
import 'package:markkito_customer/app/data/models/SingleVendorHomeResponse.dart';
import 'package:markkito_customer/app/data/models/WishListResponse.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/app/modules/search/controllers/search_controller.dart';
import 'package:markkito_customer/app/modules/store_details/controllers/store_details_controller.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

class ProductsController extends GetxController {
  //using same controller for wishlist and product list
  var defaultCatTypeIndex = 0.obs;

  var productTypeValue = <int, String>{}.obs;
  var selectedProductTypeObject = <int, PriceList>{}.obs;
  var productPriceValue = <int, String>{}.obs;

  var isLoading = true.obs;

  var products = <Products?>[].obs;
  var productsTemp = <Products?>[];
  var pageNo = 0;

  var catagories = <Categories>[].obs;
  var isLoggedIn = Storage.box.get(Constants.isUserLoggedIn, defaultValue: false);

  var shopFavorite = 0.obs;

  var wishListProducts = <Products>[].obs;

  ScrollController scrollController = ScrollController();

  @override
  void onInit() {
    super.onInit();
    scrollController.addListener(pagination);

    Get.find<StoreDetailsController>().catogories.forEach((element) {
      catagories.add(element);
    });
    catagories.insert(0, Categories(categoryName: 'All'));
    getProductList();
  }

  void pagination() {
    if ((scrollController.position.pixels == scrollController.position.maxScrollExtent)) {
      isLoading.value = true;
      pageNo += 1;
      //add api for load the more data according to new page

      getProductList();
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  getProductList() async {
    var data = {
      'shop_id': Storage.instance.getValue(Constants.storeId),
      'pageNo': pageNo,
    };
    var result = await XHttp.request(Endpoints.r_productList, method: XHttp.POST, data: data);
    var productListResponse = ProductListResponse.fromJson(jsonDecode(result.data));
    if (productListResponse.statusCode == 200) {
      if (productListResponse.data!.productList!.isNotEmpty) {
        // products.addAll(shopDataResponse.shopData!.products!);
        for (var product in productListResponse.data!.productList!) {
          // var i = 0;
          // set selected product varient id from the product list
          if (product.priceList!.isNotEmpty) {
            product.selectedPriceListId = product.priceList![0].generatelistdetailsId;
            // selectedProductTypeObject[i] = product.priceList![0];
            product.cartTempCount = product.priceList![0].cartQty!;
          } else {
            product.selectedPriceListId = product.id;
            product.cartTempCount = 0;
          }
          products.add(product);
          productsTemp.add(product);
          // i++;
        }
      }
      isLoading.value = false;
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void updateProductsByCat(Categories cat) {
    products.clear();
    productTypeValue.clear();
    if (cat.categoryName == "All") {
      products.addAll(productsTemp);
    } else {
      products.value = productsTemp
          .where((element) => element?.catagoryName?.toLowerCase() == cat.categoryName?.toLowerCase())
          .toList();
    }
  }

  void updateProductPrice(Products product) {
    products[products.indexWhere((element) => element?.id == product.id)] = product;
  }

  doProductFavorite(Products product) {
    //product.selectedPriceListId! = generatedProductListId
    Get.find<HomeController>().doProductFavorite(product.selectedPriceListId!, (product.favouirte == 1)).then((value) {
      if (value) {
        product.favouirte = product.favouirte == 1 ? 0 : 1;
        updateProductPrice(product);
      } //complete productTileUpdate
    });
  }

  void addToCart({status, product_id, quantity, productData}) async {
    var data = {
      'productId': product_id,
      'status': status,
      'quantity': quantity,
      'varient_id': '',
      'choice_of_crust_id': '',
      'topping_id': '',
    };
    var result = await XHttp.request(Endpoints.c_ProductToCart, method: XHttp.POST, data: data);
    var addToCartResponse = CartStatusResponse.fromJson(jsonDecode(result.data));
    if (addToCartResponse.statusCode == 200) {
      (productData as Products).isIncart = 1;
//find active productList index from the current product object
      var productIndex = products.indexWhere((element) => element?.id == productData.id);
      //find current selected product index from the product list
      var productListIndex = productData.priceList!
          .indexWhere((element) => element.generatelistdetailsId == productData.selectedPriceListId);
      // update productList cartQty
      productData.priceList![productListIndex].cartQty = productData.priceList![productListIndex].cartQty! + 1;
      // update localTemp Varieable for temp cart count
      productData.cartTempCount = productData.priceList![productListIndex].cartQty!;
      products[productIndex] = productData;
      var cartData = CartData()
        ..totalAmount = addToCartResponse.data?.cartDatas?.totalAmount
        ..totalQty = addToCartResponse.data?.cartDatas?.totalQty
        ..totalSavings = addToCartResponse.data?.cartDatas?.totalSavings;

      Get.find<StoreDetailsController>().updateCartData(
        cartData: cartData,
        isAdded: false,
      );

      //update products data across all screens for update view
      Get.find<HomeController>().updateProductsAcrossAllScreens(productData);

      SnackBarSuccess(titleText: "Success", messageText: "${addToCartResponse.data?.message}").show();
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void updateCartCount(Products product, String status) async {
    //find active productList index from the current product object
    var productListIndex =
        product.priceList!.indexWhere((element) => element.generatelistdetailsId == product.selectedPriceListId);
    print('Zeekoi $productListIndex ${product.toJson()}');
    //find current selected product index from the product list
    var productIndex = products.indexWhere((element) => element?.id == product.id);
    if (status != 'remove') {
      // update cart qty
      // var productIndex = products.indexWhere((element) => element?.id == product.id);

      var data = {
        'productId': product.selectedPriceListId,
        'quantity': status == 'decrement'
            ? product.priceList![productListIndex].cartQty! - 1
            : product.priceList![productListIndex].cartQty! + 1,
      };
      var result = await XHttp.request(Endpoints.c_ChangeCartItem, method: XHttp.POST, data: data);
      var addToCartResponse = CartStatusResponse.fromJson(jsonDecode(result.data));
      if (addToCartResponse.statusCode == 200) {
        // update product to the current product list
        product.priceList![productListIndex].cartQty = status == 'decrement'
            ? product.priceList![productListIndex].cartQty! - 1
            : product.priceList![productListIndex].cartQty! + 1;
        product.cartTempCount = product.priceList![productListIndex].cartQty!;
        products[productIndex] = product;
        var cartData = CartData()
          ..totalAmount = addToCartResponse.data?.cartDatas?.totalAmount
          ..totalQty = addToCartResponse.data?.cartDatas?.totalQty
          ..totalSavings = addToCartResponse.data?.cartDatas?.totalSavings;

        Get.find<StoreDetailsController>().updateCartData(
          cartData: cartData,
          isAdded: false,
        );
        //update products data across all screens for update view
        Get.find<HomeController>().updateProductsAcrossAllScreens(product);
        // SnackBarSuccess(titleText: "Success", messageText: "${addToCartResponse.data}").show();
      } else {
        SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
        isLoading.value = false;
      }
    } else {
      //remove item from cart if qty selected zero or less than 1

      var data = {
        'productId': product.selectedPriceListId,
        'status': 'remove',
        'quantity': 0,
        'varient_id': '',
        'choice_of_crust_id': '',
        'topping_id': '',
      };
      var result = await XHttp.request(Endpoints.c_ProductToCart, method: XHttp.POST, data: data);
      var addToCartResponse = CartStatusResponse.fromJson(jsonDecode(result.data));
      if (addToCartResponse.statusCode == 200) {
        // reset temprory update product data
        product.isIncart = 0;
        product.priceList![productListIndex].cartQty = 0;
        product.cartTempCount = 0;
        products[productIndex] = product;
        var cartData = CartData()
          ..totalAmount = addToCartResponse.data?.cartDatas?.totalAmount
          ..totalQty = addToCartResponse.data?.cartDatas?.totalQty
          ..totalSavings = addToCartResponse.data?.cartDatas?.totalSavings;

        Get.find<StoreDetailsController>().updateCartData(
          cartData: cartData,
          isAdded: false,
        );
        //update products data across all screens for update view
        Get.find<HomeController>().updateProductsAcrossAllScreens(product);

        SnackBarSuccess(titleText: "Success", messageText: "${addToCartResponse.data?.message}").show();
      } else {
        SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
        isLoading.value = false;
      }
    }
  }
}
