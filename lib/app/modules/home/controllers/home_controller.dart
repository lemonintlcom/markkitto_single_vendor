import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/UserData.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/FavoriteResponse.dart';
import 'package:markkito_customer/app/data/models/ProductListResponse.dart';
import 'package:markkito_customer/app/data/models/ShopDetailsResponse.dart';

import 'package:markkito_customer/app/modules/home/controllers/cart_update_controller.dart';
import 'package:markkito_customer/app/modules/home/controllers/products_controller.dart';

import 'package:markkito_customer/app/modules/home/views/cart_view_update.dart';
import 'package:markkito_customer/app/modules/home/views/home_content_view.dart';
import 'package:markkito_customer/app/modules/home/views/more_view.dart';
import 'package:markkito_customer/app/modules/home/views/products_view.dart';
import 'package:markkito_customer/app/modules/search/controllers/search_controller.dart';
import 'package:markkito_customer/app/modules/search/views/search_view.dart';
import 'package:markkito_customer/app/modules/store_details/controllers/store_details_controller.dart';
import 'package:markkito_customer/app/modules/store_details/views/store_details_view.dart';
import 'package:markkito_customer/app/modules/wishlist/controllers/wishlist_controller.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';
import 'package:package_info_plus/package_info_plus.dart';

class HomeController extends GetxController {
  var selectedIndex = 0.obs;

  selectIndex(int index) => selectedIndex.value = index;

  var cartWindowVisibilty = false.obs;

  var isLoggedIn = Storage.box.get(Constants.isUserLoggedIn, defaultValue: false);

  NumberFormat numberFormat = NumberFormat("#,##0.00", "en_US");

  User? user;

  PackageInfo? packageInfo;

  final List<Widget> screenList = const [
    StoreDetailsView(key: PageStorageKey("HomeContentView")),
    SearchView(key: PageStorageKey("SearchView")),
    ProductsView(key: PageStorageKey("ProductsView")),
    CartUpdateView(key: PageStorageKey("CartUpdateView")),
    // CartView(key: PageStorageKey("CartView")),
    MoreView(key: PageStorageKey("MoreView")),
  ];

  final PageStorageBucket pageStorageBucket = PageStorageBucket();

  @override
  void onInit() {
    super.onInit();

    if (isLoggedIn) {
      user = User.fromJson((jsonDecode(Storage.box.get(Constants.userdb))));
      Logger().e(jsonDecode(Storage.box.get(Constants.userdb)));
    }

    getpackageInfo();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<bool> doShopFavorite(int shopId) async {
    var data = {
      'vendorUserId': shopId,
    };
    var result = await XHttp.request(Endpoints.c_shop_favourite, method: XHttp.POST, data: data);
    var favoriteResponse = FavoriteResponse.fromJson(jsonDecode(result.data));
    if (favoriteResponse.statusCode == 200) {
      if (favoriteResponse.data?.status == 'success') {
        SnackBarSuccess(titleText: "Success", messageText: "Shop added to favorite").show();
        return true;
      } else {
        SnackBarFailure(titleText: "Failed", messageText: "Unable to favorite").show();
        return false;
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      return false;
    }
  }

  Future<bool> doProductFavorite(int generateproductlistid, bool hasFavorite) async {
    var data = {
      'productId': generateproductlistid,
    };
    var result = await XHttp.request(Endpoints.c_createdProductFavorate, method: XHttp.POST, data: data);
    var favoriteResponse = FavoriteResponse.fromJson(jsonDecode(result.data));
    if (favoriteResponse.statusCode == 200) {
      if (favoriteResponse.data?.status == 'success') {
        SnackBarSuccess(titleText: "Success", messageText: hasFavorite ? "Removed From wishlist" : "Added to wishlist")
            .show();
        return true;
      } else {
        SnackBarFailure(titleText: "Failed", messageText: "Add to wishlist failed").show();
        return false;
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      return false;
    }
  }

  Future<bool> willPopCallbackCartRefresh() async {
    // await showDialog or Show add banners or whatever
    // then
    Future.delayed(const Duration(milliseconds: 200), () {
      Get.find<CartUpdateController>().getCartDetails();
    });
    return Future.value(true);
  }

  void navigateToCart() {
    Get.back();
    Get.back();
    selectIndex(3);
    cartWindowVisibilty.value = false;
    try {
      Future.delayed(const Duration(milliseconds: 200), () {
        Get.find<CartUpdateController>().getCartDetails();
      });
    } on Exception catch (_) {
      log('never reached');
    }
  }

  void navigateToProductPage() {
    selectIndex(2);
    cartWindowVisibilty.value = false;
  }

  void navigateToSearchPage() {
    selectIndex(1);
    cartWindowVisibilty.value = false;
  }

  void updateProductsAcrossAllScreens(Products productData) {
    try {
      if (Get.isRegistered<StoreDetailsController>()) {
        if (Get.find<StoreDetailsController>().products.isNotEmpty) {
          var productUpdateIndex =
              Get.find<StoreDetailsController>().products.indexWhere((element) => element?.id == productData.id);
          if (productUpdateIndex >= 0) {
            Get.find<StoreDetailsController>().products[productUpdateIndex] = productData;
          }
        }
      }
    } on Exception catch (_) {}

    try {
      if (Get.isRegistered<ProductsController>()) {
        if (Get.find<ProductsController>().products.isNotEmpty) {
          var productUpdateIndex =
              Get.find<ProductsController>().products.indexWhere((element) => element?.id == productData.id);
          if (productUpdateIndex >= 0) {
            Get.find<ProductsController>().products[productUpdateIndex] = productData;
          }
        }
      }
    } on Exception catch (_) {}

    try {
      if (Get.isRegistered<SearchController>()) {
        if (Get.find<SearchController>().products.isNotEmpty) {
          var productUpdateIndex =
              Get.find<SearchController>().products.indexWhere((element) => element?.id == productData.id);
          if (productUpdateIndex >= 0) {
            Get.find<SearchController>().products[productUpdateIndex] = productData;
          }
        }
      }
    } on Exception catch (_) {}

    try {
      if (Get.isRegistered<WishListController>()) {
        if (Get.find<WishListController>().products.isNotEmpty) {
          var productUpdateIndex =
              Get.find<WishListController>().products.indexWhere((element) => element?.id == productData.id);
          if (productUpdateIndex >= 0) {
            Get.find<WishListController>().products[productUpdateIndex] = productData;
          }
        }
      }
    } on Exception catch (_) {}
  }

  void updateProductsCountAcrossAllScreens(
      {int? generatedProductId, int? isIncart, int? cartTempCount, PriceList? priceList}) {
    try {
      if (Get.isRegistered<StoreDetailsController>()) {
        if (Get.find<StoreDetailsController>().products.isNotEmpty) {
          var productUpdateIndex =
              Get.find<StoreDetailsController>().products.indexWhere((element) => element?.id == generatedProductId);
          if (productUpdateIndex >= 0) {
            var tempProduct = Get.find<StoreDetailsController>().products[productUpdateIndex];
            tempProduct!.cartTempCount = cartTempCount!;
            tempProduct.isIncart = isIncart;

            var productPriceListIndex = tempProduct.priceList!
                .indexWhere((element) => element.generatelistdetailsId == priceList!.generatelistdetailsId);
            if (productPriceListIndex >= 0) {
              tempProduct.priceList![productPriceListIndex] = priceList!;
            }

            Get.find<StoreDetailsController>().products[productUpdateIndex] = tempProduct;

            // Get.find<StoreDetailsController>().products[productUpdateIndex]!.isIncart = isIncart;
            // Get.find<StoreDetailsController>().products[productUpdateIndex]!.cartTempCount = cartTempCount;

          }
        }
      }
    } on Exception catch (_) {}

    try {
      if (Get.isRegistered<ProductsController>()) {
        if (Get.find<ProductsController>().products.isNotEmpty) {
          var productUpdateIndex =
              Get.find<ProductsController>().products.indexWhere((element) => element?.id == generatedProductId);
          if (productUpdateIndex >= 0) {
            var tempProduct = Get.find<ProductsController>().products[productUpdateIndex];
            tempProduct!.cartTempCount = cartTempCount!;
            tempProduct.isIncart = isIncart;
            var productPriceListIndex = tempProduct.priceList!
                .indexWhere((element) => element.generatelistdetailsId == priceList!.generatelistdetailsId);
            if (productPriceListIndex >= 0) {
              tempProduct.priceList![productPriceListIndex] = priceList!;
            }

            Get.find<ProductsController>().products[productUpdateIndex] = tempProduct;
            // Get.find<ProductsController>().products[productUpdateIndex] = productData;
          }
        }
      }
    } on Exception catch (_) {}

    try {
      if (Get.isRegistered<SearchController>()) {
        if (Get.find<SearchController>().products.isNotEmpty) {
          var productUpdateIndex =
              Get.find<SearchController>().products.indexWhere((element) => element?.id == generatedProductId);
          if (productUpdateIndex >= 0) {
            var tempProduct = Get.find<SearchController>().products[productUpdateIndex];
            tempProduct!.cartTempCount = cartTempCount!;
            tempProduct.isIncart = isIncart;
            var productPriceListIndex = tempProduct.priceList!
                .indexWhere((element) => element.generatelistdetailsId == priceList!.generatelistdetailsId);
            if (productPriceListIndex >= 0) {
              tempProduct.priceList![productPriceListIndex] = priceList!;
            }

            Get.find<SearchController>().products[productUpdateIndex] = tempProduct;
            // Get.find<SearchController>().products[productUpdateIndex] = productData;
          }
        }
      }
    } on Exception catch (_) {}

    try {
      if (Get.isRegistered<WishListController>()) {
        if (Get.find<WishListController>().products.isNotEmpty) {
          var productUpdateIndex =
              Get.find<WishListController>().products.indexWhere((element) => element?.id == generatedProductId);
          if (productUpdateIndex >= 0) {
            var tempProduct = Get.find<WishListController>().products[productUpdateIndex];
            tempProduct!.cartTempCount = cartTempCount!;
            tempProduct.isIncart = isIncart;
            var productPriceListIndex = tempProduct.priceList!
                .indexWhere((element) => element.generatelistdetailsId == priceList!.generatelistdetailsId);
            if (productPriceListIndex >= 0) {
              tempProduct.priceList![productPriceListIndex] = priceList!;
            }

            Get.find<WishListController>().products[productUpdateIndex] = tempProduct;
            // Get.find<WishListController>().products[productUpdateIndex] = productData;
          }
        }
      }
    } on Exception catch (_) {}
  }

  void logout() async {
    await Storage.box.clear();
    Get.offAllNamed(Routes.SPLASH);
  }

  void getpackageInfo() async {
    packageInfo = await PackageInfo.fromPlatform();
  }
}
