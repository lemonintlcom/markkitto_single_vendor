import 'package:get/get.dart';

import 'package:markkito_customer/app/modules/home/controllers/cart_update_controller.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_content_controller.dart';
import 'package:markkito_customer/app/modules/home/controllers/more_controller.dart';
import 'package:markkito_customer/app/modules/home/controllers/products_controller.dart';
import 'package:markkito_customer/app/modules/search/controllers/search_controller.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ProductsController>(
      () => ProductsController(),
    );
    Get.lazyPut<MoreController>(
      () => MoreController(),
    );
    Get.lazyPut<SearchController>(
      () => SearchController(),
    );
    Get.lazyPut<HomeContentController>(
      () => HomeContentController(),
    );
    Get.lazyPut<CartUpdateController>(
      () => CartUpdateController(),
    );
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
    Get.lazyPut<ProductsController>(
      () => ProductsController(),
    );
  }
}
