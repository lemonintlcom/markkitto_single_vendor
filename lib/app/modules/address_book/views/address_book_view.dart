import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:markkito_customer/app/modules/custom_widgets/bottom_sheet_modal.dart';
import 'package:markkito_customer/app/modules/custom_widgets/custom_app_bar.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/generated/assets.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/color_utils.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../controllers/address_book_controller.dart';

class AddressBookView extends GetView<AddressBookController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: const CustomAppBar(
          titleText: 'Saved Address',
        ),
        floatingActionButton: Obx(() {
          return AnimatedSlide(
            duration: controller.duration,
            offset: controller.showFab.value ? Offset.zero : const Offset(0, 2),
            child: FloatingActionButton.extended(
              label: const Text('Add Address'),
              icon: const Icon(Icons.reviews),
              backgroundColor: Storage.instance.getValue(Constants.colorCode) == null
                  ? AppColors.primary_color
                  : hexToColor(Storage.instance.getValue(Constants.colorCode)),
              onPressed: () {
                Get.find<HomeController>().isLoggedIn
                    ? Get.toNamed(Routes.ADDRESS_BOOK_EDIT, arguments: {'addNew': true})
                    : BottomSheetModal().showLoginRequired(context);
              },
            ),
          );
        }),
        body: Obx(() {
          return controller.isLoading.value
              ? Container()
              : controller.addressList.isEmpty
                  ? Padding(
                      padding: const EdgeInsets.all(paddingExtraLarge),
                      child: Center(
                        child: Lottie.asset(Assets.lottieNodata, repeat: false),
                      ),
                    )
                  : ListView.separated(
                      controller: controller.controller,
                      itemCount: controller.addressList.length,
                      separatorBuilder: (BuildContext context, int index) => const Divider(height: 1),
                      itemBuilder: (BuildContext context, int index) {
                        var item = controller.addressList[index];
                        return Padding(
                          padding: const EdgeInsets.only(top: 4.0, bottom: 4.0),
                          child: ListTile(
                            trailing: Wrap(
                              spacing: 12,
                              children: [
                                TextButton(
                                  onPressed: () {
                                    controller.onEditAddress(item, index);
                                  },
                                  child: const Icon(Icons.edit, color: AppColors.primary_color),
                                  style: TextButton.styleFrom(
                                    minimumSize: Size.zero,
                                    padding: EdgeInsets.zero,
                                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                  ),
                                ),
                                TextButton(
                                  onPressed: () {
                                    controller.onDeleteAddress(item, index, context);
                                  },
                                  child: const Icon(Icons.delete, color: Colors.red),
                                  style: TextButton.styleFrom(
                                    minimumSize: Size.zero,
                                    padding: EdgeInsets.zero,
                                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                  ),
                                )
                              ],
                            ),
                            leading: const Icon(Icons.location_pin),
                            subtitle: Text('${item.address1}\n${item.contact1}',
                                softWrap: true,
                                overflow: TextOverflow.fade,
                                style: captionLite.copyWith(
                                    fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black)),
                          ),
                        );
                      },
                    );
        }),
      ),
    );
  }
}
