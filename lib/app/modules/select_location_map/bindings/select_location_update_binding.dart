import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/select_location_map/controllers/select_location_update_controller.dart';

class SelectLocationUpdateBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SelectLocationUpdateController>(
      () => SelectLocationUpdateController(),
    );
  }
}
