import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:markkito_customer/app/modules/custom_widgets/custom_app_bar.dart';
import 'package:markkito_customer/app/modules/custom_widgets/primary_button.dart';
import 'package:markkito_customer/app/modules/select_location_map/controllers/select_location_update_controller.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/generated/assets.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

class SelectLocationUpdateView extends GetView<SelectLocationUpdateController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(() {
        return controller.isLoading.value
            ? Container()
            : Column(
                children: [
                  Container(
                    height: Get.height * .7,
                    child: GoogleMap(
                      markers: controller.markers.values.toSet(),
                      myLocationEnabled: true,
                      onMapCreated: controller.onMapCreated,
                      onLongPress: (position) {
                        controller.updateNewMarkerPosition(position);
                      },
                      initialCameraPosition: CameraPosition(
                        target: LatLng(
                            controller.initialPosition.value!.latitude, controller.initialPosition.value!.longitude),
                        zoom: 13.0,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(paddingLarge),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: Row(
                                  children: [
                                    SvgPicture.asset(
                                      Assets.svgLocation,
                                      width: 16,
                                    ),
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    Obx(() {
                                      return Flexible(
                                        child: Text(
                                          controller.placemarks.value,
                                          style: subtitleLite.copyWith(fontWeight: FontWeight.bold),
                                          maxLines: 2,
                                          textAlign: TextAlign.start,
                                          overflow: TextOverflow.fade,
                                          softWrap: true,
                                        ),
                                      );
                                    }),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              MaterialButton(
                                color: Colors.grey.shade200,
                                textColor: AppColors.primary_color,
                                height: 32,
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(4),
                                    ),
                                    side: BorderSide(color: Colors.grey.shade300)),
                                onPressed: () async {
                                  controller.showOverlayPlaces();
                                },
                                child: Text(
                                  'CHANGE',
                                  style: caption,
                                ),
                              ),
                            ],
                          ),
                          // Obx(() {
                          //   return Text(
                          //     controller.currentAddress.value?.address1,
                          //     style: captionLite,
                          //     maxLines: 2,
                          //     textAlign: TextAlign.start,
                          //     overflow: TextOverflow.fade,
                          //     softWrap: true,
                          //   );
                          // }),
                          const SizedBox(
                            height: paddingExtraLarge,
                          ),

                          Row(
                            children: [
                              Expanded(
                                child: PrimaryButton(
                                  text: 'Confirm Location',
                                  onTap: () {
                                    if (Get.arguments['toLogin'] as bool) {
                                      Get.toNamed(Routes.LOGIN);
                                    } else {
                                      Get.arguments['fromHome'] as bool ? Get.back() : Get.offAllNamed(Routes.HOME);
                                    }
                                    // Get.parameters['fromHome'];
                                  },
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              );
      }),
    );
  }
}
