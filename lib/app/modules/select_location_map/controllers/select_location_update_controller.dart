import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/AddressListResponse.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:google_api_headers/google_api_headers.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:location/location.dart' as loc;

class SelectLocationUpdateController extends GetxController {
  var isLoading = true.obs;

  late GoogleMapController mapController;
  late TextEditingController mobileNumberController;

  var box = Hive.box(Constants.configName);

  void onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  var markers = <MarkerId, Marker>{}.obs;

  var placemarks = ''.obs;
  var locality = ''.obs;

  loc.Location location = loc.Location();
  // var initialPosition = LatLng(-15.4630239974464, 28.363397732282127).obs;
  Rx<LatLng?> initialPosition = Rx(null);

  @override
  void onInit() {
    super.onInit();
    initialPosition.value = const LatLng(0.0, 0.0);
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      SnackBarFailure(messageText: 'Location services are disabled.', titleText: 'Oops..').show();
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        SnackBarFailure(messageText: 'Please turned on your device location.', titleText: 'Location unavailable')
            .show();
      }
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        SnackBarFailure(messageText: 'Location permissions are denied.', titleText: 'Oops..').show();

        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      SnackBarFailure(
              messageText: 'Location permissions are permanently denied, we cannot request permissions.',
              titleText: 'Oops..')
          .show();
      return Future.error('Location permissions are permanently denied, we cannot request permissions');
    }

    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    initialPosition.value = LatLng(position.latitude, position.longitude);
    // addOrUpdateMarker();

    // await getGeoCodingAddress();
    updateNewMarkerPosition(initialPosition.value!);
    isLoading.value = false;
    return position;
  }

  @override
  void onReady() {
    super.onReady();
    if (box.get(Constants.hasUserLocation, defaultValue: false)) {
      locality.value = box.get(Constants.locality, defaultValue: "");
      placemarks.value = box.get(Constants.locationText, defaultValue: "");
      initialPosition.value = LatLng(box.get(Constants.latitude), box.get(Constants.longitude));

      Logger().e('${initialPosition.value!.latitude}');
      // await getGeoCodingAddress();

      Future.delayed(const Duration(milliseconds: 400)).then((value) {
        addOrUpdateMarker();
        isLoading.value = false;
        //   mapController.animateCamera(
        //     CameraUpdate.newCameraPosition(
        //       CameraPosition(target: initialPosition.value!, zoom: 13),
        //     ),
        //   );
      });
    } else {
      Future.delayed(const Duration(seconds: 2));
      isLoading.value = false;
      _determinePosition();
    }
  }

  @override
  void onClose() {}

  getGeoCodingAddress() async {
    String? placeText;
    List<Placemark> placemarks1 =
        await placemarkFromCoordinates(initialPosition.value!.latitude, initialPosition.value!.longitude);
    if (placemarks1.isNotEmpty) {
      Logger().e(placemarks1[0]);
      placeText =
          "${placemarks1[0].street} ${placemarks1[0].locality},${placemarks1[0].name},${placemarks1[0].subAdministrativeArea}";
      placemarks.value = placeText;
      locality.value = "${placemarks1[0].locality}";
    }
    box.put(Constants.latitude, initialPosition.value!.latitude);
    box.put(Constants.longitude, initialPosition.value!.longitude);
    box.put(Constants.locationText, placeText);
    box.put(Constants.hasUserLocation, true);
    box.put(Constants.buildingDetails, placemarks1[0].name);
    box.put(Constants.streetDetails, placemarks1[0].street);
    box.put(Constants.locality, placemarks1[0].locality);
    box.put(Constants.state, placemarks1[0].administrativeArea);
    box.put(Constants.district, placemarks1[0].subAdministrativeArea);
    box.put(Constants.country, placemarks1[0].country);
    box.put(Constants.city, placemarks1[0].subAdministrativeArea);
    box.put(Constants.pinCode, placemarks1[0].postalCode);
    box.put(Constants.landmark, placemarks1[0].thoroughfare);
    addOrUpdateMarker(); //update
  }

  void addOrUpdateMarker() {
    final marker = Marker(
      markerId: MarkerId('1'),
      position: LatLng(initialPosition.value!.latitude, initialPosition.value!.longitude),
      // icon: BitmapDescriptor.,
      draggable: true,
      onDragEnd: (position) {
        updateNewMarkerPosition(position);
      },
      infoWindow: InfoWindow(
        title: placemarks.value,
        snippet: placemarks.value,
      ),
    );
    markers[MarkerId('1')] = marker;
    Future.delayed(const Duration(seconds: 1)).then((value) {
      mapController.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: initialPosition.value!, zoom: 13),
        ),
      );
    });
  }

  updateNewMarkerPosition(LatLng position) async {
    // final marker = markers.values.toList().firstWhere((item) => item.markerId == currentAddress.value?.addressId);

    String? placeText;
    List<Placemark> placemarks1 = await placemarkFromCoordinates(position.latitude, position.longitude);
    if (placemarks1.isNotEmpty) {
      Logger().e(placemarks1[0]);
      placeText =
          "${placemarks1[0].street} ${placemarks1[0].locality},${placemarks1[0].name},${placemarks1[0].subAdministrativeArea}";
      // placemarks.value = placeText;
      // locality.value = "${placemarks1[0].locality}";
      placemarks.value = placeText;
      initialPosition.value = LatLng(position.latitude, position.longitude);
      box.put(Constants.latitude, initialPosition.value!.latitude);
      box.put(Constants.longitude, initialPosition.value!.longitude);
      box.put(Constants.locationText, placeText);
      box.put(Constants.hasUserLocation, true);
      box.put(Constants.buildingDetails, placemarks1[0].name);
      box.put(Constants.streetDetails, placemarks1[0].street);
      box.put(Constants.locality, placemarks1[0].locality);
      box.put(Constants.state, placemarks1[0].administrativeArea);
      box.put(Constants.district, placemarks1[0].subAdministrativeArea);
      box.put(Constants.country, placemarks1[0].country);
      box.put(Constants.city, placemarks1[0].subAdministrativeArea);
      box.put(Constants.pinCode, placemarks1[0].postalCode);
      box.put(Constants.landmark, placemarks1[0].thoroughfare);
      addOrUpdateMarker(); //update
      // getGeoCodingAddress();
    }
  }

  // show input autocomplete with selected mode
  // then get the Prediction selected
  Future<void> showOverlayPlaces() async {
    Prediction? p = await PlacesAutocomplete.show(
      context: Get.context!,
      apiKey: 'AIzaSyAODoObqlpreCc_Rx86b4FFJXG702X5TDI',
      onError: (e) {},
      mode: Mode.overlay,
      strictbounds: false,
      types: [],
      language: "en",
      decoration: InputDecoration(
        hintText: 'Search',
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
          borderSide: const BorderSide(
            color: Colors.white,
          ),
        ),
      ),
      components: [Component(Component.country, "in"), Component(Component.country, "ae")],
    );

    displayPrediction(p, Get.context!);
  }

  Future<void> displayPrediction(Prediction? p, BuildContext context) async {
    if (p != null) {
      // get detail (lat/lng)
      GoogleMapsPlaces _places = GoogleMapsPlaces(
        apiKey: 'AIzaSyAODoObqlpreCc_Rx86b4FFJXG702X5TDI',
        apiHeaders: await const GoogleApiHeaders().getHeaders(),
      );
      PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId!);
      final lat = detail.result.geometry!.location.lat;
      final lng = detail.result.geometry!.location.lng;

      mapController.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(lat, lng), zoom: 13),
        ),
      );
      // isLoading.value = true;
      await updateNewMarkerPosition(LatLng(lat, lng));
      // isLoading.value = false;
      // ScaffoldMessenger.of(context).showSnackBar(
      //   SnackBar(content: Text("${p.description} - $lat/$lng")),
      // );
    }
  }

  void updateAddress() async {
    /* var data = {
      'vendorUserId': shopId,
    };
    var result = await XHttp.request(Endpoints.c_shop_favourite, method: XHttp.POST, data: data);
    var favoriteResponse = FavoriteResponse.fromJson(jsonDecode(result.data));
    if (favoriteResponse.statusCode == 200) {
      if (favoriteResponse.data?.status == 'success') {
        SnackBarSuccess(titleText: "Success", messageText: "Shop added to favorite").show();
      } else {
        SnackBarFailure(titleText: "Failed", messageText: "Unable to favorite").show();
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
    }*/
  }
}
