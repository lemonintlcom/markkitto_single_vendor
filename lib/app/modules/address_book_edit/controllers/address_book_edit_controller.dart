import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/AddressListResponse.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:google_api_headers/google_api_headers.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:markkito_customer/app/data/models/FavoriteResponse.dart';
import 'package:markkito_customer/app/modules/address_book/controllers/address_book_controller.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:location/location.dart' as loc;
import 'package:markkito_customer/utils/storage.dart';

class AddressBookEditController extends GetxController {
  Rx<Address?> currentAddress = Rx(null);
  var isLoading = true.obs;

  late GoogleMapController mapController;
  late TextEditingController mobileNumberController;

  String? completePhoneNumber;

  String? countryCode;

  String? phoneNumber;

  String? buildingDetails;

  String? streetDetails;

  String? locality;

  String? state;

  String? district;

  String? country;

  String? city;

  String? pinCode;

  String? landmark;

  var addNew = false.obs;

  loc.Location location = loc.Location();

  void onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  var markers = <MarkerId, Marker>{}.obs;

  @override
  void onInit() {
    super.onInit();
    addNew.value = Get.arguments['addNew'];
    mobileNumberController = TextEditingController();
    if (addNew.value) {
      phoneNumber = '${Get.find<HomeController>().user?.mobileNo}';
      mobileNumberController.text = '${Get.find<HomeController>().user?.mobileNo}';
      countryCode = '971';
      _determinePosition();
    } else {
      log('${Get.arguments['position']}');
      log('${Get.arguments['data']}');
      var data = jsonEncode(Get.arguments['data']);
      log('${data}');
      currentAddress.value = Address.fromJson(jsonDecode(data));
      log('${currentAddress.value?.addressId}');
      isLoading.value = false;
      mobileNumberController.text = '${currentAddress.value?.contact1}';

      addOrUpdateMarker();
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      SnackBarFailure(messageText: 'Location services are disabled.', titleText: 'Oops..').show();
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        SnackBarFailure(messageText: 'Please turned on your device location.', titleText: 'Location unavailable')
            .show();
      }
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        SnackBarFailure(messageText: 'Location permissions are denied.', titleText: 'Oops..').show();

        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      SnackBarFailure(
              messageText: 'Location permissions are permanently denied, we cannot request permissions.',
              titleText: 'Oops..')
          .show();
      return Future.error('Location permissions are permanently denied, we cannot request permissions');
    }

    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    var address = Address()
      ..latitude = position.latitude
      ..longitude = position.longitude
      ..addressId = 100
      ..contact1 = Get.find<HomeController>().user?.mobileNo;
    currentAddress.value = address;

    // addOrUpdateMarker();

    // await getGeoCodingAddress();
    updateNewMarkerPosition(LatLng(position.latitude, position.longitude));
    isLoading.value = false;
    return position;
  }

  void addOrUpdateMarker() {
    final marker = Marker(
      markerId: MarkerId('${currentAddress.value?.addressId}'),
      position: LatLng(currentAddress.value?.latitude, currentAddress.value?.longitude),
      // icon: BitmapDescriptor.,
      draggable: true,
      onDragEnd: (position) {
        updateNewMarkerPosition(position);
      },
      infoWindow: InfoWindow(
        title: '${currentAddress.value?.address1}',
        snippet: '${currentAddress.value?.address1}\n${currentAddress.value?.contact1}',
      ),
    );
    markers[MarkerId('${currentAddress.value?.addressId}')] = marker;
  }

  updateNewMarkerPosition(LatLng position) async {
    // final marker = markers.values.toList().firstWhere((item) => item.markerId == currentAddress.value?.addressId);

    String? placeText;
    List<Placemark> placemarks1 = await placemarkFromCoordinates(position.latitude, position.longitude);
    if (placemarks1.isNotEmpty) {
      Logger().e(placemarks1[0]);
      placeText =
          "${placemarks1[0].street} ${placemarks1[0].locality},${placemarks1[0].name},${placemarks1[0].subAdministrativeArea}";
      // placemarks.value = placeText;
      // locality.value = "${placemarks1[0].locality}";
      currentAddress.value?.address1 = placeText;
      currentAddress.value?.latitude = position.latitude;
      currentAddress.value?.longitude = position.longitude;
      buildingDetails = placemarks1[0].name;
      streetDetails = placemarks1[0].street;
      locality = placemarks1[0].locality;
      state = placemarks1[0].administrativeArea;
      district = placemarks1[0].subAdministrativeArea;
      country = placemarks1[0].country;
      city = placemarks1[0].subAdministrativeArea;
      pinCode = placemarks1[0].postalCode;
      landmark = placemarks1[0].thoroughfare;
      addOrUpdateMarker(); //update
    }
  }

  // show input autocomplete with selected mode
  // then get the Prediction selected
  Future<void> showOverlayPlaces() async {
    Prediction? p = await PlacesAutocomplete.show(
      context: Get.context!,
      apiKey: 'AIzaSyAODoObqlpreCc_Rx86b4FFJXG702X5TDI',
      onError: (e) {},
      mode: Mode.overlay,
      strictbounds: false,
      types: [],
      language: "en",
      decoration: InputDecoration(
        hintText: 'Search',
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
          borderSide: const BorderSide(
            color: Colors.white,
          ),
        ),
      ),
      components: [Component(Component.country, "in"), Component(Component.country, "ae")],
    );

    displayPrediction(p, Get.context!);
  }

  Future<void> displayPrediction(Prediction? p, BuildContext context) async {
    if (p != null) {
      // get detail (lat/lng)
      GoogleMapsPlaces _places = GoogleMapsPlaces(
        apiKey: 'AIzaSyAODoObqlpreCc_Rx86b4FFJXG702X5TDI',
        apiHeaders: await const GoogleApiHeaders().getHeaders(),
      );
      PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId!);
      final lat = detail.result.geometry!.location.lat;
      final lng = detail.result.geometry!.location.lng;

      mapController.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(lat, lng), zoom: 13),
        ),
      );
      // isLoading.value = true;
      await updateNewMarkerPosition(LatLng(lat, lng));
      // isLoading.value = false;
      // ScaffoldMessenger.of(context).showSnackBar(
      //   SnackBar(content: Text("${p.description} - $lat/$lng")),
      // );
    }
  }

  Future<void> addNewAddress(BuildContext context) async {
    var data = {
      'address2': locality,
      'address1': currentAddress.value?.address1,
      'DeliveryContact1': '$countryCode$phoneNumber',
      'DeliveryContact2': '',
      'usertypeid': Get.find<HomeController>().user?.userId,
      'BuildingDetails': currentAddress.value?.address1,
      'StreetDetails': streetDetails,
      'Locality': locality,
      'State': state,
      'City': city,
      'PinCode': pinCode,
      'Landmark': landmark,
      'Latitude': currentAddress.value?.latitude,
      'Longitude': currentAddress.value?.longitude,
    };
    var result = await XHttp.request(Endpoints.i_user_address, method: XHttp.POST, data: data);
    var responseData = FavoriteResponse.fromJson(jsonDecode(result.data));
    if (responseData.statusCode == 200) {
      if (responseData.data?.status == 'success') {
        Get.back();
        Get.find<AddressBookController>().getAddress();
      } else {
        SnackBarFailure(titleText: "Failed", messageText: "Unable to save user location. Try again").show();
      }
    }
  }

  void updateAddress(BuildContext context) async {
    var data = {
      'addressid': currentAddress.value?.addressId,
      'BuildingDetails': currentAddress.value?.address1,
      'Latitude': currentAddress.value?.latitude,
      'Longitude': currentAddress.value?.longitude,
      'StreetDetails': streetDetails,
      'Locality': locality,
      'State': state,
      'City': city,
      'PinCode': pinCode,
      'Landmark': landmark,
    };
    var result = await XHttp.request(Endpoints.r_user_address, method: XHttp.POST, data: data);
    if (result.code == 200) {
      SnackBarSuccess(titleText: "Success", messageText: "Address successfully updated").show();
      Get.find<AddressBookController>().getAddress();
      Navigator.pop(context);
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
    }
    // var favoriteResponse = FavoriteResponse.fromJson(jsonDecode(result.data));
    // if (favoriteResponse.statusCode == 200) {
    //   if (favoriteResponse.data?.status == 'success') {
    //     SnackBarSuccess(titleText: "Success", messageText: "Shop added to favorite").show();
    //   } else {
    //     SnackBarFailure(titleText: "Failed", messageText: "Unable to favorite").show();
    //   }
    // } else {
    //   SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
    // }
  }
}
