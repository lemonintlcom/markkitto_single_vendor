import 'dart:convert';

import 'package:get/get.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/BannersResponse.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../../../../themes/custom_theme.dart';

class OffersController extends GetxController {
  var isLoading = true.obs;

  var banners = <BannerData>[];

  @override
  void onInit() {
    super.onInit();
    getOffers();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  getOffers() async {
    var data = {
      'shop_id': Storage.instance.getValue(Constants.storeId),
    };
    var result = await XHttp.request(Endpoints.r_single_banners, method: XHttp.POST, data: data);
    if (result.code == 200) {
      isLoading.value = false;
      var bannerResponse = BannersResponse.fromJson(jsonDecode(result.data));
      banners.addAll(bannerResponse.banners!);
    } else {
      isLoading.value = false;
      SnackBarFailure(titleText: "Oops..", messageText: "Unable to load offers..Try again later").show();
    }
  }
}
