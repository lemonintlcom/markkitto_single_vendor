import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:markkito_customer/app/modules/custom_widgets/custom_app_bar.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/generated/assets.dart';

import '../controllers/offers_controller.dart';

class OffersView extends GetView<OffersController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: const CustomAppBar(
            titleText: 'Offers',
          ),
          body: Obx(() {
            return controller.isLoading.value
                ? Container()
                : controller.banners.isEmpty
                    ? Padding(
                        padding: const EdgeInsets.all(paddingExtraLarge),
                        child: Center(
                          child: Lottie.asset(Assets.lottieNodata, repeat: false),
                        ),
                      )
                    : ListView.builder(
                        itemCount: controller.banners.length,
                        itemBuilder: (context, index) {
                          var item = controller.banners[index];
                          return InkWell(
                            onTap: () {
                              Get.toNamed(Routes.CATEGORY_PRODUCTS, arguments: {
                                'categoryName': 'Offer products',
                                'categoryId': item.id,
                                'type': 'banner'
                              });
                            },
                            child: Padding(
                              key: ValueKey(item.id),
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                height: 150,
                                width: Get.mediaQuery.size.width,
                                decoration: BoxDecoration(
                                  image: DecorationImage(image: NetworkImage('${item.url}'), fit: BoxFit.fill),
                                ),
                              ),
                            ),
                          );
                        },
                      );
          })),
    );
  }
}
