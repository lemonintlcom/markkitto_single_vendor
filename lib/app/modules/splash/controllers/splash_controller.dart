import 'dart:convert';
import 'dart:io';

import 'package:country_codes/country_codes.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/CountryList.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/api_service/remote_services.dart';
import 'package:markkito_customer/app/data/models/AppConfigResponse.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

import '../../../routes/app_pages.dart';

class SplashController extends GetxController {
  var box = Hive.box(Constants.configName);

  var test = "";

  var isLoading = true.obs;
  @override
  void onInit() {
    super.onInit();
    // setConfigAndLoadApp();
    if (box.get(Constants.isAppConfigLoaded, defaultValue: false)) {
      Future.delayed(const Duration(seconds: 1), () {
        if (box.get(Constants.isUserLoggedIn, defaultValue: false)) {
          Get.offAndToNamed(Routes.HOME);
        } else {
          Get.offAndToNamed(Routes.INTRO);
        }
      });
    } else {
      getAppConfig();
    }
  }

  @override
  void onClose() {}

  getDeviceId() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      // import 'dart:io'
      var iosDeviceInfo = await deviceInfo.iosInfo;
      box.put(Constants.deviceId, iosDeviceInfo.identifierForVendor);
      // unique ID on iOS
    } else {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      box.put(Constants.deviceId, androidDeviceInfo.androidId); // unique ID on Android
    }
    Logger().i('Device id ${box.get(Constants.deviceId)}');
  }

  void getAppConfig() async {
    var data = {'application_id': 2}; //aromagallery.markkito.ae
    // var data = {'application_id': 1};
    // var result = await RemoteService.postHTTP(Endpoints.r_get_app_conf, data);
    box.put(Constants.baseUrl, 'https://uae.markkito.com/');
    var result = await XHttp.request(Endpoints.r_get_app_conf, method: XHttp.POST, data: data);
    // method: XHttp.POST, data: data, baseUrl: 'http://uae.markkito.com/api/');

    if (result.code == 200) {
      var appConfigResponse = AppConfigResponse.fromJson(jsonDecode(result?.data));
      box.get(Constants.deviceId) ?? getDeviceId();
      box.put(Constants.colorCode, appConfigResponse.data?.colorCode);
      box.put(Constants.appName, appConfigResponse.data?.appName);
      box.put(Constants.storeName, appConfigResponse.data?.storeName);
      box.put(Constants.tagLine, appConfigResponse.data?.tagLine);
      box.put(Constants.isVendorActive, appConfigResponse.data?.isActive);
      box.put(Constants.currencySymbol, appConfigResponse.data?.currencySymbol);
      box.put(Constants.currency, appConfigResponse.data?.currency);
      box.put(Constants.country, appConfigResponse.data?.country);
      box.put(Constants.countryMobileCode, appConfigResponse.data?.countryCode);
      box.put(Constants.countryId, appConfigResponse.data?.CountryId);
      box.put(Constants.locationText, appConfigResponse.data?.location);
      box.put(Constants.latitude, appConfigResponse.data?.latitude);
      box.put(Constants.longitude, appConfigResponse.data?.longitude);
      box.put(Constants.longitude, appConfigResponse.data?.longitude);
      box.put(Constants.contact1, appConfigResponse.data?.contact1);
      box.put(Constants.contact2, appConfigResponse.data?.contact2);
      box.put(Constants.email, appConfigResponse.data?.email);
      box.put(Constants.logo, appConfigResponse.data?.logo);
      box.put(Constants.storeType, appConfigResponse.data?.storeType);
      box.put(Constants.storeId, appConfigResponse.data?.storeId);
      box.put(Constants.displayName, appConfigResponse.data?.displayName);
      box.put(Constants.whatsappContact, appConfigResponse.data?.whatsappNo);
      box.put(Constants.termsCondition, appConfigResponse.data?.terms_condition);
      box.put(Constants.privacyPolicy, appConfigResponse.data?.privacy_policy);
      box.put(Constants.globalCountryCode, appConfigResponse.data?.GlobalCountryCode);
      box.put(Constants.isGAuthEnabled, appConfigResponse.data?.gauth_enabled);

      box.put(Constants.baseUrl, appConfigResponse.data?.baseUrl);

      box.put(Constants.storeDb, jsonEncode(jsonEncode(appConfigResponse.data?.toJson())));
      box.put(Constants.isAppConfigLoaded, true);

      SnackBarFailure(messageText: 'Something went wrong', titleText: '${appConfigResponse.data?.CountryId}');

      if (box.get(Constants.isUserLoggedIn, defaultValue: false)) {
        Get.offAndToNamed(Routes.HOME);
      } else {
        Get.offAndToNamed(Routes.INTRO);
      }
      isLoading.value = false;
    } else {
      isLoading.value = false;
      Logger().e(result.code);
      SnackBarFailure(messageText: 'Something went wrong', titleText: 'Oops');
    }
  }
}
