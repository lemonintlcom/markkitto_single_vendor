import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/CountryList.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/generated/assets.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../controllers/splash_controller.dart';

class SplashView extends GetView<SplashController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Expanded(
          child: Center(
            child: Storage.box.get(Constants.logo, defaultValue: '') == ''
                ? Container()
                : Image.network(
                    Storage.box.get(Constants.logo),
                    height: 120,
                    width: 120,
                  ),
          ),
        ),
        Obx(() {
          return Padding(
            padding: const EdgeInsets.all(paddingLarge),
            child: controller.isLoading.value ? const CircularProgressIndicator() : Container(),
          );
        }),
        Padding(
          padding: const EdgeInsets.only(bottom: 48.0, left: paddingLarge, right: paddingLarge),
          child: Text(
            Storage.box.get(Constants.logo, defaultValue: '') == ''
                ? 'Buy Everywhere ${controller.test}'
                : Storage.box.get(Constants.tagLine),
            textAlign: TextAlign.center,
            style: headline6.copyWith(fontSize: 20, fontWeight: FontWeight.normal, color: Colors.grey),
          ),
        )
      ],
    ));
  }
}
