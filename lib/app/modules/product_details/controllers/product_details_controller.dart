import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/CartStatusResponse.dart';
import 'package:markkito_customer/app/data/models/ProductDetailResponse.dart';
import 'package:markkito_customer/app/data/models/ProductDetailUpdateResponse.dart';
import 'package:markkito_customer/app/data/models/ShopDetailsResponse.dart' as store;
import 'package:markkito_customer/app/data/models/SingleVendorHomeResponse.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';

import 'package:markkito_customer/app/modules/store_details/controllers/store_details_controller.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

class ProductDetailsController extends GetxController {
  List<String> images = [
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQTIZccfNPnqalhrWev-Xo7uBhkor57_rKbkw&usqp=CAU",
    "https://wallpaperaccess.com/full/2637581.jpg",
    "https://picsum.photos/200/300"
  ];
  late PageController pageController;

  List productSizes = [
    'S',
    'M',
    'L',
    'XL',
  ];

  List productColors = [
    Colors.black,
    Colors.yellow,
    Colors.blueAccent,
    Colors.red,
  ];

  var productSizesSelected = 0.obs;
  var productColorsSelected = 0.obs;

  var isLoading = true.obs;
  // ProductDetail? productDetail;
  var choiceOfCrust = <ChoiceOfCrust>[].obs;
  var toping = <Topping>[].obs;
  var varients = <Varient>[].obs;
  var selectedVariantData = <int, VarientData>{}.obs;
  var isFav = false.obs;
  var isInCart = 0.obs;

  Rx<PriceList?> selectedPriceData = Rx<PriceList?>(null);
  Rx<ProductDetail?> productDetail = Rx<ProductDetail?>(null);

  var selectedCartQuantity = 0.obs;

  @override
  void onInit() {
    super.onInit();
    pageController = PageController(viewportFraction: 1);
    getOrdersList();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  getOrdersList() async {
    var data = {
      'VendorUserTypeId': Get.arguments['VendorUserTypeId'],
      'productId': Get.arguments['productId'],
    };
    // var data = {
    //   'VendorUserTypeId': 25,
    //   'productId': 897,
    // };
    var result = await XHttp.request(Endpoints.r_ProductDetailsv2, method: XHttp.POST, data: data);
    result.data;
    var productDetailsResponse = ProductDetailUpdateResponse.fromJson(jsonDecode(result.data));
    if (productDetailsResponse.statusCode == 200) {
      if (productDetailsResponse.productDetail != null) {
        productDetail.value = productDetailsResponse.productDetail;
        if (productDetail.value != null && productDetail.value!.priceList!.isNotEmpty) {
          selectedPriceData.value = productDetail.value?.priceList![0];
          selectedCartQuantity.value = productDetail.value!.priceList![0].cartQty!;
        }
        isFav.value = productDetail.value!.favorite == 0 ? false : true;
        isInCart.value = productDetail.value!.isIncart!;

        choiceOfCrust.addAll(productDetailsResponse.productDetail!.choiceOfCrust!);
        toping.addAll(productDetailsResponse.productDetail!.topping!);
        productDetailsResponse.productDetail!.varient!.asMap().forEach((key, variant) {
          if (variant.varientData!.isNotEmpty) {
            variant.varientData![0].checked = true;
            selectedVariantData[key] = variant.varientData![0];
          }
          varients.add(variant);
        });

        // varients.addAll(productDetailsResponse.productDetail!.varient!);
        isLoading.value = false;
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void updateChoiceOfCrust(ChoiceOfCrust choice) {
    choiceOfCrust[choiceOfCrust.indexWhere((element) => element.id == choice.id)] = choice;
  }

  void updateTopping(Topping topp) {
    toping[toping.indexWhere((element) => element.id == topp.id)] = topp;
  }

  doProductFavorite() {
    //product.selectedPriceListId! = generatedProductListId
    Get.find<HomeController>().doProductFavorite(productDetail.value!.productId!, isFav.value).then((value) {
      if (value) {
        isFav.value = isFav.value ? false : true;
      } //complete productTileUpdate
    });
  }

  void addToCart({status, product_id, quantity}) async {
    var data = {
      'productId': product_id,
      'status': status,
      'quantity': quantity,
      'varient_id': '',
      'choice_of_crust_id': '',
      'topping_id': '',
    };
    var result = await XHttp.request(Endpoints.c_ProductToCart, method: XHttp.POST, data: data);
    var addToCartResponse = CartStatusResponse.fromJson(jsonDecode(result.data));
    if (addToCartResponse.statusCode == 200) {
      print('${productDetail.value!.isIncart}');
      productDetail.value!.isIncart = 1;
      isInCart.value = 1;

      //find current selected product index from the product list
      var productListIndex = productDetail.value!.priceList!
          .indexWhere((element) => element.generatelistdetailsId == selectedPriceData.value!.generatelistdetailsId);
      // update productList cartQty
      productDetail.value!.priceList![productListIndex].cartQty =
          productDetail.value!.priceList![productListIndex].cartQty! + 1;
      // update localTemp Varieable for temp cart count
      selectedPriceData.value!.cartQty = productDetail.value!.priceList![productListIndex].cartQty!;
      selectedCartQuantity.value = selectedPriceData.value!.cartQty!;
      var cartData = CartData()
        ..totalAmount = addToCartResponse.data?.cartDatas?.totalAmount
        ..totalQty = addToCartResponse.data?.cartDatas?.totalQty
        ..totalSavings = addToCartResponse.data?.cartDatas?.totalSavings;

      Get.find<StoreDetailsController>().updateCartData(
        cartData: cartData,
        isAdded: false,
      );
      updateProductAcrossAllScreens();
      //update products data across all screens for update view
      // Get.find<HomeController>().updateProductsAcrossAllScreens(productData);

      SnackBarSuccess(titleText: "Success", messageText: "${addToCartResponse.data?.message}").show();
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void updateCartCount(String status) async {
    //find active productList index from the current product object
    var productListIndex = productDetail.value!.priceList!
        .indexWhere((element) => element.generatelistdetailsId == selectedPriceData.value!.generatelistdetailsId!);

    //find current selected product index from the product list
    // var productIndex = products.indexWhere((element) => element?.id == product.id);
    if (status != 'remove') {
      // update cart qty
      // var productIndex = products.indexWhere((element) => element?.id == product.id);

      var data = {
        'productId': selectedPriceData.value!.generatelistdetailsId,
        'quantity': status == 'decrement'
            ? productDetail.value!.priceList![productListIndex].cartQty! - 1
            : productDetail.value!.priceList![productListIndex].cartQty! + 1,
      };
      var result = await XHttp.request(Endpoints.c_ChangeCartItem, method: XHttp.POST, data: data);
      var addToCartResponse = CartStatusResponse.fromJson(jsonDecode(result.data));
      if (addToCartResponse.statusCode == 200) {
        // update product to the current product list
        productDetail.value!.priceList![productListIndex].cartQty = status == 'decrement'
            ? productDetail.value!.priceList![productListIndex].cartQty! - 1
            : productDetail.value!.priceList![productListIndex].cartQty! + 1;
        selectedPriceData.value!.cartQty = productDetail.value!.priceList![productListIndex].cartQty!;
        // products[productIndex] = product;
        selectedCartQuantity.value = selectedPriceData.value!.cartQty!;

        var cartData = CartData()
          ..totalAmount = addToCartResponse.data?.cartDatas?.totalAmount
          ..totalQty = addToCartResponse.data?.cartDatas?.totalQty
          ..totalSavings = addToCartResponse.data?.cartDatas?.totalSavings;

        Get.find<StoreDetailsController>().updateCartData(
          cartData: cartData,
          isAdded: false,
        );
        //update products data across all screens for update view
        // Get.find<HomeController>().updateProductsAcrossAllScreens(product);
        // SnackBarSuccess(titleText: "Success", messageText: "${addToCartResponse.data}").show();
        updateProductAcrossAllScreens();
      } else {
        SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
        isLoading.value = false;
      }
    } else {
      //remove item from cart if qty selected zero or less than 1

      var data = {
        'productId': selectedPriceData.value!.generatelistdetailsId,
        'status': 'remove',
        'quantity': 0,
        'varient_id': '',
        'choice_of_crust_id': '',
        'topping_id': '',
      };
      var result = await XHttp.request(Endpoints.c_ProductToCart, method: XHttp.POST, data: data);
      var addToCartResponse = CartStatusResponse.fromJson(jsonDecode(result.data));
      if (addToCartResponse.statusCode == 200) {
        // reset temprory update product data
        // product.isIncart = 0;
        // product.priceList![productListIndex].cartQty = 0;
        // product.cartTempCount = 0;
        // products[productIndex] = product;
        selectedPriceData.value!.cartQty = 0;
        isInCart.value = 0;
        productDetail.value!.priceList![productListIndex].cartQty = 0;
        selectedCartQuantity.value = 0;
        var cartData = CartData()
          ..totalAmount = addToCartResponse.data?.cartDatas?.totalAmount
          ..totalQty = addToCartResponse.data?.cartDatas?.totalQty
          ..totalSavings = addToCartResponse.data?.cartDatas?.totalSavings;

        Get.find<StoreDetailsController>().updateCartData(
          cartData: cartData,
          isAdded: false,
        );

        //update products data across all screens for update view
        updateProductAcrossAllScreens();

        SnackBarSuccess(titleText: "Success", messageText: "${addToCartResponse.data?.message}").show();
      } else {
        SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
        isLoading.value = false;
      }
    }
  }

  void updateProductAcrossAllScreens() {
    var priceList = store.PriceList()
      ..generatelistdetailsId = selectedPriceData.value!.generatelistdetailsId!
      ..cartQty = selectedPriceData.value!.cartQty!
      ..price = selectedPriceData.value!.price!
      ..priceFor = selectedPriceData.value!.priceFor;

    Get.find<HomeController>().updateProductsCountAcrossAllScreens(
        generatedProductId: selectedPriceData.value!.generatelistdetailsId!,
        isIncart: isInCart.value,
        cartTempCount: selectedCartQuantity.value,
        priceList: priceList);
  }
}
