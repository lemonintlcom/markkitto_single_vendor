import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/models/ProductDetailUpdateResponse.dart';
import 'package:markkito_customer/app/modules/custom_widgets/LabeledCheckbox.dart';
import 'package:markkito_customer/app/modules/custom_widgets/bottom_sheet_modal.dart';
import 'package:markkito_customer/app/modules/custom_widgets/custom_app_bar.dart';
import 'package:markkito_customer/app/modules/custom_widgets/increment_decrement.dart';
import 'package:markkito_customer/app/modules/custom_widgets/primary_button.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/color_utils.dart';
import 'package:markkito_customer/utils/storage.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../../../constants/colors.dart';
import '../../../../generated/assets.dart';
import '../controllers/product_details_controller.dart';

class ProductDetailsView extends GetView<ProductDetailsController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.white,
        appBar: const CustomAppBar(
          titleText: '',
        ),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark
              .copyWith(statusBarColor: AppColors.transparent, systemNavigationBarColor: AppColors.white),
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Obx(() {
              return controller.isLoading.value
                  ? Container()
                  : SingleChildScrollView(
                      physics: const BouncingScrollPhysics(),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Stack(
                            children: [
                              Column(
                                children: [
                                  SizedBox(
                                    height: context.height * .3,
                                    child: PageView.builder(
                                        itemCount: controller.productDetail.value?.thumbList?.length,
                                        pageSnapping: true,
                                        controller: controller.pageController,
                                        onPageChanged: (page) {},
                                        itemBuilder: (context, pagePosition) {
                                          return Container(
                                              width: Get.width,
                                              margin: const EdgeInsets.all(paddingLarge),
                                              child: Image.network(
                                                  controller.productDetail.value!.thumbList![pagePosition]));
                                        }),
                                  ),
                                  SmoothPageIndicator(
                                      controller: controller.pageController, // PageController
                                      count: controller.productDetail.value!.thumbList!.length,
                                      effect: const WormEffect(
                                          activeDotColor: AppColors.primary_color,
                                          paintStyle: PaintingStyle.stroke,
                                          dotHeight: 8,
                                          dotWidth: 8,
                                          radius: 8), // your preferred effect
                                      onDotClicked: (index) {})
                                ],
                              ),
                              Positioned(
                                right: paddingLarge,
                                top: 10,
                                child: InkWell(
                                  onTap: () {
                                    Get.find<HomeController>().isLoggedIn
                                        ? controller.doProductFavorite()
                                        : BottomSheetModal().showLoginRequired(context);
                                  },
                                  child: Obx(() {
                                    return Image.asset(
                                      Assets.imagesBookmarkOutlineGrey,
                                      color: controller.isFav.value ? AppColors.primary_color : Colors.grey,
                                    );
                                  }),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${controller.productDetail.value!.productType}',
                                  style: body2.copyWith(color: Colors.grey),
                                ),
                                Text(
                                  '${controller.productDetail.value!.productName}',
                                  maxLines: 1,
                                  overflow: TextOverflow.fade,
                                  softWrap: false,
                                  style: body1.copyWith(fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  height: paddingSmall,
                                ),
                                // Text(
                                //   '${Storage.instance.getValue(Constants.currency)} ${controller.productDetail?.productPrice}',
                                //   maxLines: 1,
                                //   overflow: TextOverflow.fade,
                                //   softWrap: false,
                                //   style: headline5.copyWith(fontWeight: FontWeight.bold),
                                // ),
                                const SizedBox(
                                  height: paddingLarge,
                                ),
                                Obx(() {
                                  return Wrap(
                                    spacing: 8,
                                    children: List.generate(controller.productDetail.value!.priceList!.length, (index) {
                                      var priceData = controller.productDetail.value!.priceList![index];
                                      return ChoiceChip(
                                        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
                                        shape: const RoundedRectangleBorder(
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(20), bottomRight: Radius.circular(20))),

                                        label: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              '${Storage.instance.getValue(Constants.currency)} ${priceData.price}',
                                              style: subtitle1.copyWith(
                                                  fontWeight: FontWeight.bold,
                                                  color: controller.selectedPriceData.value?.generatelistdetailsId ==
                                                          priceData.generatelistdetailsId
                                                      ? AppColors.white
                                                      : AppColors.black1),
                                            ),
                                            Text(
                                              'Price for ${priceData.priceFor}',
                                              style: captionLite.copyWith(
                                                  color: controller.selectedPriceData.value?.generatelistdetailsId ==
                                                          priceData.generatelistdetailsId
                                                      ? AppColors.white
                                                      : AppColors.black1),
                                            ),
                                            const SizedBox(
                                              height: paddingLarge,
                                            ),
                                            priceData.type == 'Size'
                                                ? Text(
                                                    'Size - ${priceData.value}',
                                                    style: captionLite.copyWith(
                                                        color:
                                                            controller.selectedPriceData.value?.generatelistdetailsId ==
                                                                    priceData.generatelistdetailsId
                                                                ? AppColors.white
                                                                : AppColors.black1),
                                                  )
                                                : SizedBox(
                                                    width: 100,
                                                    child: Row(
                                                      children: [
                                                        Text(
                                                          'Color - ',
                                                          style: captionLite.copyWith(
                                                              color: controller.selectedPriceData.value
                                                                          ?.generatelistdetailsId ==
                                                                      priceData.generatelistdetailsId
                                                                  ? AppColors.white
                                                                  : AppColors.black1),
                                                        ),
                                                        Container(
                                                          height: 24,
                                                          width: 24,
                                                          decoration: BoxDecoration(
                                                              color: priceData.color!.isEmpty
                                                                  ? hexToColor('#000000')
                                                                  : hexToColor(priceData.color!),
                                                              shape: BoxShape.circle,
                                                              border: Border.all(
                                                                  color: controller.productColorsSelected.value == index
                                                                      ? AppColors.primary_color
                                                                      : Colors.white,
                                                                  width: 0),
                                                              boxShadow: [
                                                                BoxShadow(
                                                                    blurRadius: 2,
                                                                    spreadRadius: 2,
                                                                    color: Colors.grey.shade200),
                                                              ]),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                          ],
                                        ),
                                        selected: controller.selectedPriceData.value == priceData,
                                        selectedColor: Storage.instance.getValue(Constants.colorCode) == null
                                            ? AppColors.primary_color
                                            : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                                        pressElevation: 0,
                                        backgroundColor: AppColors.white,
                                        onSelected: (value) {
                                          controller.selectedPriceData.value =
                                              value ? priceData : controller.selectedPriceData.value;
                                        },
                                        // backgroundColor: color,
                                        elevation: 1,
                                      );
                                    }),
                                  );
                                }),
                                const SizedBox(
                                  height: paddingLarge,
                                ),
/*                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: PrimaryButton(
                                        height: 40,
                                        text: 'Add to Cart',
                                        onTap: () {
                                          showVarientsDialog(context);
                                        },
                                      ),
                                    ),
                                    controller.productDetail.value!.offerText!.isNotEmpty
                                        ? Row(
                                            children: [
                                              const SizedBox(
                                                width: paddingExtraLarge,
                                              ),
                                              const Icon(
                                                Icons.info_outline,
                                                color: AppColors.card_offer_color,
                                              ),
                                              const SizedBox(
                                                width: 4,
                                              ),
                                              Text(
                                                '${controller.productDetail.value!.offerText}',
                                                style: subtitle2.copyWith(color: AppColors.card_offer_color),
                                              ),
                                            ],
                                          )
                                        : Container(),
                                  ],
                                ),*/
                                !Get.find<HomeController>().isLoggedIn
                                    ? Container()
                                    : Obx(() {
                                        return Row(
                                          children: [
                                            controller.isInCart.value == 0
                                                ? Expanded(
                                                    child: MaterialButton(
                                                      color: Storage.instance.getValue(Constants.colorCode) == null
                                                          ? AppColors.primary_color
                                                          : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                                                      textColor: AppColors.white,
                                                      height: 42,
                                                      elevation: 0,
                                                      shape: const RoundedRectangleBorder(
                                                        borderRadius: BorderRadius.all(
                                                          Radius.circular(6),
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        Get.find<HomeController>().isLoggedIn
                                                            ? controller.addToCart(
                                                                status: 'add',
                                                                product_id: controller
                                                                    .selectedPriceData.value!.generatelistdetailsId,
                                                                quantity: 1)
                                                            : BottomSheetModal().showLoginRequired(context);
                                                      },
                                                      child: Text(
                                                        'Add to cart ',
                                                        style: caption.copyWith(fontWeight: FontWeight.bold),
                                                      ),
                                                    ),
                                                  )
                                                : Expanded(
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: [
                                                        SizedBox(
                                                          height: 42,
                                                          child: IncrementDecrement(
                                                            cartProductItems: null,
                                                            count: controller.selectedCartQuantity.value,
                                                            // count: controller.selectedPriceData.value!.cartQty!,
                                                            onDecrement: () {
                                                              if (controller.selectedPriceData.value!.cartQty! > 1) {
                                                                // product.cartTempCount = product.cartTempCount - 1;
                                                                controller.updateCartCount('decrement');
                                                              }
                                                              if (controller.selectedPriceData.value!.cartQty! == 1) {
                                                                controller.updateCartCount('remove');
                                                              }
                                                            },
                                                            onIncrement: () {
                                                              // product.cartTempCount = product.cartTempCount + 1;
                                                              controller.updateCartCount('increment');
                                                            },
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                          ],
                                        );
                                      }),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          Container(width: double.infinity, height: paddingExtraSmall, color: Colors.grey.shade200),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Overview',
                                  style: body1.copyWith(fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  '${controller.productDetail.value!.productDescription}',
                                  style: caption.copyWith(fontSize: 15, color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          /*Container(width: double.infinity, height: paddingExtraSmall, color: Colors.grey.shade200),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          controller.choiceOfCrust.isEmpty
                              ? Container()
                              : Padding(
                                  padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Choice of Crust',
                                        style: body1.copyWith(fontWeight: FontWeight.bold),
                                      ),
                                      Obx(() {
                                        return ListView.separated(
                                          itemCount: controller.choiceOfCrust.length,
                                          physics: const NeverScrollableScrollPhysics(),
                                          shrinkWrap: true,
                                          scrollDirection: Axis.vertical,
                                          itemBuilder: (context, index) {
                                            var choice = controller.choiceOfCrust[index];
                                            return LabeledCheckbox(
                                              trailinglabel:
                                                  '${Storage.instance.getValue(Constants.currency)} ${choice.name}',
                                              label: '${choice.name}',
                                              selectedColor: AppColors.primary_color,
                                              value: choice.checked,
                                              onChanged: (value) {
                                                choice.checked = value;
                                                controller.updateChoiceOfCrust(choice);
                                              },
                                            );
                                          },
                                          separatorBuilder: (context, index) {
                                            return const SizedBox(
                                              height: 4,
                                            );
                                          },
                                        );
                                      }),
                                    ],
                                  ),
                                ),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          Container(width: double.infinity, height: paddingExtraSmall, color: Colors.grey.shade200),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          controller.toping.isEmpty
                              ? Container()
                              : Padding(
                                  padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Topping(1)',
                                        style: body1.copyWith(fontWeight: FontWeight.bold),
                                      ),
                                      Obx(() {
                                        return ListView.separated(
                                          itemCount: controller.toping.length,
                                          physics: const NeverScrollableScrollPhysics(),
                                          shrinkWrap: true,
                                          scrollDirection: Axis.vertical,
                                          itemBuilder: (context, index) {
                                            var topping = controller.toping[index];
                                            return LabeledCheckbox(
                                              trailinglabel:
                                                  '${Storage.instance.getValue(Constants.currency)} ${topping.name}',
                                              label: '${topping.name}',
                                              selectedColor: AppColors.primary_color,
                                              value: topping.checked,
                                              onChanged: (value) {
                                                topping.checked = value;
                                                controller.updateTopping(topping);
                                              },
                                            );
                                          },
                                          separatorBuilder: (context, index) {
                                            return const SizedBox(
                                              height: 4,
                                            );
                                          },
                                        );
                                      }),
                                    ],
                                  ),
                                ),*/
                          const SizedBox(
                            height: paddingLarge,
                          ),
                        ],
                      ),
                    );
            }),
          ),
        ),
      ),
    );
  }

  void showVarientsDialog(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.vertical(top: Radius.circular(16))),
      builder: (context) => DraggableScrollableSheet(
        initialChildSize: 0.4,
        minChildSize: 0.2,
        maxChildSize: 0.75,
        expand: false,
        builder: (_, scrollController) {
          return Column(
            children: [
              Center(
                child: FractionallySizedBox(
                  widthFactor: 0.25,
                  child: Container(
                    margin: const EdgeInsets.symmetric(
                      vertical: 8,
                    ),
                    height: 4,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.circular(2),
                      border: Border.all(
                        color: Colors.black12,
                        width: 0.5,
                      ),
                    ),
                  ),
                ),
              ),
              Center(
                child: Text(
                  'Select product variants',
                  style: subtitle1,
                ),
              ),
              const SizedBox(height: 24),
              Expanded(
                child: ListView.builder(
                  itemCount: controller.varients.length,
                  controller: scrollController,
                  itemBuilder: (BuildContext context, int index) {
                    var variants = controller.varients[index];
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        ListTile(
                          title: Text("${variants.title}"),
                        ),
                        variants.title == 'Color'
                            ? Obx(() {
                                return Padding(
                                  padding: const EdgeInsets.only(left: 18, right: 18),
                                  child: Wrap(
                                    spacing: 8,
                                    children: List.generate(variants.varientData!.length, (index) {
                                      return InkWell(
                                        onTap: () {
                                          controller.productColorsSelected.value = index;
                                        },
                                        child: Container(
                                          height: 38,
                                          width: 38,
                                          decoration: BoxDecoration(
                                              color: hexToColor(variants.varientData![index].varientName!),
                                              shape: BoxShape.circle,
                                              border: Border.all(
                                                  color: controller.productColorsSelected.value == index
                                                      ? AppColors.primary_color
                                                      : Colors.white,
                                                  width: 4),
                                              boxShadow: [
                                                BoxShadow(blurRadius: 2, spreadRadius: 2, color: Colors.grey.shade200),
                                              ]),
                                        ),
                                      );
                                    }),
                                  ),
                                );
                              })
                            : Row(
                                children: [
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 18, right: 18),
                                      child: Container(
                                        decoration: BoxDecoration(
                                          borderRadius: const BorderRadius.all(Radius.circular(4)),
                                          border: Border.all(color: Colors.grey.shade400),
                                        ),
                                        child: Obx(() {
                                          return DropdownButton<VarientData>(
                                            items: variants.varientData!.map((VarientData value) {
                                              return DropdownMenuItem<VarientData>(
                                                value: value,
                                                child: Row(
                                                  children: [
                                                    Expanded(
                                                      child: Padding(
                                                        padding: const EdgeInsets.all(8.0),
                                                        child: Text(
                                                          "${value.varientName}",
                                                          style: body1.copyWith(color: Colors.black),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            }).toList(),
                                            onChanged: (selectedVarient) {
                                              selectedVarient!.checked = true;
                                              controller.selectedVariantData[index] = selectedVarient;
                                            },
                                            value: controller.selectedVariantData[index],
                                            isExpanded: true,
                                          );
                                        }),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                      ],
                    );
                  },
                ),
              ),
            ],
          );
        },
      ),
    );
  }
/*void showVarientsDialog(BuildContext context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        barrierColor: Colors.grey.withOpacity(.4),
        builder: (context) {
          return Padding(
            padding: const EdgeInsets.all(paddingLarge),
            child: Container(
              height: Get.height * .5,
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: const BorderRadius.all(Radius.circular(16)),
                border: Border.all(
                  color: AppColors.white,
                  width: 0.8,
                ),
              ),
              child: Column(
                children: [
                  Center(
                    child: FractionallySizedBox(
                      widthFactor: 0.25,
                      child: Container(
                        margin: const EdgeInsets.symmetric(
                          vertical: 8,
                        ),
                        height: 4,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(2),
                          border: Border.all(
                            color: Colors.black12,
                            width: 0.5,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Text(
                      'Select product variants',
                      style: subtitle1,
                    ),
                  ),
                  const SizedBox(height: 24),
                  SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: List.generate(controller.varients.length, (index) {
                        var item = controller.varients[index];
                        return InkWell(
                          customBorder: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          onTap: () {
                            // selectedDeliveryAddress.value = item;
                          },
                          child: Column(
                            children: [
                              ListTile(
                                title: Text('${item.title}'),
                              ),
                              ListView.separated(
                                shrinkWrap: true,
                                itemCount: item.varientData!.length,
                                separatorBuilder: (BuildContext context, int index) => const Divider(height: 1),
                                itemBuilder: (BuildContext context, int position) {
                                  var variant = item.varientData![position];
                                  return ListTile(
                                    title: Text('item ${variant.varientName}'),
                                  );
                                },
                              ),
                            ],
                          ),
                        );
                      }),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }*/
}
