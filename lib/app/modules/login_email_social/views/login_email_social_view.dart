import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:markkito_customer/app/modules/custom_widgets/custom_app_bar.dart';
import 'package:markkito_customer/app/modules/custom_widgets/primary_button.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/color_utils.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../controllers/login_email_social_controller.dart';

class LoginEmailSocialView extends GetView<LoginEmailSocialController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(
        titleText: 'Update your profile',
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 100,
            ),
            CircleAvatar(
                radius: 30,
                backgroundColor: Storage.instance.getValue(Constants.colorCode) == null
                    ? AppColors.primary_color
                    : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                backgroundImage: Image.network(
                  controller.avatar,
                  errorBuilder: (x, y, z) {
                    return const Icon(
                      Icons.person,
                      size: 48,
                    );
                  },
                ).image),
            const SizedBox(
              height: 4,
            ),
            Text(
              '${controller.name}',
              style: headline6.copyWith(fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 4,
            ),
            Text(
              '${controller.email}',
              style: body1.copyWith(fontWeight: FontWeight.normal),
            ),
            const SizedBox(
              height: 42,
            ),
            IntlPhoneField(
              obscureText: false,
              disableLengthCheck: false,
              dropdownIconPosition: IconPosition.trailing,
              showDropdownIcon: true,
              flagsButtonPadding: const EdgeInsets.all(10),
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(8),
                enabledBorder: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  // width: 0.0 produces a thin "hairline" border
                  borderSide: BorderSide(color: Colors.grey, width: 0.0),
                ),
                focusedBorder: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  borderSide: BorderSide(color: AppColors.primary_color, width: 1),
                ),
                hintText: 'Mobile Number',
                hintStyle: subtitle1,
              ),
              initialCountryCode: '${Storage.instance.getValue(Constants.globalCountryCode)}',
              onChanged: (phone) {
                controller.phoneNumber = phone.number;
                controller.completePhoneNumber = phone.completeNumber;
              },
            ),
            const SizedBox(
              height: 42,
            ),
            Row(
              children: [
                Expanded(
                  child: PrimaryButton(
                    text: 'Submit',
                    onTap: () {
                      controller.submitProfile();
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
