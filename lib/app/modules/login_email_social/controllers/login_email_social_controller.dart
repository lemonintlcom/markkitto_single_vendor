import 'dart:convert';

import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/UserData.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/FavoriteResponse.dart';
import 'package:markkito_customer/app/data/models/RegisterWithEmailResponse.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

class LoginEmailSocialController extends GetxController {
  //TODO: Implement LoginEmailSocialController

  final count = 0.obs;
  var box = Hive.box(Constants.configName);
  var phoneNumber;

  String? completePhoneNumber;

  var name = Get.arguments['name'];
  var email = Get.arguments['email'];
  var avatar = Get.arguments['avatar'];

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void increment() => count.value++;

  void submitProfile() async {
    if (GetUtils.isPhoneNumber(phoneNumber!)) {
      var data = {
        'CountryId': Storage.instance.getValue(Constants.countryId),
        'Mobile_Number': '${completePhoneNumber?.replaceAll('+', '')}',
        'Email': '$email',
        'avatar': '$avatar',
        'name': '$name',
        'BuildingDetails': box.get(Constants.buildingDetails, defaultValue: ''),
        'StreetDetails': box.get(Constants.streetDetails, defaultValue: ''),
        'Locality': box.get(Constants.locality, defaultValue: ''),
        'State': box.get(Constants.state, defaultValue: ''),
        'City': box.get(Constants.city, defaultValue: ''),
        'PinCode': box.get(Constants.pinCode, defaultValue: ''),
        'Landmark': box.get(Constants.landmark, defaultValue: ''),
        'Latitude': box.get(Constants.latitude, defaultValue: ''),
        'Longitude': box.get(Constants.longitude, defaultValue: ''),
      };
      var result = await XHttp.request(Endpoints.login_or_register_with_email, method: XHttp.POST, data: data);
      var responseData = RegisterWithEmailResponse.fromJson(jsonDecode(result.data));
      if (responseData.statusCode == 200) {
        // box.put(isUserLoggedIn, true);
        if (responseData.user!.isNotEmpty) {
          Storage.instance.setValue(Constants.isUserLoggedIn, true);
          await saveUsrDataToDB(responseData.user![0]);
          updateUserLocation(responseData.user![0].userId);
          SnackBarSuccess(titleText: "Success", messageText: "User successfully logged in").show();
          Get.offAllNamed(Routes.HOME);
        } else {
          SnackBarFailure(titleText: "Oops..", messageText: "Unable to login right now").show();
        }
      } else {
        SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Invalid phone number.").show();
      return;
    }
  }

  Future<void> saveUsrDataToDB(User? user) async {
    box.put(Constants.userdb, jsonEncode(user?.toJson()));
    box.put(Constants.authToken, user?.auth);
    XHttp.setAuthToken(user?.auth);

    Logger().e((box.get(Constants.userdb)));
  }

  Future<void> updateUserLocation(int? userTypeId) async {
    var data = {
      'address2': box.get(Constants.locality, defaultValue: ''),
      'address1': box.get(Constants.locationText, defaultValue: ''),
      'DeliveryContact1': completePhoneNumber,
      'DeliveryContact2': '',
      'usertypeid': userTypeId,
      'BuildingDetails': box.get(Constants.buildingDetails, defaultValue: ''),
      'StreetDetails': box.get(Constants.streetDetails, defaultValue: ''),
      'Locality': box.get(Constants.locality, defaultValue: ''),
      'State': box.get(Constants.state, defaultValue: ''),
      'City': box.get(Constants.city, defaultValue: ''),
      'PinCode': box.get(Constants.pinCode, defaultValue: ''),
      'Landmark': box.get(Constants.landmark, defaultValue: ''),
      'Latitude': box.get(Constants.latitude, defaultValue: ''),
      'Longitude': box.get(Constants.longitude, defaultValue: ''),
    };
    var result = await XHttp.request(Endpoints.i_user_address, method: XHttp.POST, data: data);
    var responseData = FavoriteResponse.fromJson(jsonDecode(result.data));
    if (responseData.statusCode == 200) {
      if (responseData.data?.status == 'success') {
        Get.offAllNamed(Routes.HOME);
      } else {
        SnackBarFailure(titleText: "Failed", messageText: "Unable to save user location. Try again").show();
      }
    }
  }
}
