import 'package:get/get.dart';

import '../controllers/login_email_social_controller.dart';

class LoginEmailSocialBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LoginEmailSocialController>(
      () => LoginEmailSocialController(),
    );
  }
}
