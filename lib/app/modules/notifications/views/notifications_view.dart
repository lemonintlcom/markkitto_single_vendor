import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:markkito_customer/app/modules/custom_widgets/custom_app_bar.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/generated/assets.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

import '../controllers/notifications_controller.dart';

class NotificationsView extends GetView<NotificationsController> {
  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        appBar: const CustomAppBar(
          titleText: 'Notifications',
        ),
        body: controller.isLoading.value
            ? Container()
            : controller.notifications.isNotEmpty
                ? buildListView(context)
                : Padding(
                    padding: EdgeInsets.all(paddingExtraLarge),
                    child: Center(
                      child: Lottie.asset(Assets.lottieNodata, repeat: false),
                    ),
                  ),
      );
    });
  }

  Widget buildListView(BuildContext context) {
    return ListView.separated(
      controller: controller.scrollController,
      itemCount: controller.notifications.length,
      separatorBuilder: (BuildContext context, int index) => Divider(height: 1),
      itemBuilder: (BuildContext context, int index) {
        var notiItem = controller.notifications[index];
        return ListTile(
          leading: const Icon(Icons.notifications),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '${notiItem.title}',
                softWrap: false,
                overflow: TextOverflow.fade,
                maxLines: 1,
                style: body1,
              ),
              Text(
                '${notiItem.createdAt}',
                style: caption.copyWith(color: Colors.grey),
              ),
            ],
          ),
          subtitle: Text(
            '${notiItem.message}',
            style: caption.copyWith(color: Colors.black),
          ),
        );
      },
    );
  }
}
