import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/custom_widgets/custom_app_bar.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../controllers/about_controller.dart';

class AboutView extends GetView<AboutController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
          titleText: 'About Us',
        ),
        body: Container(
          width: Get.width,
          height: Get.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: SizedBox(
                  height: paddingSmall,
                ),
              ),
              Image.network(Storage.instance.getValue(Constants.logo), height: 150, width: 150,
                  errorBuilder: (context, error, stackTrace) {
                return Container(
                  child: Image.network("https://picsum.photos/200/300", height: 150, width: 150),
                );
              }),
              SizedBox(
                height: paddingExtraLarge,
              ),
              Text(
                '${Storage.instance.getValue(Constants.appName)}',
                style: headline4,
              ),
              SizedBox(
                height: paddingSmall,
              ),
              Padding(
                padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                child: Text(
                  '${Storage.instance.getValue(Constants.tagLine)}',
                  textAlign: TextAlign.center,
                  style: subtitle1,
                ),
              ),
              Expanded(
                child: SizedBox(
                  height: paddingSmall,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                child: Text(
                  'Contact',
                  textAlign: TextAlign.start,
                  style: body1.copyWith(fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: paddingSmall,
              ),
              Padding(
                padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                child: Text(
                  '+${Storage.instance.getValue(Constants.countryMobileCode)}-${Storage.instance.getValue(Constants.contact1)} \n ${Storage.instance.getValue(Constants.email)}',
                  textAlign: TextAlign.center,
                  style: body1.copyWith(fontWeight: FontWeight.normal),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
